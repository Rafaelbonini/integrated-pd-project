//
//  TableOrder.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/22/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import Foundation

class tableOrder{
    
    var userUid:String
    var tableStatus:String
    var tableNum:Int
    var items:[ItemFromOrder]
    var timeStamp : Int
    
    init(userUid:String,tableStatus:String,tableNum:Int,items:[ItemFromOrder],timeStamp : Int) {
    
        self.userUid = userUid
        self.tableStatus = tableStatus
        self.tableNum = tableNum
        self.items = items
        self.timeStamp = timeStamp
    }
}
