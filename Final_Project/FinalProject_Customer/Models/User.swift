//
//  User.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 12/14/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase

class User
{
    var userUid:String
    var userName:String
    var userEmail:String
    var profilePic:UIImage?
    var phoneNumber:String
    
    
    
    init(userUid:String,userName:String,userEmail:String,profilePic:UIImage,phoneNumber:String) {
        self.userUid = userUid
        self.userName = userName
        self.userEmail = userEmail
        self.profilePic = profilePic
        self.phoneNumber = phoneNumber
    }

    func save(completion: @escaping (Error?) -> Void)
    {
        
        // reference to the database
        let ref = Database.database().reference().child("Users").child(userUid)
        
        
        
        // setValue of the reference
        ref.setValue(toDictionary())
        
        // 3. save the user's profile Image to the storage
        
        
        if let image = profilePic?.jpegData(compressionQuality: 0.1){
            
            let storageRefere = Storage.storage().reference().child("ProfilePictures/\(userUid).jpg")
            
            storageRefere.putData(image, metadata: nil){ (metadata, error) in
                
                if error != nil{
                    print(error!)
                    
                }else{
                    
                    print("naicela")
                }

                
            }
        }
    }
    
    func toDictionary() -> [String : Any]
    {
        return [
            "uid" : userUid,
            "username" : userName,
            "email" : userEmail,
            "phone" : phoneNumber,

        ]
    }
    
    
}
