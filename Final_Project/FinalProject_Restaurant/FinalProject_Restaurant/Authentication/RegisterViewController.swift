//
//  RegisterViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/13/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
import CoreLocation
import MapKit
import JGProgressHUD

private let goToMainSegue = "GoToMain"
class RegisterViewController: UIViewController,CLLocationManagerDelegate, UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextFieldDelegate {
    
    @IBOutlet weak var backgroundImageview: UIImageView!
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    @IBOutlet weak var invalidInputLabel: UILabel!
    @IBOutlet weak var userProfileImage: UIImageView!
    @IBOutlet weak var numOfTables: UITextField!
    
    @IBOutlet weak var locationView: UIView!
    @IBOutlet weak var locationViewBottomConstraint: NSLayoutConstraint!
    var imagePicker = UIImagePickerController()
    
//    @IBOutlet weak var backgroundView: UIView!asds
    
    @IBOutlet weak var locationFeedback: UILabel!
    @IBOutlet weak var locationSteet: UITextField!
    @IBOutlet weak var locationCity: UITextField!
    @IBOutlet weak var locationState: UITextField!
    @IBOutlet weak var locationZipCode: UITextField!
    var added = false
    let hud = JGProgressHUD(style: .dark)
    let manager = CLLocationManager()
    
    var latitude = ""
    var longitude = ""
    
    let blackView : UIView = {
        let bView = UIView(frame: .zero)
        
        bView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.1)
        
        return bView
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
//        backgroundView.layer.addSublayer(setGradientBackground())
        
        
        
        let blurredBackgroundView = UIVisualEffectView()
        blurredBackgroundView.frame = CGRect(x: 0, y: 0, width: locationView.layer.frame.width, height: locationView.layer.frame.height)
        blurredBackgroundView.effect = UIBlurEffect(style: .regular)

        locationView.backgroundColor = UIColor(displayP3Red: 0, green: 0, blue: 0, alpha: 0)
        locationView.addSubview(blurredBackgroundView)
        locationView.sendSubviewToBack(blurredBackgroundView)
//        if !added{
//
//            added = true
//        }
        
        self.userName.delegate = self
        self.userEmail.delegate = self
        self.userPassword.delegate = self
        self.numOfTables.delegate = self
        
        self.locationSteet.delegate = self
        self.locationCity.delegate = self
        self.locationState.delegate = self
        self.locationZipCode.delegate = self

        
        manager.delegate = self
        
        locationViewBottomConstraint.constant = -(backgroundImageview.frame.height/2)
        
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissLocation)))
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(editProfileImage))
        userProfileImage.isUserInteractionEnabled = true
        userProfileImage.addGestureRecognizer(singleTap)
        
        userProfileImage.layer.cornerRadius = userProfileImage.bounds.width/2
        userProfileImage.layer.masksToBounds = true
        
        
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil{
                
                self.performSegue(withIdentifier: goToMainSegue, sender: nil)
                
            }
        }
        
        invalidInputLabel.alpha = 0
        
        SetTextViews()
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        if textField == userName{
            textField.resignFirstResponder()
            userEmail.becomeFirstResponder()
        }else if textField == userEmail{
            textField.resignFirstResponder()
            userPassword.becomeFirstResponder()
        }else if textField == userPassword{
            textField.resignFirstResponder()
            numOfTables.becomeFirstResponder()
        }else if textField == numOfTables{
            textField.resignFirstResponder()
        }
        
        if textField == locationSteet{
            textField.resignFirstResponder()
            locationCity.becomeFirstResponder()
        }else if textField == locationCity{
            textField.resignFirstResponder()
            locationState.becomeFirstResponder()
        }else if textField == locationState{
            textField.resignFirstResponder()
            locationZipCode.becomeFirstResponder()
        }else if textField == locationZipCode{
            textField.resignFirstResponder()
        }
        
        
        return true
    }
    
    @objc func editProfileImage(){
        
        
        self.imagePicker.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                
                self.imagePicker.sourceType = .photoLibrary;
                self.imagePicker.allowsEditing = true
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    //MARK: - RESGISTER
    @IBAction func createAccount(_ sender: Any) {
        
        userPassword.layer.borderWidth = 0
        userName.layer.borderWidth = 0
        userEmail.layer.borderWidth = 0
        numOfTables.layer.borderWidth = 0
        
        if let email = userEmail.text, let password = userPassword.text, let name = userName.text, let nTables = numOfTables.text{
            
            if name.count > 2 {
                
                if email.isValidEmail(){
                    
                    if password.count > 5{
                        
                        if nTables != "" && nTables != "0"{
                            
                            if latitude != ""{
                                
                                hud.textLabel.text = "Signing Up"
                                hud.show(in: self.view)
                                
                                
                                Auth.auth().createUser(withEmail: email, password: password) { (firuser, error) in
                                    if(error != nil){
                                        print("error in create user: \(String(describing: error))")
                                        
                                        self.hud.dismiss(animated: true)
                                        
                                        // create the alert
                                        let alert = UIAlertController(title: "Register", message: "This account already exists", preferredStyle: UIAlertController.Style.alert)
                                        
                                        // add an action (button)
                                        alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                                        
                                        // show the alert
                                        self.present(alert, animated: true, completion: nil)
                                        
                                        
                                    }else if let firuser = firuser{
                                        
                                        //set the user name
                                        let changeResquest = Auth.auth().currentUser?.createProfileChangeRequest();
                                        changeResquest?.displayName = name;
                                        changeResquest?.commitChanges(completion: { (error) in
                                            if (error != nil){
                                                print("user name change request failed")
                                            }else{
                                                print("user name chaged!")
                                            }
                                        })
                                        //update saved restaurant num of tables
                                        UserDefaults.standard.setValue(Int(nTables), forKey: numOfTableKey)
                                        
                                        var newUser = Restaurant(userUid: "", userName: "", userEmail: "", profilePic: UIImage(), latitude: "", longitude: "", numOfTables: 0)
                                        
                                        if let image = self.userProfileImage.image{
                                            newUser = Restaurant(userUid:firuser.user.uid , userName: name, userEmail: email, profilePic: image, latitude: self.latitude, longitude: self.longitude, numOfTables: Int(nTables)!)
                                        }else{
                                            newUser = Restaurant(userUid:firuser.user.uid , userName: name, userEmail: email, profilePic: UIImage(named: "profileimage")!, latitude: self.latitude, longitude: self.longitude, numOfTables: Int(nTables)!)
                                        }
                                        
                                        
                                        self.hud.dismiss(animated: true)
                                        
                                        newUser.save(completion: { (error) in
                                            if error == nil{
                                                print("error saving user to database")
                                                self.hud.dismiss(animated: true)
                                            }
                                        })
                                        
                                    }
                                }
                            }else{
                                
                                //location not set
                                
                                invalidInputLabel.text = "Please, set the location first"
                                invalidInputLabel.alpha = 1
                                //                            userPassword.layer.borderWidth = 2
                                //                            userPassword.layer.borderColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 0.5).cgColor
                                
                            }
                            
                        }else{
                            
                            //number of tables empty / invalid
                            
                            invalidInputLabel.text = "Please, enter a valid number of tables."
                            invalidInputLabel.alpha = 1
                            numOfTables.layer.borderWidth = 2
                            numOfTables.layer.borderColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 0.5).cgColor
                            
                        }
                    }else{
                        //password invalid
                        
                        invalidInputLabel.text = "A valid password needs 6 or more characters."
                        invalidInputLabel.alpha = 1
                        userPassword.layer.borderWidth = 2
                        userPassword.layer.borderColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 0.5).cgColor
                        
                    }
                    
                }else{
                    //email invalid
                    
                    invalidInputLabel.text = "Please, enter a valid email."
                    invalidInputLabel.alpha = 1
                    userEmail.layer.borderWidth = 2
                    userEmail.layer.borderColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 0.5).cgColor
                }
                
            }else{
                //warn name invalid
                
                invalidInputLabel.text = "A valid name need 3 or more characters"
                invalidInputLabel.alpha = 1
                userName.layer.borderWidth = 2
                userName.layer.borderColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 0.5).cgColor
                
            }
            
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        userProfileImage.image = image
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    @IBAction func setLocation(_ sender: Any) {
        
        
        if let window = UIApplication.shared.keyWindow{
            
            window.addSubview(blackView)
            blackView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            
//            view.bringSubviewToFront(location)
            
            blackView.alpha = 0
            
            UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                if let window = UIApplication.shared.keyWindow{
                    
                    
                    self.blackView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height/2+10)
                    self.blackView.alpha = 1
                    self.locationViewBottomConstraint.constant = 0
                    self.view.layoutIfNeeded()
                }
            }, completion: nil)
            
        }
        
        
        
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
    }
    
    @objc func dismissLocation(){
        
        
        UIView.animate(withDuration: 0.4, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            if let window = UIApplication.shared.keyWindow{
                
                self.blackView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
                self.blackView.alpha = 0
                self.locationViewBottomConstraint.constant = -(self.backgroundImageview.frame.height/2)
                self.view.layoutIfNeeded()
                
                self.locationView.endEditing(true)
            }
        }, completion: nil)
        
        
        blackView.removeFromSuperview()
        
        //        locationViewBottomConstraint.constant = -(backgroundView.frame.height/2)
        
    }
    
    @IBAction func setLocationFromInput(_ sender: Any) {
        
        //use address inpu
        let geocoder = CLGeocoder()
        
        //get input location for text fields
        if let street = locationSteet.text, let city = locationCity.text, let state = locationState.text, let zipCode = locationZipCode.text{
            
            //verify that input is not empty
            if street.count != 0 && city.count != 0 && state.count != 0 && zipCode.count != 0{
                
                locationFeedback.alpha = 0
                
                let address = "\(street), \(city), \(state) \(zipCode)"
                
                //try to get the point location from the input address
                geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
                    if((error) != nil){
                        print("Error", error ?? "")
                    }
                    
                    if let placemark = placemarks?.first {
                        let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                        print("Lat: \(coordinates.latitude) -- Long: \(coordinates.longitude)")
                    
                        //save latitude and longitude representation from the address
                        self.latitude = String(coordinates.latitude)
                        self.longitude = String(coordinates.longitude)
                        
                        let mapSnapshotOptions = MKMapSnapshotter.Options()

                        let location = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude)

                        let region = MKCoordinateRegion(center: location, latitudinalMeters: 350, longitudinalMeters: 350)
                        mapSnapshotOptions.region = region
                        

                        //Get image of current location
                        // Set the scale of the image. We'll just use the scale of the current device, which is 2x scale on Retina screens.
                        mapSnapshotOptions.scale = UIScreen.main.scale
                        
                        // Set the size of the image output.
                        mapSnapshotOptions.size = CGSize(width: 300, height: 300)
                        
                        // Show buildings and Points of Interest on the snapshot
                        mapSnapshotOptions.showsBuildings = true
                        mapSnapshotOptions.showsPointsOfInterest = true
                        
                        let snapShotter = MKMapSnapshotter(options: mapSnapshotOptions)
                        
                        snapShotter.start(completionHandler: { (snapshot, error) in
                            
                            guard let snapshot = snapshot, error == nil else {
//                                print("\(error)")
                                return
                            }
                            
                            //set an pin at the center of the map image
                            let imageView = UIImageView(frame: CGRect(x: 10, y: 50, width: 250, height: 230))
                                
                                UIGraphicsBeginImageContextWithOptions(CGSize(width: 300, height: 300), true, 0)
                                snapshot.image.draw(at: .zero)
                                
                                let pinView = MKPinAnnotationView(annotation: nil, reuseIdentifier: nil)
                                let pinImage = pinView.image
                                
                                var point = snapshot.point(for: coordinates)
                                
                                if imageView.bounds.contains(point) {
                                    let pinCenterOffset = pinView.centerOffset
                                    point.x -= pinView.bounds.size.width / 2
                                    point.y -= pinView.bounds.size.height / 2
                                    point.x += pinCenterOffset.x
                                    point.y += pinCenterOffset.y
                                    pinImage?.draw(at: point)
                                }
                                
                                let image = UIGraphicsGetImageFromCurrentImageContext()
                                
                                UIGraphicsEndImageContext()
                                
                            
                                
                                DispatchQueue.main.async {
                                    //display map image from set address with a pin at the center in alert for confirmation
                                    let showAlert = UIAlertController(title: "Confirm Location", message: nil, preferredStyle: .alert)
                                    
                                    imageView.image = image //  image here
                                    showAlert.view.addSubview(imageView)
                                    let height = NSLayoutConstraint(item: showAlert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 335)
                                    let width = NSLayoutConstraint(item: showAlert.view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
                                    
                                    showAlert.view.addConstraint(height)
                                    showAlert.view.addConstraint(width)
                                    showAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { action in
                                        // confirmed
                                        self.dismissLocation()
                                        
                                    }))
                                    showAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                                        // canceled
                                        self.latitude = ""
                                        self.longitude = ""
                                    }))
                                    self.present(showAlert, animated: true, completion: nil)
                                }
                            
                            
                        })

                    }
                })
                
                
            }else{
                locationFeedback.alpha = 1
            }
        }
    }
    
    
    func SetTextViews(){
        
        
        //set text field background color
        userName.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        userEmail.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        userPassword.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        numOfTables.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        locationSteet.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        locationCity.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        locationState.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        locationZipCode.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        
        //set textfield placeholder text color
        userName.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        userEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        userPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        numOfTables.attributedPlaceholder = NSAttributedString(string: "Num of tables", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        
        locationSteet.attributedPlaceholder = NSAttributedString(string: "Street", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        locationCity.attributedPlaceholder = NSAttributedString(string: "City", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        locationState.attributedPlaceholder = NSAttributedString(string: "State", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        locationZipCode.attributedPlaceholder = NSAttributedString(string: "Zip Code", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        
        //set textfield text padding
        userName.setLeftPaddingPoints(20)
        userEmail.setLeftPaddingPoints(20)
        userPassword.setLeftPaddingPoints(20)
        numOfTables.setLeftPaddingPoints(20)
    }
    
    func setGradientBackground() -> CAGradientLayer {
        let colorTop =  UIColor(red: 130/255.0, green: 194/255.0, blue: 238/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 185/255.0, green: 244/255.0, blue: 182/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        
        return gradientLayer
    }
    
    @objc func keyboardWillAppear(_ notification: Notification) {
        
        let keyboardDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber
        let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
        
        
        
        UIView.animate(withDuration: keyboardDuration!, delay: 0.0, options: UIView.AnimationOptions(rawValue: UInt(curve)), animations: {
            
            
            if self.locationViewBottomConstraint.constant == 0{
                
                self.locationViewBottomConstraint.constant = (keyboardFrame?.height)!
                
                self.locationView.frame = CGRect(x: 0, y: (self.backgroundImageview.frame.height/2)-(keyboardFrame?.height)!-10, width: self.locationView.frame.width, height: self.locationView.frame.height)
                self.blackView.frame = CGRect(x: 0, y: 0, width: self.blackView.frame.width, height: self.locationView.frame.minY+20)
                
            }
        }, completion: nil)
        
    }
    
    @objc func keyboardWillDisappear(_ notification: Notification) {
        
        let keyboardDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        
        
        UIView.animate(withDuration: keyboardDuration!, animations: {
            
            
            //            if let window = UIApplication.shared.keyWindow{
            if self.locationViewBottomConstraint.constant > 0{
                self.locationViewBottomConstraint.constant = 0
                
                self.locationView.frame = CGRect(x: 0, y: self.backgroundImageview.frame.height/2, width: self.locationView.frame.width, height: self.locationView.frame.height)
                self.blackView.frame = CGRect(x: 0, y: 0, width: self.blackView.frame.width, height: self.backgroundImageview.frame.height/2+20)
                
            }
            
        }, completion: nil)
    }
    
    @IBAction func userCurrentLocation(_ sender: Any) {
        hud.textLabel.text = "Getting current Location"
        hud.show(in: self.view)
        
        manager.requestWhenInUseAuthorization()
        manager.requestLocation()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            hud.dismiss(animated: true)
            print("Found user's location: \(location)")
            latitude = String(location.coordinate.latitude)
            longitude = String(location.coordinate.longitude)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
        hud.dismiss(animated: true)
    }
    
    func addCameraIcToImgView(){
        let image = UIImage(named: "camera_ic_white")
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: (userProfileImage.bounds.width/2)-25, y: (userProfileImage.bounds.height/2)-25, width: 50, height: 40)
        imageView.alpha = 0.6
        userProfileImage.addSubview(imageView)
    }
}
