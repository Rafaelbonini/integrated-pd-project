//
//  MenuCellTableViewCell.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/29/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

class MenuCellTableViewCell: UITableViewCell {

    
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemPrice: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
