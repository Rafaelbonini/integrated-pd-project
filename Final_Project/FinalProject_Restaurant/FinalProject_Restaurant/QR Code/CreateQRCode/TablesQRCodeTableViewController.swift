//
//  TablesQRCodeTableViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/15/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import JGProgressHUD

private let qrCodeCellReuseId = "QrCodeCell"
class TablesQRCodeTableViewController: UITableViewController {

    let hud = JGProgressHUD(style: .dark)
    
    var numOfTables = 0
    var userUid : String? = ""
    let imageView = UIImageView()
    var getTableNum : Int?
    
    var qrCodeImageslist : [UIImage] = []
//    let hud = JGProgressHUD(style: .dark)
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let tablesAmount = getTableNum{
            numOfTables = tablesAmount
        }
        userUid = Auth.auth().currentUser?.uid

    }
    
    override func viewWillAppear(_ animated: Bool) {
        

        
        
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return numOfTables
    }

    

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: qrCodeCellReuseId, for: indexPath) as! QRCodeTableViewCell

        // Configure the cell...

        cell.qrCellLabel.text = "Table \(indexPath.row+1)"
        
        let qrCodeContent = "\(userUid!).\(indexPath.row+1)"
        
        let qrCodeImage = QRCodeGenerator.generateQRCodeFromString(qrCodeContent)
        let image = QRCodeGenerator.convert(qrCodeImage)
        
        qrCodeImageslist.append(image)
        cell.qrCellImage.image = textToImage(drawText: "\(indexPath.row+1)", inImage: image, atPoint: CGPoint(x: 151, y:130))
        
        
        return cell
    }
    
    func textToImage(drawText text: String, inImage image: UIImage, atPoint point: CGPoint) -> UIImage {
        let textColor = UIColor(displayP3Red: 255/255, green: 255/255, blue: 255/255, alpha: 0.5)
        let textFont = UIFont(name: "Avenir Black", size: 85)!
        
        let scale = UIScreen.main.scale
        UIGraphicsBeginImageContextWithOptions(image.size, false, scale)
        
        
        let textFontAttributes = [
            NSAttributedString.Key.font: textFont,
            NSAttributedString.Key.foregroundColor: textColor,
            ] as [NSAttributedString.Key : Any]
        image.draw(in: CGRect(origin: CGPoint.zero, size: image.size))
        
        let rect = CGRect(origin: point, size: image.size)
        text.draw(in: rect, withAttributes: textFontAttributes)
        
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
     

        let actionSheet = UIAlertController(title: "QR Code", message: "Save code image?", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action:UIAlertAction) in
            
            let imageV = UIImageView()
            imageV.image = self.qrCodeImageslist[indexPath.row]
            
            let image = self.qrCodeImageslist[indexPath.row]
            UIImageWriteToSavedPhotosAlbum(image, self, #selector(self.self.image(_:didFinishSavingWithError:contextInfo:)), nil)
        }))
        
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
  
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 400
    }
    
    
    
    @objc func image(_ image: UIImage, didFinishSavingWithError error: Error?, contextInfo: UnsafeRawPointer) {
        if let error = error {
            // we got back an error!
            let ac = UIAlertController(title: "Save error", message: error.localizedDescription, preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        } else {
            let ac = UIAlertController(title: "Saved!", message: "Code has been saved to your photos.", preferredStyle: .alert)
            ac.addAction(UIAlertAction(title: "OK", style: .default))
            present(ac, animated: true)
        }
    }
    @IBAction func saveImages(_ sender: Any) {
        
        let alert = UIAlertController(title: "Save Images", message: "Save all \(numOfTables) table QR Codes to your Photos?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (action) in
            

            for i in 1...self.numOfTables{
                
                let qrCodeContent = "\(self.userUid!).\(i)"
                
                let qrCodeImage = QRCodeGenerator.generateQRCodeFromString(qrCodeContent)
                let image = QRCodeGenerator.convert(qrCodeImage)
                UIImageWriteToSavedPhotosAlbum(image, self, nil, nil)
            }

            
            let finalizedAlert = UIAlertController(title: "Images Saved", message: "The images have been saved in your Photos.", preferredStyle: .alert)
            finalizedAlert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            self.present(finalizedAlert, animated: true)
            
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(alert, animated: true)
    }
}
