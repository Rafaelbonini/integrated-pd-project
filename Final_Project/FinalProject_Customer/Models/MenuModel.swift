//
//  MenuModel.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/16/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import Foundation
import UIKit

class MenuModel{
    
    
    var dishName : String
    var dishCategory : String
    var dishDesc : String
    var dishPortion : String
    var dishPrice : String
    var hasImg:Bool
    var index:Int
    var image:UIImage?
    
    
    init(dishName : String,dishCategory : String, dishDesc : String, dishPortion : String, dishPrice : String,hasImg:Bool,index:Int,image:UIImage?) {
        self.dishName = dishName
        self.dishCategory = dishCategory
        self.dishDesc = dishDesc
        self.dishPortion = dishPortion
        self.dishPrice = dishPrice
        self.hasImg = hasImg
        self.index = index
        self.image = image
    }
    
    
    
}
