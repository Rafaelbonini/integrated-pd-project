//
//  QRCodeViewController.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 12/11/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
import AVFoundation
import QRCodeReader
import JGProgressHUD
import SwiftMessages
import CoreLocation


public let restaurantUidKey = "restaurantUid"
public let restaurantTableNumber = "tableNumber"
public let orderStatusKey = "orderStatus"
private let goToMenuSegueId = "GoToMenu"
class QRCodeViewController: UIViewController, CLLocationManagerDelegate, QRCodeReaderViewControllerDelegate {
    
    @IBOutlet weak var backgroundView: UIView!
    
    let hud = JGProgressHUD(style: .dark)
    
    let manager = CLLocationManager()
    
    @IBOutlet weak var navigationBarC: UINavigationBar!
    
    var restaurantMenu : [MenuModel] = []
    var userOrder : [RestaurantItemsTestModel] = []
    var qrCodeContent = ""
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader                  = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            $0.showTorchButton         = true
            $0.preferredStatusBarStyle = .lightContent
            
            
            
            $0.reader.stopScanningWhenCodeIsFound = false
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    
    //permissions to scan qr code
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            
                            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    //present qr code scan
    @IBAction func scan(_ sender: Any) {
        
        guard checkScanPermissions() else { return }
        
        readerVC.modalPresentationStyle = .overCurrentContext
        readerVC.delegate               = self
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
            }
        }
        
        present(readerVC, animated: true, completion: nil)
        
    }
    
    
    //check table status and get restuarant menu to present
    func checkPlacedOrder(){
        guard let userUid = Auth.auth().currentUser?.uid else{return}
        
        let ref = Database.database().reference()
        
        
        ref.child("Users").child(userUid).observeSingleEvent(of: .value, with: { (snapshot) in
            
            if let value = snapshot.value as? [String:Any]{
        
                //check user "curentOrderInfo", if it is the user likely has not finished the payment
                if let currentOrderInfo = value["curentOrderInfo"] as? [String:Any]{
                    
                    
                    self.hud.textLabel.text = "Loading Order Info"
                    self.hud.show(in: self.view)
                    print("has placed order")
                    
                    //get restuarant and table number from order saved to the user "curentOrderInfo"
                    if let restaurantUid = currentOrderInfo["RestaurantUid"] as? String,let tableNum = currentOrderInfo["tableNumber"] as? String{
                        
                        print("restaurant uid \(restaurantUid), table num: \(tableNum)")
                        
                        
                        //TODO: - CHECK WHAT USER NUMBER THE CURRENT USER IS BEFORE GETTING THE ORDER
                        ref.child("Restaurants").child(restaurantUid).child("Tables").child("Table\(tableNum)").observeSingleEvent(of: .value, with: { (snaphotR) in
                            
                            self.hud.textLabel.text = "Verifying Table"
                            
                            if let value = snaphotR.value as? [String:Any]{
                                
                                //loop users in table
                                for item in value{
                                    
                                    //get info from customer in table
                                    if let itemValue = item.value as? NSDictionary{

                                        //check if customer in table corresponds to current user
                                        if item.key == userUid{

                                            //get order
                                            if let order = itemValue["order"] as? [String:Any]{
                                                self.hud.textLabel.text = "Loading Order"
                                                
                                                for item in order{
                                                    
                                                    if let dish = order[item.key] as? [String:Any]{
                                                        
                                                        if let dishName = dish["dishName"] as? String, let dishPrice = dish["dishPrice"] as? Double, let dishQuantity = dish["dishQuantity"] as? Int, let servWFood = dish["servWFood"] as? Bool{
                                                            
                                                            
                                                            self.userOrder.append(RestaurantItemsTestModel(image: UIImage(), name: dishName, desc: "", portionSize: "", servWithFood: servWFood, price: dishPrice, quantity: dishQuantity))
                                                        }
                                                    }
                                                }
                                                // after loading current user order, load restaurant menu and open page
                                                self.getRestaurantMenu(restaurantUid: restaurantUid, restTable: tableNum)
                                            }
                                        }else{
                                            self.hud.dismiss()
                                        }
                                    }
                                }
                                

                            }else{
                                //table num does not exist
                                //restaurant removed user from the table and no one is using the table
                                self.hud.dismiss(animated: true)
                                
                                UserDefaults.standard.setValue("", forKey: orderStatusKey)
                                print("no table")
                            }
                            
                        }, withCancel: { (error) in
                            print(error)
                        })
                    }
                }else{
                    self.hud.dismiss(animated: true)
                    UserDefaults.standard.setValue("", forKey: orderStatusKey)
                    print("does not have placed order")
                }

            }

        }) { (error) in
            print(error)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        let status = UserDefaults.standard.string(forKey: orderStatusKey)
        
        if status == "Placed"{
            
            self.hud.textLabel.text = "Verifying Last Order"
            self.hud.show(in: self.view)
        }
        
        
    }
    
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        
        
        dismiss(animated: true) { [weak self] in
            
            
            self!.qrCodeContent = result.value
            
            
            let splitResult = result.value.components(separatedBy: ".")
            let restaurantUid = splitResult[0]
            let restaurantTable = splitResult[1]
            
            
            print(restaurantUid)
            print(restaurantTable)
            
            UserDefaults.standard.setValue(restaurantUid, forKey: restaurantUidKey)
            UserDefaults.standard.setValue(restaurantTable, forKey: restaurantTableNumber)
            
            
            self!.hud.textLabel.text = "Getting current Location"
            self!.hud.show(in: self!.view)
            self!.manager.requestWhenInUseAuthorization()
            self!.manager.requestLocation()

            
            
            
        }
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        
        reader.stopScanning()
        
//        dismiss(animated: true, completion: nil)
    }
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        manager.delegate = self
        
        
        self.navigationBarC.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationBarC.shadowImage = UIImage()
        self.navigationBarC.isTranslucent = true
        self.navigationBarC.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]

        
        backgroundView.layer.addSublayer(setGradientBackground())
        // Do any additional setup after loading the view.
        UserDefaults.standard.setValue("", forKey: restaurantUidKey)
        
        checkPlacedOrder()
        
    }
    

    
    func getRestaurantMenu(restaurantUid:String,restTable:String){
        
        hud.textLabel.text = "Loading table"
        hud.show(in: self.view)
        
        
        UserDefaults.standard.setValue(restaurantUid, forKey: restaurantUidKey)
        UserDefaults.standard.setValue(restTable, forKey: restaurantTableNumber)
        
        guard let userUid = Auth.auth().currentUser?.uid else {
            return
        }
        
        //check if scanned table number belongs to an active tables from de restaurant
        let ref = Database.database().reference().child("Restaurants").child(restaurantUid)
        
        ref.observeSingleEvent(of: .value, with: { (snapshot) in
            
            let value = snapshot.value as? [String:Any]
            
            if let tableNum = value?["NumerOfTables"] as? Int{
                
                
                print("This is the number of tables: \(tableNum)")
                
                
                //table from the scanned code
                let readTable = Int(restTable)!
                
                if 1...tableNum ~= readTable{
                    
                    self.hud.textLabel.text = "Loading Menu"
                    
                    
                    ref.child("Menu").observeSingleEvent(of: .value) { (snapshot) in
                        
                        print(restaurantUid)
                        
                        let value = snapshot.value as? NSDictionary
                        
                        if value == nil {
                            
                            self.hud.dismiss(afterDelay: 0)
                            
                            let alert = UIAlertController(
                                title: "QRCode",
                                //                message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                                message: String (format:"QR Code not linked to a Restaurant"),
                                preferredStyle: .alert
                            )
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                            return
                        }
                        
                        let numOfDishes = value?["numOfDishes"] as? Int ?? -1
                        var dishName = ""
                        var dishCategory = ""
                        var dishDesc = ""
                        var dishPortion = ""
                        var dishPrice = ""
                        var dishHasImg = false
                        
                        for index in 1...numOfDishes{
                            
                            let foodIndex = value?[String(index)] as? NSDictionary
                            
                            
                            dishName = foodIndex?["dishName"] as? String ?? ""
                            dishCategory = foodIndex?["dishCategory"] as? String ?? ""
                            dishDesc = foodIndex?["dishDesc"] as? String ?? ""
                            dishPortion = foodIndex?["dishPortion"] as? String ?? ""
                            dishPrice = foodIndex?["dishPrice"] as? String ?? ""
                            dishHasImg = foodIndex?["dishHasImage"] as? Bool ?? false
                            
                            
                            self.restaurantMenu.append(MenuModel(dishName: dishName, dishCategory: dishCategory, dishDesc: dishDesc, dishPortion: dishPortion, dishPrice: dishPrice, hasImg: dishHasImg, index: index, image: nil))
                            
                            print("qrcldevkew dish price  \(dishPrice)")
                            
                        }
                        self.customerSeatedToDB(restaurantUid: restaurantUid, restaurantTable: restTable)
                        
                        self.performSegue(withIdentifier: goToMenuSegueId, sender: nil)
                    }
                    //
                }else{
                    print("not ok")
                    self.hud.dismiss(afterDelay: 0)
                    
                    
                    let alert = UIAlertController(title: "Error", message: "Not an avaliable Code", preferredStyle: .alert)
                    
                    alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                    self.present(alert, animated: true, completion: nil)
                    
                }
            }
            
        })
    }
    
    func customerSeatedToDB(restaurantUid:String,restaurantTable:String){
        
        var userUid = ""
        var userName = ""
        if let uUid = Auth.auth().currentUser?.uid{
            userUid = uUid
        }
        if let userNameDisplay = Auth.auth().currentUser?.displayName{
            userName = userNameDisplay
        }
        
        print("current table \(restaurantTable)")
        print("current restaurant uid \(restaurantUid)")
        let ref = Database.database().reference().child("Restaurants").child(restaurantUid).child("Tables").child("Table\(restaurantTable)")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            
            if let value = snapshot.value as? [String:Any]{
               //there is someone at the table
                print("snapshot value: \(value)")
            
                print("number of keys: \(value.keys.count)")

//                if more than 1 user sitted at the table
                
//                if value.keys.count > 1{
                    for id in value.keys{

                        print("v: \(id)")
                        
                        if userUid == id{
                            //current user is already at the
                            print("user is sitted at table")
                            return
                        }

                    }
                
                    print("new user")
                    let key = value.keys.count
                ref.updateChildValues([userUid:["tableStatus":"Seated","userUid":userUid,"tableNumber":restaurantTable,"userName":userName]])

                
            }else{
                //no one at the table (table does not exist)
                print("snapshot no one at the table")
                ref.updateChildValues([userUid:["tableStatus":"Seated","userUid":userUid,"tableNumber":restaurantTable,"userName":userName]])
//                ref.child("[0]").updateChildValues(["tableStatus":"Seated","userUid":userUid,"tableNumber":restaurantTable])
            }
            
        }
    
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        hud.dismiss(afterDelay: 0)
        
        if segue.identifier == goToMenuSegueId{
            
            let nav = segue.destination as? UINavigationController
            
            let menuVC = nav!.topViewController as! MenuItemsViewController

            menuVC.getRestaurantMenu = restaurantMenu
            
            if !userOrder.isEmpty{
                menuVC.itemsOrdered = userOrder
            }
            
        }
    }
    func setMessage(body:String,error:Bool){
        
        let view = MessageView.viewFromNib(layout: .centeredView)
        
        // Theme message elements with the warning style.
        if error{
            view.configureTheme(.error)
        }else{
            view.configureTheme(.info)
        }
        
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        view.configureContent(title: "Message", body: body)
        
        view.button?.isHidden = true
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
        
    }
    
    func setGradientBackground() -> CAGradientLayer {
        let colorTop =  UIColor(red: 182/255.0, green: 77/255.0, blue: 67/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 243/255.0, green: 198/255.0, blue: 143/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        
        return gradientLayer
    }
    
    func checkForSplitBill(){
    
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            hud.textLabel.text = "Matching Location"

            let restaurantUid = UserDefaults.standard.string(forKey: restaurantUidKey)
            let restaurantTable = UserDefaults.standard.string(forKey: restaurantTableNumber)
            
            let restRef = Database.database().reference().child("Restaurants").child(restaurantUid!).child("location")
            
            restRef.observeSingleEvent(of: .value, with: { (snapshot) in
                
                print("snapshot:: \(snapshot)")
                if let value = snapshot.value as? [String:Any]{
                    
                    
                    print("value::\(value)")
                    
                    let rLatitude = value["latitude"] as! String
                    let rLongitude = value["longitude"] as! String
                    
                    print(" resturant latitude: \(rLatitude)")
                    
                    let distances = self.distance(lat1: Double(rLatitude)!, lon1: Double(rLongitude)!, lat2: Double(location.coordinate.latitude), lon2: location.coordinate.longitude, unit: "K")
                    print("Distance: \(distances)")
                    
                    if distances <= 10{
                        self.getRestaurantMenu(restaurantUid: restaurantUid!,restTable: restaurantTable!)
                    }else{
                        self.hud.dismiss()
                        
                        let alert = UIAlertController(title: "Distance Error", message: "The current location does not match with the restaurant location, Please alert the staff.", preferredStyle: .alert)

                        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))

                        self.present(alert, animated: true, completion: nil)
                    }
                }
                
                
            }, withCancel: nil)
            
            
            
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
        hud.dismiss(animated: true)
    }
    
    func rad2deg(rad:Double) -> Double {
        return rad * 180.0 / .pi
    }
    func deg2rad(deg:Double) -> Double {
        return deg * .pi / 180
    }
    
    func distance(lat1:Double, lon1:Double, lat2:Double, lon2:Double, unit:String) -> Double {
        let theta = lon1 - lon2
        var dist = sin(deg2rad(deg: lat1)) * sin(deg2rad(deg:lat2)) + cos(deg2rad(deg:lat1)) * cos(deg2rad(deg:lat2)) * cos(deg2rad(deg: theta))
        dist = acos(dist)
        dist = rad2deg(rad: dist)
        dist = dist * 60 * 1.1515
        if (unit == "K") {
            dist = dist * 1.609344
        }
        else if (unit == "N") {
            dist = dist * 0.8684
        }
        return dist
    }
    
}
