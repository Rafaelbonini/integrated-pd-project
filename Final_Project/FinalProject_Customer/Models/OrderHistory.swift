//
//  orderHistory.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/24/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import Foundation

class OrderHistory{
    
    let restaurantName: String
    let restaurantUid : String
    let tableNum : String
    let timeStamp : Double
    let order:[ItemFromOrder]
    let totalOrderCost:Double
    
    init(restaurantName: String, restaurantUid : String, tableNum : String, timeStamp : Double,order:[ItemFromOrder],totalOrderCost:Double) {
        
        self.restaurantName = restaurantName
        self.restaurantUid = restaurantUid
        self.tableNum = tableNum
        self.timeStamp = timeStamp
        self.order = order
        self.totalOrderCost = totalOrderCost
    }
}
