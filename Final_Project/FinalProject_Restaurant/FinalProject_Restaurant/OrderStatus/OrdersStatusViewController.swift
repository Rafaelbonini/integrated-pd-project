//
//  OrdersStatusViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/31/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase

private let cellId = "CellId"
private let detailsSegueId = "detailsIdentifier"
private let orderHistorySegueId = "toOrderHistory"
class OrdersStatusViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    var selectedTableOrderIndex = -1
    
    var databaseRef = Database.database().reference()
    
    var waiterRequest :[String:Bool] = [:]
    var managerRequest : [String:Bool] = [:]
    
    @IBOutlet weak var segmentedOrderStatus: UISegmentedControl!
    
    var paidOrders : [tableOrder] = []
    var openOrders : [tableOrder] = []
    var updatedOrders : [OrdersInTableData] = []
    var newOpenOrders : [OrdersInTableData] = []
    var newPaidOrders : [OrdersInTableData] = []
    var tablesInfo : [TablesData] = []
    
    
//    let bla = tableOrder(userUid: <#T##String#>, tableStatus: <#T##String#>, tableNum: <#T##Int#>, items: <#T##[ItemFromOrder]#>, timeStamp: <#T##Int#>)
//    let blabla = TablesData(tableStatus: <#T##String#>, tableNum: <#T##Int#>, customersTableData: <#T##[OrdersInTableData]#>)
//    let blablabla = OrdersInTableData(userUid: <#T##String#>, userName: <#T##String#>, tableStatus: <#T##String#>, tableNum: <#T##Int#>, items: <#T##[ItemFromOrder]#>, timeStamp: <#T##Int#>)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        getInitialDataFromDB()
        keepDataUpdated()
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
    
        print("segmentedcount index \(segmentedOrderStatus.selectedSegmentIndex)")
        if segmentedOrderStatus.selectedSegmentIndex == 0{
            return updatedOrders.count
        }else if segmentedOrderStatus.selectedSegmentIndex == 1{
            return self.newOpenOrders.count
        }else if segmentedOrderStatus.selectedSegmentIndex == 2{
            return self.newPaidOrders.count
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! OrderStatusCellTableViewCell
        
        
        
        if segmentedOrderStatus.selectedSegmentIndex == 0{
            cell.orderInfo = updatedOrders[indexPath.row]
        }else if segmentedOrderStatus.selectedSegmentIndex == 1{
            cell.orderInfo = newOpenOrders[indexPath.row]
        }else if segmentedOrderStatus.selectedSegmentIndex == 2{
            cell.orderInfo = newPaidOrders[indexPath.row]
        }
        
        
        
        return cell
    }
    
    
    func getInitialDataFromDB(){
        
        tablesInfo = []
        
        guard let restaurantUid = Auth.auth().currentUser?.uid else{
            return
        }
        
        // get all orders active in tables
        let ref = Database.database().reference().child("Restaurants").child(restaurantUid).child("Tables")
        ref.observeSingleEvent(of: .value) { (snapshot) in
            
            if let dictionary = snapshot.value as? [String:AnyObject]{
                
                print("dictionary: \(dictionary)")
                
                for tables in dictionary{
                    
                    print("tables: \(tables)")
                    
                    if let table = dictionary[tables.key] as? [String:AnyObject] {
                        
                        var tableCustomers : [OrdersInTableData] = []
                        var status = ""
                        
                        for customers in table.values{
                            
                            print("customers: \(customers)")
//                            print("customers: \(customers)")
                            
                            if let customersValue =  customers as? Bool{

                                print(customersValue)

                            }else{

                                let userUid = customers["userUid"] as? String
                                let tableStatus = customers["tableStatus"] as? String
                                let tableNumber = customers["tableNumber"] as? String
                                let timeStamp = customers["timeStamp"] as? Int
                                let userName = customers["userName"] as? String
                                
                                if let tableStat = tableStatus{
                                    
                                    if tableStat == "Seated"{
                                        status = "Seated"
                                        break;
                                    }else{
                                        status = ""
                                    }
                                }

                                var itemsFromOrder : [ItemFromOrder] = []
                                
                                if let orders = customers["order"] as? [String:AnyObject]{
                                    
                                    for items in orders{
                                        
                                        if let item = orders[items.key] as? [String:AnyObject]{
                                            
                                            if let dishName = item["dishName"] as? String,let dishPrice = item["dishPrice"] as? Double,let dishQuantity = item["dishQuantity"] as? Int,let servWFood = item["servWFood"] as? Bool{
                                                
                                                itemsFromOrder.append(ItemFromOrder(dishName: dishName, dishPrice: dishPrice, dishQuantity: dishQuantity, servWFood: servWFood))
                                                
                                                print("dishName \(dishName), dishPrice: \(dishPrice), dishQuantity: \(dishQuantity), servWFood: \(servWFood)")
                                                
                                            }
                                        }
                                    }
                                }
                                
                                
                                
                                if let tableNum = Int(tableNumber!), let tablestatus = tableStatus, let userid = userUid, let timestamp = timeStamp, let userN = userName{
                                    
                                    
                                    print("useruid: \(userUid),status: \(tableStatus),numbert: \(tableNumber),timestamp: \(timeStamp), userName: \(userN)")
                                    
                                    
                                    
                                    let openOrder = OrdersInTableData(userUid: userid, userName: userN, tableStatus: tablestatus, tableNum: tableNum, items: itemsFromOrder, timeStamp: timestamp)
                                    
                                    tableCustomers.append(openOrder)
                                    
                                    self.newOpenOrders.append(openOrder)
                                    
                                    
                                }
                            }
                            
                        }

                        
                        
                        if status != "Seated"{
                            let tableNum = tableCustomers[0].tableNum
                            
                            self.tablesInfo.append(TablesData(tableStatus: "Order Placed", tableNum: tableNum, customersTableData: tableCustomers))
                        }
                            
                        
                        

                        
                        
                        for items in self.tablesInfo{
                            
                            for bla in items.customersTableData{
                                
                                print("blabla; \(bla.userUid)")
                                print("blabla; \(bla.tableNum)")
                                print("blabla; \(bla.tableStatus)")
                                print("blabla; \(bla.userName)")
                            }
                        }
                        
                    }
                }
            }
            
            //reload table here
            self.tableView.reloadData()
            
            
            for item in self.updatedOrders{
                print("tablenum: \(item.tableNum)")
            }
        }
        
        //get all orders that have been paid
        let refR = Database.database().reference().child("Restaurants").child(restaurantUid).child("orderHistory")
        
        //get all orders uid saved to the user
        refR.observe(.childAdded, with: { (snapshot) in
            print(snapshot.key)
            
            let orderId = snapshot.key
            let orderReference = Database.database().reference().child("Orders").child(orderId)
            
            //use orders uid to get each order information
            orderReference.observeSingleEvent(of: .value, with: { (snapshot) in
            
                if let value = snapshot.value as? [String:Any]{
                    
                    //check if request node for waiter exists
                    if let tableWaiterRequest = value["WaiterRequest"] as? Bool, let tableNum = value["tableNum"] as? String{
                        
                        //if exists check if is true
                        if tableWaiterRequest{
                            //a waiter has been requested, save table number that requested
                            self.waiterRequest[tableNum] = true
                            
                            //inform that the table has requested the waiter with an alert
                            let alert = UIAlertController(
                                title: "Staff Request",
                                message: String (format:"Table %@ is requesting a Waiter!", tableNum),
                                preferredStyle: .alert
                            )
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                        }else{
                            //the node exists but has already been dismissed (restaurant market the request as solved)
                            self.waiterRequest[tableNum] = false
                        }
                        
                    }else if let tableManagerRequest = value["managerRequest"] as? Bool, let tableNum = value["tableNum"] as? String{
                        //managerRequest
                        
                        //if exists check if is true
                        if tableManagerRequest{
                            //a manager has been requested, save table number that requested
                            self.managerRequest[tableNum] = true
                            
                            //inform that the table has requested the Manager with an alert
                            let alert = UIAlertController(
                                title: "Staff Request",
                                message: String (format:"Table %@ is requesting a Manager!", tableNum),
                                preferredStyle: .alert
                            )
                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                            self.present(alert, animated: true, completion: nil)
                            
                        }else{
                            //the node exists but has already been dismissed (restaurant market the request as solved)
                            self.managerRequest[tableNum] = false
                        }
                        
                    }

                    if let customerUid = value["customerUid"] as? String, let restaurantName = value["restaurantName"] as? String, let restaurantUid = value["restaurantUid"] as? String, let tableNum = value["tableNum"] as? String, let timeStamp = value["timeStamp"] as? Int,let orderCost = value["orderTotalPrice"] as? Double, let orderDic = value["order"] as? [String:Any] {
                        
                        var orderItems : [ItemFromOrder] = []
                        for items in orderDic{
                            
                            if let item = orderDic[items.key] as? [String:Any]{
                                
                                if let dishName = item["dishName"] as? String, let dishPrice = item["dishPrice"] as? Double, let dishQuantity = item["dishQuantity"] as? Int, let servWFood = item["servWFood"] as? Bool{
                                    
                                    
                                    orderItems.append(ItemFromOrder(dishName: dishName, dishPrice: dishPrice, dishQuantity: dishQuantity, servWFood: servWFood))
                                    
                                    print("\(items.key)=  \(dishName):\(dishPrice):\(dishQuantity):\(servWFood)")
                                }
                            }
                        }
                        
//                        self.paidOrders.append(tableOrder(userUid: customerUid, tableStatus: "", tableNum: Int(tableNum)!, items: orderItems, timeStamp: timeStamp))
                        
                        if let userName = value["userName"] as? String{
                            self.newPaidOrders.append(OrdersInTableData(userUid: customerUid, userName: userName, tableStatus: "Order Paid", tableNum: Int(tableNum)!, items: orderItems, timeStamp: timeStamp))
                        }else{
                            self.newPaidOrders.append(OrdersInTableData(userUid: customerUid, userName: "", tableStatus: "Order Paid", tableNum: Int(tableNum)!, items: orderItems, timeStamp: timeStamp))
                        }
                        
                        
                    }
                    
                }
                
                //RELOAD TABLE WITH SORTED BY DATE LIST
                for item in self.newPaidOrders{
                    print("new updateorder: \(item.tableStatus)")
                }
                
                self.newPaidOrders = self.newPaidOrders.sorted(by: { $0.timeStamp > $1.timeStamp })
                self.newOpenOrders = self.newOpenOrders.sorted(by: { $0.timeStamp > $1.timeStamp })
                
                self.updatedOrders = []
                self.updatedOrders.append(contentsOf: self.newPaidOrders)
                self.updatedOrders.append(contentsOf: self.newOpenOrders)
                self.updatedOrders = self.updatedOrders.sorted(by: { $0.timeStamp > $1.timeStamp })
                print("counter: \(self.updatedOrders.count)")

                self.tableView.reloadData()
                
            }, withCancel: nil)
            
            
       
        }, withCancel: nil)
       
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

        selectedTableOrderIndex = indexPath.row
        
        //filter if if is an open order or an paid order and from what tab it comes from
        if segmentedOrderStatus.selectedSegmentIndex == 2{
            self.performSegue(withIdentifier: orderHistorySegueId, sender: nil)
        }else if(segmentedOrderStatus.selectedSegmentIndex == 1){
            self.performSegue(withIdentifier: detailsSegueId, sender: nil)
        }else{
            
            if updatedOrders[indexPath.row].tableStatus == "Order Placed"{
                self.performSegue(withIdentifier: detailsSegueId, sender: nil)
            }else{
                self.performSegue(withIdentifier: orderHistorySegueId, sender: nil)
            }
            
            
            
        }
        
        
        
        
        
        
        print("selected: \(updatedOrders[indexPath.row].userUid)")
        tableView.deselectRow(at: indexPath, animated: true)
    }
    @IBAction func segmentedValueChanged(_ segmentedControl: UISegmentedControl) {
        
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            //display all orders
            
            print("blabla")
            tableView.reloadData()
        case 1:
            //display open orders
            print("blabla")
            tableView.reloadData()
        case 2:
            //display paid orders
            tableView.reloadData()
            print("blabla")
        default:
            break;
        }
        
        
    }
    
    func keepDataUpdated(){
        
        guard let restaurantUid = Auth.auth().currentUser?.uid else{
            return
        }
        

        
         databaseRef = Database.database().reference().child("Restaurants").child(restaurantUid).child("Tables")

        // get all orders active in tables
       
        databaseRef.observe(.childChanged, with: { (snapshot) in
            
            if let dic = snapshot.value as? [String:Any]{
    
            let userUid = dic["userUid"] as? String
            let tableStatus = dic["tableStatus"] as? String
            let tableNumber = dic["tableNumber"] as? String
            let timeStamp = dic["timeStamp"] as? Int
                
                
                //check if request node for waiter exists
                if let tableWaiterRequest = dic["WaiterRequest"] as? Bool, let tableNum = tableNumber{
                    
                    //if exists check if is true
                    if tableWaiterRequest{
                        //a waiter has been requested, save table number that requested
                        self.waiterRequest[tableNum] = true
                        
                        //inform that the table has requested the waiter with an alert
                        let alert = UIAlertController(
                            title: "Staff Request",
                            message: String (format:"Table %@ is requesting a Waiter!", tableNum),
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        //the node exists but has already been dismissed (restaurant market the request as solved)
                        self.waiterRequest[tableNum] = false
                    }
                    
                }else if let tableManagerRequest = dic["managerRequest"] as? Bool, let tableNum = tableNumber{
                    //managerRequest
                    
                    //if exists check if is true
                    if tableManagerRequest{
                        //a manager has been requested, save table number that requested
                        self.managerRequest[tableNum] = true
                        
                        //inform that the table has requested the Manager with an alert
                        let alert = UIAlertController(
                            title: "Staff Request",
                            message: String (format:"Table %@ is requesting a Manager!", tableNum),
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        //the node exists but has already been dismissed (restaurant market the request as solved)
                        self.managerRequest[tableNum] = false
                    }
                    
                }
                
                
                
                
                
                //TODO: - UPDATE
                //fix this to new databasestructure
                
                
            
            print("useruid: \(userUid),status: \(tableStatus),numbert: \(tableNumber),timestamp: \(timeStamp)")
            var itemsFromOrder : [ItemFromOrder] = []
            
            if let orders = dic["order"] as? [String:AnyObject]{

                for items in orders{

                    if let item = orders[items.key] as? [String:AnyObject]{

                        if let dishName = item["dishName"] as? String,let dishPrice = item["dishPrice"] as? Double,let dishQuantity = item["dishQuantity"] as? Int,let servWFood = item["servWFood"] as? Bool{

                            itemsFromOrder.append(ItemFromOrder(dishName: dishName, dishPrice: dishPrice, dishQuantity: dishQuantity, servWFood: servWFood))

                            print("dishName \(dishName), dishPrice: \(dishPrice), dishQuantity: \(dishQuantity), servWFood: \(servWFood)")

                        }
                    }
                }
                
                }
                
                if let tableNum = tableNumber, let tablestatus = tableStatus, let userid = userUid, let timestamp = timeStamp{

                    if self.openOrders.contains(where: { $0.timeStamp == timestamp }) {
                        
                    }else{
                        
                    self.openOrders.append(tableOrder(userUid: userid, tableStatus: tablestatus, tableNum: Int(tableNum)!, items: itemsFromOrder, timeStamp: timestamp))
                        
                        
                        self.updatedOrders.append(OrdersInTableData(userUid: userid, userName: "", tableStatus: tablestatus, tableNum:Int(tableNum)! , items: itemsFromOrder, timeStamp: timestamp))
//                        self.updatedOrders.append(tableOrder(userUid: userid, tableStatus: tablestatus, tableNum: Int(tableNum)!, items: itemsFromOrder, timeStamp: timestamp))
                        
                        self.openOrders = self.openOrders.sorted(by: { $0.timeStamp > $1.timeStamp })
                        self.updatedOrders = self.updatedOrders.sorted(by: { $0.timeStamp > $1.timeStamp })
                        self.tableView.reloadData()
                    }
   
                }
            }

        }) { (error) in
            print(error)
        }

        databaseRef = Database.database().reference().child("Restaurants").child(restaurantUid).child("Tables")
        
        // itemRemoved
        
        databaseRef.observe(.childRemoved, with: { (snapshot) in
            
            if let dic = snapshot.value as? [String:Any]{
                
                let userUid = dic["userUid"] as? String
                let tableStatus = dic["tableStatus"] as? String
                let tableNumber = dic["tableNumber"] as? String
                let timeStamp = dic["timeStamp"] as? Int
                
                print("useruid: \(userUid),status: \(tableStatus),numbert: \(tableNumber),timestamp: \(timeStamp)")
                var itemsFromOrder : [ItemFromOrder] = []
//
//                if let orders = dic["order"] as? [String:AnyObject]{
//
//                    for items in orders{
//
//                        if let item = orders[items.key] as? [String:AnyObject]{
//
//                            if let dishName = item["dishName"] as? String,let dishPrice = item["dishPrice"] as? Double,let dishQuantity = item["dishQuantity"] as? Int,let servWFood = item["servWFood"] as? Bool{
//
//                                itemsFromOrder.append(ItemFromOrder(dishName: dishName, dishPrice: dishPrice, dishQuantity: dishQuantity, servWFood: servWFood))
//
//                                print("dishName \(dishName), dishPrice: \(dishPrice), dishQuantity: \(dishQuantity), servWFood: \(servWFood)")
//
//                            }
//                        }
//                    }
//
//                }
                
                if let tableNum = tableNumber, let tablestatus = tableStatus, let userid = userUid, let timestamp = timeStamp{
                    
                    
                    
                    for (index,_) in self.openOrders.enumerated().reversed(){
                        
                        if self.openOrders[index].timeStamp == timestamp && self.openOrders[index].tableNum == Int(tableNum){
                            
                            self.openOrders.remove(at: index)
                            
                        }
                        
                        
                    }
                    
                    self.updatedOrders.removeAll()
                    self.updatedOrders.append(contentsOf: self.newOpenOrders)
                    self.updatedOrders.append(contentsOf: self.newPaidOrders)
                    
                    self.updatedOrders = self.updatedOrders.sorted(by: { $0.timeStamp > $1.timeStamp })
                    self.paidOrders = self.paidOrders.sorted(by: { $0.timeStamp > $1.timeStamp })
                    self.openOrders = self.openOrders.sorted(by: { $0.timeStamp > $1.timeStamp })
                    
                }
            }
            
        }) { (error) in
            print(error)
        }
        
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
//        databaseRef.removeAllObservers()
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == detailsSegueId{
            
            //order selected is an open order
            let detailsVc = segue.destination as! TableDetailsViewController
            
            //it is an order from the "all" tab
            if segmentedOrderStatus.selectedSegmentIndex == 0{
            
                let tableNum = updatedOrders[selectedTableOrderIndex].tableNum
                
                for item in self.tablesInfo{
                    
                    if item.tableNum == tableNum{
                        
                        detailsVc.getTableInfo = item
                    }
                    
                }
                
                //it is an order from the "open" tab
            }else if segmentedOrderStatus.selectedSegmentIndex == 1{
                
               
                let tableNum = newOpenOrders[selectedTableOrderIndex].tableNum
                
                for item in self.tablesInfo{
                    
                    if item.tableNum == tableNum{
                        
                        detailsVc.getTableInfo = item
                    }
                    
                }
             
            }
            
            if let waiterRequestForTable = waiterRequest[String(selectedTableOrderIndex+1)]{
                
                print(waiterRequestForTable)
                detailsVc.getWaiterRequested = waiterRequestForTable
            }else if let managerRequestForTable = managerRequest[String(selectedTableOrderIndex+1)]{
                
                print(managerRequestForTable)
                detailsVc.getManagerRequested = managerRequestForTable
            }
            
        }else if segue.identifier == orderHistorySegueId{
            
            //the order selected is a order that has been paid
            
            //it is an order from the "all" tab
            if segmentedOrderStatus.selectedSegmentIndex == 0{
                
                let historyVc = segue.destination as! DetailsOrderHistoryViewController
                
                guard let userUid = Auth.auth().currentUser?.uid else{
                    return
                }
                guard let userName = Auth.auth().currentUser?.displayName else{
                    return
                }
                
                var totalCost = 0.0
                let order = updatedOrders[selectedTableOrderIndex].items
                
                for item in order{
                    
                    totalCost += item.dishPrice * Double(item.dishQuantity)
                }
                
                
                let orderInfo = OrderHistory(restaurantName: userName, restaurantUid: userUid, customerUid: updatedOrders[selectedTableOrderIndex].userUid, tableNum: String(updatedOrders[selectedTableOrderIndex].tableNum), timeStamp: Double(updatedOrders[selectedTableOrderIndex].timeStamp), order: updatedOrders[selectedTableOrderIndex].items, totalOrderCost: totalCost)
                
                historyVc.getSelectedOrder = orderInfo
                
                //order from "paid" tab
            }else if segmentedOrderStatus.selectedSegmentIndex == 2{
                
                let historyVc = segue.destination as! DetailsOrderHistoryViewController
                
                guard let userUid = Auth.auth().currentUser?.uid else{
                    return
                }
                guard let userName = Auth.auth().currentUser?.displayName else{
                    return
                }
                
                var totalCost = 0.0
                let order = newPaidOrders[selectedTableOrderIndex].items
                
                for item in order{
                    
                    totalCost += item.dishPrice * Double(item.dishQuantity)
                }
                
                
                let orderInfo = OrderHistory(restaurantName: userName, restaurantUid: userUid, customerUid: newPaidOrders[selectedTableOrderIndex].userUid, tableNum: String(newPaidOrders[selectedTableOrderIndex].tableNum), timeStamp: Double(newPaidOrders[selectedTableOrderIndex].timeStamp), order: newPaidOrders[selectedTableOrderIndex].items, totalOrderCost: totalCost)
                
                historyVc.getSelectedOrder = orderInfo
            }
        }
    }
}

