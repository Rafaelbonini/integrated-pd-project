//
//  OrderHistoryViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/25/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
import PNChart
import JGProgressHUD

private let cellId = "CellId"
private let detailSegueid = "detailsSegueId"
class OrderHistoryViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    let hud = JGProgressHUD(style: .dark)
    
    @IBOutlet weak var dataVisualizationSegment: UISegmentedControl!
    @IBOutlet weak var chartVc: UIView!
    @IBOutlet weak var tableView: UITableView!
    
    var chart = PNLineChart(frame: CGRect(x: 10, y: 0, width: UIScreen.main.bounds.width, height: 100))
    let chartData = PNLineChartData()
    
    var orderHistoryItems : [OrderHistory] = []
    var selectedItemIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hud.detailTextLabel.text = "Loading Orders"
        hud.show(in: self.view)
        
        
        
        chartVc.addSubview(chart)
        chartVc.clipsToBounds = true
        chart.frame = CGRect(x: 10, y: 30 , width: UIScreen.main.bounds.width, height: chartVc.frame.height)
        

        
        chart.xLabelFont = UIFont(name: "Avenir Medium", size: 15)
        chartData.inflexionPointStyle = PNLineChartPointStyle.circle
        chartData.inflexionPointColor = UIColor.red
        chart.showSmoothLines = true
        chart.showYGridLines = true
        chart.yLabelFont = UIFont(name: "Avenir Medium", size: 8)
        chart.yGridLinesColor = UIColor.gray
        chart.yLabelFormat = "$%1.f"
        chartData.color = UIColor.green

        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        guard let currentUserUid = Auth.auth().currentUser?.uid else{
            return
        }
        
        print(currentUserUid)
        let ref = Database.database().reference().child("Restaurants").child(currentUserUid).child("orderHistory")
        
        //get all orders uid saved to the user
        ref.observe(.childAdded, with: { (snapshot) in
            print(snapshot.key)
            
            let orderId = snapshot.key
            let orderReference = Database.database().reference().child("Orders").child(orderId)
            
            //use orders uid to get each order information
            orderReference.observeSingleEvent(of: .value, with: { (snapshot) in
                //                print(snapshot)
                if let value = snapshot.value as? [String:Any]{
                    
                    if let customerUid = value["customerUid"] as? String, let restaurantName = value["restaurantName"] as? String, let restaurantUid = value["restaurantUid"] as? String, let tableNum = value["tableNum"] as? String, let timeStamp = value["timeStamp"] as? Double,let orderCost = value["orderTotalPrice"] as? Double, let orderDic = value["order"] as? [String:Any] {
                        
                        var orderItems : [ItemFromOrder] = []
                        for items in orderDic{
                            
                            if let item = orderDic[items.key] as? [String:Any]{
                                
                                if let dishName = item["dishName"] as? String, let dishPrice = item["dishPrice"] as? Double, let dishQuantity = item["dishQuantity"] as? Int, let servWFood = item["servWFood"] as? Bool{
                                    
                                    
                                    orderItems.append(ItemFromOrder(dishName: dishName, dishPrice: dishPrice, dishQuantity: dishQuantity, servWFood: servWFood))
                                    
                                    print("\(items.key)=  \(dishName):\(dishPrice):\(dishQuantity):\(servWFood)")
                                }
                            }
                        }
                        
                        self.orderHistoryItems.append(OrderHistory(restaurantName: restaurantName, restaurantUid: restaurantUid, customerUid: customerUid, tableNum: tableNum, timeStamp: timeStamp, order: orderItems, totalOrderCost: orderCost))
                        
                        self.orderHistoryItems = self.orderHistoryItems.sorted(by: { $0.timeStamp > $1.timeStamp })
                        print("\(customerUid):\(restaurantName):\(restaurantUid):\(tableNum):\(timeStamp)")
                        
                        
                        
                    }
                }
                self.tableView.reloadData()
                self.dataVisualizationDay()
                self.hud.dismiss(animated: true)
                
            }, withCancel: nil)
            
            
        }, withCancel: nil)
        
        
        
    }
    
    @IBAction func updateDataVisualization(_ segmentedControl: UISegmentedControl) {
        
        switch segmentedControl.selectedSegmentIndex
        {
        case 0:
            //display data visualization by day
            self.dataVisualizationDay()
        case 1:
            //display data visualization by week
            self.dataVisualizationWeek()
        
        case 2:
            //display data visualization by month
            self.dataVisualizationMonth()
        default:
            break;
        }
    }
    

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return orderHistoryItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! OrderHistoryTableViewCell
        
        cell.restaurantInfo = orderHistoryItems[indexPath.row]
        cell.orderNumLabel.text = "Order #\(orderHistoryItems.count-indexPath.row)"
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        selectedItemIndex = indexPath.row
        performSegue(withIdentifier: detailSegueid, sender: nil)
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == detailSegueid{
            
            let detailsVC = segue.destination as! DetailsOrderHistoryViewController
            
            detailsVC.getSelectedOrder = orderHistoryItems[selectedItemIndex]
            
        }
    }
    
    
    
    func dataVisualizationDay(){
        
        
        let currentDate = Date().timeIntervalSince1970
        
        let currentDateMinus1 = Date().timeIntervalSince1970 - 86400
        
        let currentDateMinus2 = currentDateMinus1 - 86400
        
        let currentDateMinus3 = currentDateMinus2 - 86400
        
        let currentDateMinus4 = currentDateMinus3 - 86400
        
        let currentDateMinus5 = currentDateMinus4 - 86400
        
        let currentDateMinus6 = currentDateMinus5 - 86400
        
        var chartLabels : [String] = [currentDate.convertToStringDate(),currentDateMinus1.convertToStringDate(),currentDateMinus2.convertToStringDate(),currentDateMinus3.convertToStringDate(),currentDateMinus4.convertToStringDate(),currentDateMinus5.convertToStringDate(),currentDateMinus6.convertToStringDate()]

        var incomeValues : [String:Double] = [currentDate.convertToStringDate():0,currentDateMinus1.convertToStringDate():0,currentDateMinus2.convertToStringDate():0,currentDateMinus3.convertToStringDate():0,currentDateMinus4.convertToStringDate():0,currentDateMinus5.convertToStringDate():0,currentDateMinus6.convertToStringDate():0]
        
        for item in orderHistoryItems{
            
            print(item.timeStamp.convertToStringDate())
            print("'currentdate \(currentDate.convertToStringDate())")
            print(" minus 1\(currentDateMinus1.convertToStringDate())")
            print("minus 2 \(currentDateMinus2.convertToStringDate())")
            
            if item.timeStamp.convertToStringDate() == currentDate.convertToStringDate(){
                
                let dayIncome = incomeValues[item.timeStamp.convertToStringDate()]
                
                incomeValues[item.timeStamp.convertToStringDate()] = dayIncome! + item.totalOrderCost

                print("current day \(item.timeStamp.convertToStringDate()) income is \(incomeValues)")

                
            }else if item.timeStamp.convertToStringDate() == currentDateMinus1.convertToStringDate(){
                

                let dayIncome = incomeValues[item.timeStamp.convertToStringDate()]
                
                incomeValues[item.timeStamp.convertToStringDate()] = dayIncome! + item.totalOrderCost
                
                print("current day \(item.timeStamp.convertToStringDate()) income is \(incomeValues)")
                
            }else if item.timeStamp.convertToStringDate() == currentDateMinus2.convertToStringDate(){
                
                let dayIncome = incomeValues[item.timeStamp.convertToStringDate()]
                
                incomeValues[item.timeStamp.convertToStringDate()] = dayIncome! + item.totalOrderCost
                
                print("current day \(item.timeStamp.convertToStringDate()) income is \(incomeValues)")
                
            }else if item.timeStamp.convertToStringDate() == currentDateMinus3.convertToStringDate(){
                
                let dayIncome = incomeValues[item.timeStamp.convertToStringDate()]
                
                incomeValues[item.timeStamp.convertToStringDate()] = dayIncome! + item.totalOrderCost
                
            }else if item.timeStamp.convertToStringDate() == currentDateMinus4.convertToStringDate(){
                
                let dayIncome = incomeValues[item.timeStamp.convertToStringDate()]
                
                incomeValues[item.timeStamp.convertToStringDate()] = dayIncome! + item.totalOrderCost
                
            }else if item.timeStamp.convertToStringDate() == currentDateMinus5.convertToStringDate(){
                
                let dayIncome = incomeValues[item.timeStamp.convertToStringDate()]
                
                incomeValues[item.timeStamp.convertToStringDate()] = dayIncome! + item.totalOrderCost
                
            }else if item.timeStamp.convertToStringDate() == currentDateMinus6.convertToStringDate(){
                
                let dayIncome = incomeValues[item.timeStamp.convertToStringDate()]
                
                incomeValues[item.timeStamp.convertToStringDate()] = dayIncome! + item.totalOrderCost
                
            }
            
        
        }
        

        
        var data01Array : [NSNumber] = [NSNumber(value: incomeValues[currentDate.convertToStringDate()]!),NSNumber(value: incomeValues[currentDateMinus1.convertToStringDate()]!),NSNumber(value: incomeValues[currentDateMinus2.convertToStringDate()]!),NSNumber(value: incomeValues[currentDateMinus3.convertToStringDate()]!),NSNumber(value: incomeValues[currentDateMinus4.convertToStringDate()]!),NSNumber(value: incomeValues[currentDateMinus5.convertToStringDate()]!),NSNumber(value: incomeValues[currentDateMinus6.convertToStringDate()]!)]
        data01Array = data01Array.reversed()
        chartLabels = chartLabels.reversed()
//        chartData.color = UIColor.green
//        chartData.dataTitle = "Tittle"
        chartData.itemCount = UInt(chartLabels.count)
        
        chartData.getData = { index in
            let yValue = CGFloat(data01Array[Int(index)].floatValue)
            print("index: \(index)")
            print("yvalye \(yValue)")
            return PNLineChartDataItem(y: yValue)
        }
        chart.setXLabels(chartLabels, withWidth: CGFloat(chart.frame.width/CGFloat(integerLiteral: data01Array.count+1)))

        chart.chartData = [chartData]
        
        
        chart.stroke()
        
        
    }
    
    func dataVisualizationWeek(){
 
        let currentDate = Date().timeIntervalSince1970
        let oneWeekAgo = currentDate-604800
        let twoWeekAgo = oneWeekAgo-604800
        let threeWeekAgo = twoWeekAgo-604800
        let fourWeekAgo = threeWeekAgo-604800
        
        var chartLabels : [String] = ["-4Week","-3Week","-2Week","Last 7 Days"]
        
        var incomeValues : [String:Double] = [chartLabels[0]:0,chartLabels[1]:0,chartLabels[2]:0,chartLabels[3]:0]
        
        
        for item in orderHistoryItems{
            
            if item.timeStamp < currentDate && item.timeStamp > oneWeekAgo{
                
                print("currentWeek")
                
                let dayIncome = incomeValues[chartLabels[0]]
                
                incomeValues[chartLabels[0]] = dayIncome! + item.totalOrderCost
                
            }else if item.timeStamp < oneWeekAgo && item.timeStamp > twoWeekAgo{
                
                print("LastWeek")
                let dayIncome = incomeValues[chartLabels[1]]
                
                incomeValues[chartLabels[1]] = dayIncome! + item.totalOrderCost
                
            }else if item.timeStamp < twoWeekAgo && item.timeStamp > threeWeekAgo{
                
                let dayIncome = incomeValues[chartLabels[2]]
                
                incomeValues[chartLabels[2]] = dayIncome! + item.totalOrderCost
                
            }else if item.timeStamp < threeWeekAgo && item.timeStamp > fourWeekAgo{
                
                let dayIncome = incomeValues[chartLabels[3]]
                
                incomeValues[chartLabels[3]] = dayIncome! + item.totalOrderCost
            }
            
        }
        
        var data01Array : [NSNumber] = [NSNumber(value: incomeValues[chartLabels[0]]!),NSNumber(value: incomeValues[chartLabels[1]]!),NSNumber(value: incomeValues[chartLabels[2]]!),NSNumber(value: incomeValues[chartLabels[3]]!)]
        data01Array = data01Array.reversed()
        
        chartData.itemCount = UInt(chartLabels.count)
        
        chartData.getData = { index in
            let yValue = CGFloat(data01Array[Int(index)].floatValue)
            print("index: \(index)")
            print("yvalye \(yValue)")
            return PNLineChartDataItem(y: yValue)
        }

        chart.setXLabels(chartLabels, withWidth: CGFloat(chart.frame.width/CGFloat(integerLiteral: data01Array.count+1)))
        chart.xLabelWidth = UIScreen.main.bounds.width/4.2

        chart.chartData = [chartData]

        chart.stroke()
//        chart.updateChartData([chartData])
    }
    
    
    func dataVisualizationMonth(){
        
        
        let dateFormatterGet = DateFormatter()
        dateFormatterGet.dateFormat = "yyyy-MM-dd HH:mm:ss"
        
        let dateFormatterPrint = DateFormatter()
        dateFormatterPrint.dateFormat = "MMM dd,yyyy"
        
        
        let date = Date().startOfMonth()
        
        var dateInterval : Double = date.timeIntervalSince1970
//        dateInterval = dateInterval - 7200
        
        let currentDate = Date().timeIntervalSince1970
        let firstDayCurrentMonth = NSDate(timeIntervalSince1970: dateInterval)
        
        
        var dateComponent = DateComponents()
        dateComponent.month = -1
    
        let currentMonthMinus1 = Calendar.current.date(byAdding: dateComponent, to: firstDayCurrentMonth as Date)
        dateComponent.month = -2
       
        let currentMonthMinus2 = Calendar.current.date(byAdding: dateComponent, to: firstDayCurrentMonth as Date)
        dateComponent.month = -3
        
        let currentMonthMinus3 = Calendar.current.date(byAdding: dateComponent, to: firstDayCurrentMonth as Date)
        dateComponent.month = -4
        
        let currentMonthMinus4 = Calendar.current.date(byAdding: dateComponent, to: firstDayCurrentMonth as Date)
        dateComponent.month = -5
        
        let currentMonthMinus5 = Calendar.current.date(byAdding: dateComponent, to: firstDayCurrentMonth as Date)
        dateComponent.month = -6
        
        let currentMonthMinus6 = Calendar.current.date(byAdding: dateComponent, to: firstDayCurrentMonth as Date)
        dateComponent.month = -7
        
        let currentMonthMinus7 = Calendar.current.date(byAdding: dateComponent, to: firstDayCurrentMonth as Date)
        dateComponent.month = -8
        
        let currentMonthMinus8 = Calendar.current.date(byAdding: dateComponent, to: firstDayCurrentMonth as Date)
        dateComponent.month = -9
        
        let currentMonthMinus9 = Calendar.current.date(byAdding: dateComponent, to: firstDayCurrentMonth as Date)
        dateComponent.month = -10
        
        let currentMonthMinus10 = Calendar.current.date(byAdding: dateComponent, to: firstDayCurrentMonth as Date)
        dateComponent.month = -11
        
        let currentMonthMinus11 = Calendar.current.date(byAdding: dateComponent, to: firstDayCurrentMonth as Date)
        
        
        var chartLabels : [String] = [ Date.getFormattedMonths(date: date), Date.getFormattedMonths(date: currentMonthMinus1!) ,Date.getFormattedMonths(date: currentMonthMinus2!),Date.getFormattedMonths(date: currentMonthMinus3!),Date.getFormattedMonths(date: currentMonthMinus4!),Date.getFormattedMonths(date: currentMonthMinus5!),Date.getFormattedMonths(date: currentMonthMinus6!),Date.getFormattedMonths(date: currentMonthMinus7!),Date.getFormattedMonths(date: currentMonthMinus8!),Date.getFormattedMonths(date: currentMonthMinus9!),Date.getFormattedMonths(date: currentMonthMinus10!),Date.getFormattedMonths(date: currentMonthMinus11!)]
        
        print(chartLabels)
        
        var incomeValues : [String:Double] = [chartLabels[0]:0,chartLabels[1]:0,chartLabels[2]:0,chartLabels[3]:0,chartLabels[4]:0,chartLabels[5]:0,chartLabels[6]:0,chartLabels[7]:0,chartLabels[8]:0,chartLabels[9]:0,chartLabels[10]:0,chartLabels[11]:0]
        
        for item in orderHistoryItems{
            
            
            if item.timeStamp < currentDate && item.timeStamp > dateInterval{
                
                let dayIncome = incomeValues[chartLabels[0]]
                
                incomeValues[chartLabels[0]] = dayIncome! + item.totalOrderCost
                print("current month")
                
            }else if(item.timeStamp < dateInterval && item.timeStamp > (currentMonthMinus1?.timeIntervalSince1970)!){
                let dayIncome = incomeValues[chartLabels[1]]
                
                incomeValues[chartLabels[1]] = dayIncome! + item.totalOrderCost
                print("last month")
            
            }else if(item.timeStamp < (currentMonthMinus1?.timeIntervalSince1970)!) && item.timeStamp > (currentMonthMinus2?.timeIntervalSince1970)!{
                
                let dayIncome = incomeValues[chartLabels[2]]
                
                incomeValues[chartLabels[2]] = dayIncome! + item.totalOrderCost
                
                print("-2 month")
            }else if(item.timeStamp < (currentMonthMinus2?.timeIntervalSince1970)!) && item.timeStamp > (currentMonthMinus3?.timeIntervalSince1970)!{
                
                let dayIncome = incomeValues[chartLabels[3]]
                
                incomeValues[chartLabels[3]] = dayIncome! + item.totalOrderCost
                print("-3 month")
            }else if(item.timeStamp < (currentMonthMinus3?.timeIntervalSince1970)!) && item.timeStamp > (currentMonthMinus4?.timeIntervalSince1970)!{
                
                let dayIncome = incomeValues[chartLabels[4]]
                
                incomeValues[chartLabels[4]] = dayIncome! + item.totalOrderCost
                print("-4 month")
            }else if(item.timeStamp < (currentMonthMinus4?.timeIntervalSince1970)!) && item.timeStamp > (currentMonthMinus5?.timeIntervalSince1970)!{
                
                let dayIncome = incomeValues[chartLabels[5]]
                
                incomeValues[chartLabels[5]] = dayIncome! + item.totalOrderCost
                print("-5 month")
            }else if(item.timeStamp < (currentMonthMinus5?.timeIntervalSince1970)!) && item.timeStamp > (currentMonthMinus6?.timeIntervalSince1970)!{
                
                let dayIncome = incomeValues[chartLabels[6]]
                
                incomeValues[chartLabels[6]] = dayIncome! + item.totalOrderCost
                print("-6 month")
            }else if(item.timeStamp < (currentMonthMinus6?.timeIntervalSince1970)!) && item.timeStamp > (currentMonthMinus7?.timeIntervalSince1970)!{
                
                let dayIncome = incomeValues[chartLabels[7]]
                
                incomeValues[chartLabels[7]] = dayIncome! + item.totalOrderCost
                print("-7 month")
            }else if(item.timeStamp < (currentMonthMinus7?.timeIntervalSince1970)!) && item.timeStamp > (currentMonthMinus8?.timeIntervalSince1970)!{
                
                let dayIncome = incomeValues[chartLabels[8]]
                
                incomeValues[chartLabels[8]] = dayIncome! + item.totalOrderCost
                print("-8 month")
            }else if(item.timeStamp < (currentMonthMinus8?.timeIntervalSince1970)!) && item.timeStamp > (currentMonthMinus9?.timeIntervalSince1970)!{
                
                let dayIncome = incomeValues[chartLabels[9]]
                
                incomeValues[chartLabels[9]] = dayIncome! + item.totalOrderCost
                print("-9 month")
            }else if(item.timeStamp < (currentMonthMinus9?.timeIntervalSince1970)!) && item.timeStamp > (currentMonthMinus10?.timeIntervalSince1970)!{
                
                let dayIncome = incomeValues[chartLabels[10]]
                
                incomeValues[chartLabels[10]] = dayIncome! + item.totalOrderCost
                print("-10 month")
            }else if(item.timeStamp < (currentMonthMinus10?.timeIntervalSince1970)!) && item.timeStamp > (currentMonthMinus11?.timeIntervalSince1970)!{
                
                let dayIncome = incomeValues[chartLabels[11]]
                
                incomeValues[chartLabels[11]] = dayIncome! + item.totalOrderCost
                print("-11 month")
            }
            
        }
        
        print(incomeValues)
        
        var data01Array : [NSNumber] = [NSNumber(value: incomeValues[chartLabels[0]]!),NSNumber(value: incomeValues[chartLabels[1]]!),NSNumber(value: incomeValues[chartLabels[2]]!),NSNumber(value: incomeValues[chartLabels[3]]!),NSNumber(value: incomeValues[chartLabels[4]]!),NSNumber(value: incomeValues[chartLabels[5]]!),NSNumber(value: incomeValues[chartLabels[6]]!),NSNumber(value: incomeValues[chartLabels[7]]!),NSNumber(value: incomeValues[chartLabels[8]]!),NSNumber(value: incomeValues[chartLabels[9]]!),NSNumber(value: incomeValues[chartLabels[10]]!),NSNumber(value: incomeValues[chartLabels[11]]!)]
        data01Array = data01Array.reversed()
        chartLabels = chartLabels.reversed()
        
        chartData.itemCount = UInt(chartLabels.count)
        
        chartData.getData = { index in
            let yValue = CGFloat(data01Array[Int(index)].floatValue)
            print("index: \(index)")
            print("yvalye \(yValue)")
            return PNLineChartDataItem(y: yValue)
        }
        chart.setXLabels(chartLabels, withWidth: CGFloat(chart.frame.width/CGFloat(integerLiteral: data01Array.count+1)))
        chart.xLabelWidth = UIScreen.main.bounds.width/13
        
        chart.chartData = [chartData]
        chart.stroke()
    }
    
    func getFormattedDate(date: Date) -> String{
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss +zzzz" // This formate is input formated .
        
        let formateDate = dateFormatter.date(from:"2018-02-02 06:50:16 +0000")!
        dateFormatter.dateFormat = "MMM" // Output Formated
        
        
        print ("Print :\(dateFormatter.string(from: formateDate))")
        
        return dateFormatter.string(from: formateDate)
    }
    
}

extension Double {
    
    func convertToStringDate() -> String{
        
        let date = NSDate(timeIntervalSince1970: self)
        
        let dayTimePeriodFormatter = DateFormatter()
        
        dayTimePeriodFormatter.dateFormat = "MMM/dd/YY"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        return dateString
    }
}

extension Date{
    func startOfMonth() -> Date {
    return Calendar.current.date(from: Calendar.current.dateComponents([.year, .month], from: Calendar.current.startOfDay(for: self)))!
    }
    
    static func getFormattedMonths(date:Date) -> String{
        
        
        let dayTimePeriodFormatter = DateFormatter()
        
        dayTimePeriodFormatter.dateFormat = "MMM"
        
        let dateString = dayTimePeriodFormatter.string(from: date as Date)
        
        return dateString
        
    }
}

