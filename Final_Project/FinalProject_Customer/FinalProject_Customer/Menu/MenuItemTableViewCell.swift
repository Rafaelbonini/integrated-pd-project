//
//  MenuItemTableViewCell.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 12/16/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit

class MenuItemTableViewCell: UITableViewCell {

    @IBOutlet weak var itemName: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
