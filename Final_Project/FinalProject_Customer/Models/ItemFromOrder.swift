//
//  ItemFromOrder.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/24/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import Foundation
class ItemFromOrder {
    
    var dishName:String
    var dishPrice:Double
    var dishQuantity:Int
    var servWFood:Bool
    
    init(dishName:String,dishPrice:Double,dishQuantity:Int,servWFood:Bool) {
        self.dishName = dishName
        self.dishPrice = dishPrice
        self.dishQuantity = dishQuantity
        self.servWFood = servWFood
    }
    
}
