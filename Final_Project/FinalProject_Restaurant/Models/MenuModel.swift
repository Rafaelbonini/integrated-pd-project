//
//  MenuModel.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/29/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import Foundation
import UIKit

class MenuModel{
    
    var dishName:String
    var dishPrice:Double
    
    var desc:String
    var image:UIImage?
    var category:String
    var portion:String
    var index:Int
    var hasImg:Bool
    
    init(dishName:String,dishPrice:Double,desc:String,image:UIImage?,category:String,portion:String,index:Int,hasImg:Bool) {
        self.dishName = dishName
        self.dishPrice = dishPrice
        self.desc = desc
        self.image = image
        self.category = category
        self.portion = portion
        self.index = index
        self.hasImg = hasImg
    }
    

}
