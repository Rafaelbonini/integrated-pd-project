//
//  QRCodeTableViewCell.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/15/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

class QRCodeTableViewCell: UITableViewCell {

    @IBOutlet weak var qrCellImage: UIImageView!
    @IBOutlet weak var qrCellLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
