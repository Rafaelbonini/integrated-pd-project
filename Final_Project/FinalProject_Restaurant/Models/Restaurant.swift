//
//  Restaurant.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/13/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import Foundation
import Firebase


//MARK: TODO UPDATE WITH PROPER INFO
class Restaurant
{
    var userUid:String
    var userName:String
    var userEmail:String
    var profilePic:UIImage?
    var latitude:String
    var longitude:String
    var numOfTables:Int
    
    
    
    init(userUid:String,userName:String,userEmail:String,profilePic:UIImage,latitude:String,longitude:String,numOfTables:Int) {
        self.userUid = userUid
        self.userName = userName
        self.userEmail = userEmail
        self.profilePic = profilePic
        self.latitude = latitude
        self.longitude = longitude
        self.numOfTables = numOfTables
    }
    
    func save(completion: @escaping (Error?) -> Void)
    {
        
        // reference to the database
        let ref = Database.database().reference().child("Restaurants").child(userUid)
        
        
        
        // setValue of the reference
        ref.setValue(toDictionary())
        
        // 3. save the user's profile Image to the storage
        
        
        if let image = profilePic?.jpegData(compressionQuality: 0.1){
            
            let storageRefere = Storage.storage().reference().child("RestaurantPictures/\(userUid).jpg")
            
            storageRefere.putData(image, metadata: nil){ (metadata, error) in
                
                if error != nil{
                    print(error!)
                    
                }else{
                    
                    print("naicela")
                }

                
            }
        }
    }
    
    func toDictionary() -> [String : Any]
    {
        return [
            "uid" : userUid,
            "username" : userName,
            "email" : userEmail,
            "location" : ["latitude":latitude,"longitude":longitude],
            "orderHistory" : ["order number" : ["Item" : "price" ]],
            "NumerOfTables":numOfTables,
            "Menu":""
            
        ]
    }
    
    
}
