//
//  EditProfileTableViewCell.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 2/2/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

class EditProfileTableViewCell: UITableViewCell {

    @IBOutlet weak var cellValue: UILabel!
    @IBOutlet weak var cellEditPurpose: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
