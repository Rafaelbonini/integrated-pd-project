//
//  DetailsOrderHistoryViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/25/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase

private let cellId = "CellId"
class DetailsOrderHistoryViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
    

    var getSelectedOrder : OrderHistory?
    var order : [ItemFromOrder] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var restaurantTable: UILabel!
    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var orderTotalCost: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.dataSource = self
        tableView.delegate = self
        
        
        if let selectedOrder = getSelectedOrder{
            
            let customerUid = selectedOrder.customerUid
            let ref = Database.database().reference().child("Users").child(customerUid)
            
            //use orders uid to get each order information
            ref.observeSingleEvent(of: .value, with: { (snapshot) in
                
                let value = snapshot.value as? [String:Any]
                
                if let customerNam = value?["username"] as? String {
                    
                    self.customerName.text = "Customer: \(customerNam)"
                    
                }
                
                })
            
            
            
            let date = NSDate(timeIntervalSince1970: selectedOrder.timeStamp)
            
            let dayTimePeriodFormatter = DateFormatter()
            
            dayTimePeriodFormatter.dateFormat = "MMM/dd/YYYY hh:mm"
            
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            
            
//            restaurantNameLabel.text = "Restaurant: \(selectedOrder.restaurantName)"
            restaurantTable.text = "Table: \(selectedOrder.tableNum)"
            orderDate.text = "Date: \(dateString)"
            orderTotalCost.text = "$\(selectedOrder.totalOrderCost.clean)"
            
            order = selectedOrder.order
            
            tableView.reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! ItemInOrderHistoryTableViewCell
        
//        let nameAndQuantity = "\(order[indexPath.row].dishName) x \(order[indexPath.row].dishQuantity)"
        let sumPrice = "$\((order[indexPath.row].dishPrice * Double(order[indexPath.row].dishQuantity)).clean)"
        
        
        var nameAndQuantity = "\(order[indexPath.row].dishName)"
        nameAndQuantity = nameAndQuantity.truncate(length: 24, trailing: "...")
        let quantity = " x \(order[indexPath.row].dishQuantity)"
        nameAndQuantity += quantity
        
        cell.itemName.text = nameAndQuantity
        cell.itemPrice.text = sumPrice
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}
