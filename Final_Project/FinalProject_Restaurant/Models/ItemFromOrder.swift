//
//  File.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/22/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import Foundation
import UIKit

class ItemFromOrder{
    
    var dishName:String
    var dishPrice:Double
    var dishQuantity:Int
    var servWFood:Bool
    

    
    init(dishName:String,dishPrice:Double,dishQuantity:Int,servWFood:Bool) {
        self.dishName = dishName
        self.dishPrice = dishPrice
        self.dishQuantity = dishQuantity
        self.servWFood = servWFood
    }
    
}
