//
//  tablesData.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 3/16/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import Foundation

class TablesData{

    
    var tableNum:Int
    var tableStatus:String
    var customersTableData:[OrdersInTableData]
    
    init(tableStatus:String,tableNum:Int,customersTableData:[OrdersInTableData]) {
        
        self.tableStatus = tableStatus
        self.tableNum = tableNum
        self.customersTableData = customersTableData
    }
}
