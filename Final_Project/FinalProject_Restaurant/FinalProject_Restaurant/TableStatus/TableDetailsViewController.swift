//
//  TableDetailsViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/26/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase

private let cellId = "cellId"
private let orderDetailsSegueId = "orderDetailSegue"
class TableDetailsViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    //TODO: - DELETE THIS
//    var getTableOrder : tableOrder?
    
    var getTableInfo : TablesData?
//    var getCustomerInfo : OrdersInTableData?
    
    var selectedCustomer : OrdersInTableData?
    
    var getWaiterRequested : Bool?
    var getManagerRequested : Bool?
    var getBillRequested : Bool?
    
    var customersName : [String] = []
    var customersPrice : [Double] = []
    
    //TODO: - DELETE THIS
    var orderItems : [ItemFromOrder] = []
    
    @IBOutlet weak var serviceButton: UIButton!
    @IBOutlet weak var staffRequestedLabel: UILabel!
    @IBOutlet weak var currentTableView: UIView!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var tableNumber: UILabel!
    @IBOutlet weak var tableStatus: UILabel!
    @IBOutlet weak var orderTotal: UILabel!
    var orderTotalCost = 0.0
    var currentTableNum = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        serviceButton.alpha = 0
        // Do any additional setup after loading the view.
        staffRequestedLabel.isHidden = true
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        currentTableView.backgroundColor = UIColor.green
        currentTableView.layer.cornerRadius = currentTableView.frame.width/2
        currentTableView.clipsToBounds = true

        
        if let waiterRequested = getWaiterRequested {
            
            
            if waiterRequested{
                
                staffRequestedLabel.isHidden = false
                staffRequestedLabel.text = "Waiter Requested!"
                serviceButton.setTitle("Waiter Sent!", for: .normal)
                serviceButton.alpha = 1
                
            }
            
        }
        
        if let managerRequested = getManagerRequested{
            if managerRequested {
                
                staffRequestedLabel.isHidden = false
                staffRequestedLabel.text = "Manager Requested!"
                serviceButton.setTitle("Manager Sent!", for: .normal)
                serviceButton.alpha = 1
            }
        }
        

        
        if getManagerRequested == nil && getWaiterRequested == nil{
            staffRequestedLabel.isHidden = true
            serviceButton.setTitle("Service Ended", for: .normal)
            serviceButton.alpha = 0
        }
        
//        if let billRequested = getBillRequested{
//            if billRequested {
//
//                staffRequestedLabel.isHidden = false
//                staffRequestedLabel.text = "Bill Requested!"
//                serviceButton.setTitle("Order was Paid!", for: .normal)
//
//            }
//        }

        
        if let table = getTableInfo{

            print("table num \(table.tableNum)")
            print("table Status \(table.tableStatus)")
            
            tableNumber.text = String(table.tableNum)
            tableStatus.text = "Table Status: \(table.tableStatus)"
            currentTableNum = table.tableNum
            
            var totalPrice :Double = 0
            
            for customer in table.customersTableData{
            
                print("customer name \(customer.userName)")
                print("customer uid \(customer.userUid)")
                print("customer status \(customer.tableStatus)")
                
                customersName.append(customer.userName)
                
                var customerTotalPrice = 0.0
                
                for orderItem in customer.items{
                    
//                    let itemTotal = orderItem.dishPrice * Double(orderItem.dishQuantity)
                    
                    customerTotalPrice += orderItem.dishPrice * Double(orderItem.dishQuantity)
                    
                    
                }
                totalPrice += customerTotalPrice
                customersPrice.append(customerTotalPrice)
            }
            orderTotal.text = "Total: $\(totalPrice)"
            
        }
        
        tableView.reloadData()
    }
    
    func clearStaffRequest (){
        
        
        guard let currentRestaurantUid = Auth.auth().currentUser?.uid else{
            return
        }
        
        let ref = Database.database().reference().child("Restaurants").child(currentRestaurantUid).child("Tables").child("Table\(currentTableNum)")
        
        ref.observeSingleEvent(of: .value) { (snapshot) in
            
            if let value = snapshot.value as? [String:AnyObject]{

                print(value)
                
                var uids : [String] = []
                
                for item in value.values{
                    
                    print("itemtodeleteitem\(item)")
                    if let itemUid = item["userUid"] as? String{
                        uids.append(itemUid)
                    }
                    
                }
                
                
                for uid in uids{
                    
//                    ref.child(uid).child("WaiterRequest").removeValue()
//                    ref.child(uid).child("managerRequest").removeValue()
                    
//                    ref.updateChildValues(["updated":true])
                    
                    ref.updateChildValues(["WaiterRequest":false,"managerRequest":false])
                    
                }
            }
            
            
            
        }

        staffRequestedLabel.isHidden = true
//        serviceButton.setTitle("Service Ended", for: .normal)
        serviceButton.alpha = 0

    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return customersName.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! tableStatusOrderTableViewCell
        
        
        let name = customersName[indexPath.row]
        let totalPrice = "$\(customersPrice[indexPath.row])"
        
        cell.nameQuantityLabel.text = name
        cell.priceLabel.text = totalPrice
        
        return cell
    }
    
    @IBAction func removeUser(_ sender: Any) {
        
        let alert = UIAlertController(title: "Remove", message: "Are you sure you want to remove the customer from the table?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Delete", style: .destructive, handler: { (action) in
            
            guard let restUid = Auth.auth().currentUser?.uid else{
                return
            }
            let table = "Table\(self.currentTableNum)"
            
            let ref = Database.database().reference().child("Restaurants").child(restUid).child("Tables").child(table)
            
            ref.removeValue()
            
            self.navigationController?.popViewController(animated: true)
            
        }))
        present(alert, animated: true)
        
        
    }
    
    @IBAction func ServiceAndStaffDismiss(_ sender: Any) {
        
        if serviceButton.titleLabel?.text == "Order was Paid!"{
            //save order (orders history and reference to user and restaurant) & remove user
            
            userPaidOrder()
            
            
        }else if serviceButton.titleLabel?.text == "Waiter Sent!" || serviceButton.titleLabel?.text == "Manager Sent!"{
            //confirm and end service
            clearStaffRequest()
            
            
        }
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if getTableInfo?.customersTableData[indexPath.row] != nil{
        
            selectedCustomer = getTableInfo?.customersTableData[indexPath.row]
            
            self.performSegue(withIdentifier: orderDetailsSegueId, sender: nil)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == orderDetailsSegueId{
            
            let orderDetls = segue.destination as! CustomerOrderViewController
            
            orderDetls.getCustomerInfo = selectedCustomer
            
        }
    }
    
    
    func userPaidOrder(){
        
    }
}
