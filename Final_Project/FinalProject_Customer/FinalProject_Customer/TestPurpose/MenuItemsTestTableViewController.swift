//
//  MenuItemsTestTableViewController.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/13/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

private let cellId = "menuCell"
private let detailSegueId = "toDetailItem"
private let cartSegueId = "CartIdentifier"
class MenuItemsTestTableViewController: UITableViewController,DataEnteredDelegate {
    
    
    var getRestaurantMenu : [MenuModel]?
    var tableSections : [String] = []
    var cellsInSectionsDic : [String:Int] = [:]
    
    var itemOrdered : RestaurantItemsTestModel?
    var itemsOrdered : [RestaurantItemsTestModel] = []
    
    func orderList(items: [RestaurantItemsTestModel]) {
        itemsOrdered = items
        
        
        for item in itemsOrdered{
            print("\(item.name)  quantity: \(item.quantity)")
        }
    }
    
    var category1 :[RestaurantItemsTestModel] = []
    var category2 : [RestaurantItemsTestModel] = []
    
    var selectedRow = -1
    
    func setTable(){
        
        if let restaurantMenu = getRestaurantMenu{
            
            
            
            for menuItem in restaurantMenu{
                //get num of sections / title of sections
                if !tableSections.contains(menuItem.dishCategory){ tableSections.append(menuItem.dishCategory) }
                
                //get number of cells for each sections
                if cellsInSectionsDic.keys.contains(menuItem.dishCategory) {
                    // contains key
                    let newValue = cellsInSectionsDic[menuItem.dishCategory]! + 1
                    
                    cellsInSectionsDic[menuItem.dishCategory] = newValue
                    
                } else {
                    // does not contain key
                    cellsInSectionsDic[menuItem.dishCategory] = 1
                }
            }
            
            
        }
        
        tableSections = tableSections.sorted()
        print("table sections: \(tableSections)")
        print("table cellsInSectionsDic: \(cellsInSectionsDic.reversed())")
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        for bla in getRestaurantMenu!{
            print(bla.dishName)
            print(bla.dishPrice)
        }
        
        getRestaurantMenu = getRestaurantMenu!.sorted(by: { $0.dishCategory < $1.dishCategory })
        
        for bla in getRestaurantMenu!{
            print(bla.dishName)
            print(bla.dishPrice)
        }
        
        setTable()
        
        
        
        
        
        
        //        tableView.reloadData()
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return tableSections.count
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        
        
        if let cellsInSection = cellsInSectionsDic[tableSections[section]]{
            return cellsInSection
            
        }
        
        return 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MenuItemTestTableViewCell
        
        
        
        if let item = getRestaurantMenu{
            
            print(tableSections)
            for item in getRestaurantMenu!{
                print(item.dishName)
            }
            print(indexPath.section)
            print(item[indexPath.row].dishCategory)
            print(item[indexPath.row].dishName)
            
            
            var currentRow = indexPath.row
            
            var sectionToIndex = indexPath.section
            print("section: \(indexPath.section)")
            
            if indexPath.section != 0{
                
                sectionToIndex = sectionToIndex-1
                
                for index in 0...sectionToIndex{
                    
                    currentRow += cellsInSectionsDic[tableSections[index]]!
                    
                }
                
            }
            
            print("current row: \(currentRow)")
            
            
            print("section to index: \(sectionToIndex)")
            
            if item[currentRow].dishCategory == tableSections[indexPath.section]{
                
                
                
                //              cell.itemImage.image = category1[indexPath.row].image
                cell.itemTittle.text = item[currentRow].dishName
                cell.itemDesc.text = item[currentRow].dishDesc
                cell.itemPrice.text = "$ \(item[currentRow].dishPrice)"
                
                
            }
            
            
            
        }
        
        
        //                    cell.itemImage.image = category1[indexPath.row].image
        
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        selectedRow = indexPath.row
        var sectionToIndex = indexPath.section
        selectedRow = indexPath.row
        if indexPath.section != 0{
            
            sectionToIndex = sectionToIndex-1
            
            for index in 0...sectionToIndex{
                
                selectedRow += cellsInSectionsDic[tableSections[index]]!
                
            }
            
        }
        
        
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        performSegue(withIdentifier: detailSegueId, sender: nil)
        
        
        
        
        
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    override func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        //        if section == 0{
        //            return "Appetizers"
        //        }else if section == 1{
        //            return "Main Dishes"
        //        }
        
        return tableSections[section]
        
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == detailSegueId{
            
            
            let detailView = segue.destination as! DetailsTestViewController
            detailView.delegate = self
            detailView.getitemsOrdered = itemsOrdered
            
            if let menuItems = getRestaurantMenu{
                
                detailView.getSelectedItem = menuItems[selectedRow]
                
            }
            
            
        }else if segue.identifier == cartSegueId{
            let cartVC = segue.destination as! CartViewController
            
            cartVC.getOrders = itemsOrdered
            
        }
    }
    
    
    
}
