//
//  ProfileEditTableViewCell.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 12/14/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit

class ProfileEditTableViewCell: UITableViewCell {

    @IBOutlet weak var cellPurposeLabel: UILabel!
    @IBOutlet weak var cellValueLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
