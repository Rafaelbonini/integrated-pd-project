//
//  CustomerOrderTableViewCell.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 3/27/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

class CustomerOrderTableViewCell: UITableViewCell {
    @IBOutlet weak var dishNameQuantity: UILabel!
    @IBOutlet weak var dishPrice: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
