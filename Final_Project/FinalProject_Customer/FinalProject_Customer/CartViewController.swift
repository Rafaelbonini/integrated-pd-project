//
//  CartViewController.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/14/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase



private let cartCellID = "CartCell"
private let orderPlacedId = "orderPlacedID"
class CartViewController: UIViewController,UITableViewDataSource,UITableViewDelegate{
    
    
    @IBOutlet weak var orderTotal: UILabel!
    var orderTotalSum : Double = 0
    var getOrders : [RestaurantItemsTestModel]?
    var getReorder : [ItemFromOrder]?
    
    var currentOrder : [RestaurantItemsTestModel] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cartCellID, for: indexPath) as! CartTableViewCell
        
        var nameAndQuantity = "\(currentOrder[indexPath.row].name)"
        nameAndQuantity = nameAndQuantity.truncate(length: 24, trailing: "...")
        let quantity = " x \(currentOrder[indexPath.row].quantity)"
        nameAndQuantity += quantity
        
        let sumPrice = "$\(currentOrder[indexPath.row].price * Double(currentOrder[indexPath.row].quantity))"
        
        cell.itemNameQuantity.text = nameAndQuantity
        cell.itemPriceSum.text = sumPrice
        
        return cell
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    
    @IBOutlet weak var tableViewCart: UITableView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let reorder = getReorder{
            
            for item in reorder{
                
                currentOrder.append(RestaurantItemsTestModel(image: UIImage(), name: item.dishName, desc: "", portionSize: "", servWithFood: item.servWFood, price: item.dishPrice, quantity: item.dishQuantity))
                
            }
            
        }
        
        if let orders = getOrders{
            
            currentOrder = orders
        }
        
        
        for orderItem in currentOrder{
            
            orderTotalSum += orderItem.price * Double(orderItem.quantity)
            
        }
        print(orderTotalSum)
        orderTotal.text = "$\(orderTotalSum)"
        
        
        
        self.tableViewCart.delegate = self
        self.tableViewCart.dataSource = self
        
        // Do any additional setup after loading the view.
        
        
        
        
        self.tableViewCart.reloadData()
    }
    
    @IBAction func placeOrder(_ sender: Any) {
        
        
        if currentOrder.count != 0{
            //items / timestamp / restaurant uid / table number
            
            //save to user current table
            var userUid = ""
            if let Uid = Auth.auth().currentUser?.uid{
                userUid = Uid
            }
            
            var resturantUid = ""
            var currentTable = ""
            if let restUid = UserDefaults.standard.string(forKey: restaurantUidKey), let tableNumber = UserDefaults.standard.string(forKey: restaurantTableNumber){
                resturantUid = restUid
                currentTable = tableNumber
            }
            
            let currentDateTime = Int(NSDate().timeIntervalSince1970)
            
            let ref = Database.database().reference().child("Users").child(userUid).child("curentOrderInfo")
            let restaurantRef = Database.database().reference().child("Restaurants").child(resturantUid).child("Tables").child("Table\(currentTable)")
            
            
            //        ref.updateChildValues(["placedTime":["Date":currentDate,"Time":currentTime]])
            ref.updateChildValues(["RestaurantUid":resturantUid,"tableNumber":currentTable])
            //        ref.updateChildValues(["tableNumber":currentTable])
            print("restaurantuid \(resturantUid), table: \(currentTable)")
            
            
            //save to restaurant table
            
                
                //save current order info to both the restaurant table and the user current order
                for (i,item) in self.currentOrder.enumerated(){
                    
                    ref.child("order").child("dish\(i+1)").updateChildValues([
                        "dishName": item.name,
                        "dishPrice": item.price,
                        "dishQuantity": item.quantity,
                        "servWFood": item.servWithFood
                        ])
                    
                    restaurantRef.child(userUid).child("order").child("dish\(i+1)").setValue([
                        "dishName": item.name,
                        "dishPrice": item.price,
                        "dishQuantity": item.quantity,
                        "servWFood": item.servWithFood
                        ])
                    
            
                //update table information
                    var userNameDisplayed = ""
                    if let userName = Auth.auth().currentUser?.displayName{
                        userNameDisplayed = userName
                    }
                    restaurantRef.child(userUid).updateChildValues(["userUid":userUid,"tableStatus":"Order Placed","tableNumber":currentTable,"timeStamp":currentDateTime,"userName":userNameDisplayed])
                    

            }
            
            UserDefaults.standard.set("Placed", forKey: orderStatusKey)
            
            performSegue(withIdentifier: orderPlacedId, sender: nil)
        }else{
            
            let alert = UIAlertController(title: "Order", message: "Please, select your order items before placing an order!", preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            
            present(alert, animated: true)
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        
        if segue.identifier == orderPlacedId{
            
            let orderPlacedVC = segue.destination as! OrderPlacedViewController
            
            orderPlacedVC.getPlacedOrder = currentOrder
            
            
        }
    }
}
