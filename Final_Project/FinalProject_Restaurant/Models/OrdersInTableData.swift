//
//  OrdersInTableData.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 3/16/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import Foundation

class OrdersInTableData{
    
    var userUid:String
    var userName:String
    var timeStamp:Int
    var tableStatus:String
    var items:[ItemFromOrder]
    var tableNum:Int
    
    init(userUid:String,userName:String,tableStatus:String,tableNum:Int,items:[ItemFromOrder],timeStamp : Int) {
        
        self.userUid = userUid
        self.userName = userName
        self.tableStatus = tableStatus
        self.items = items
        self.timeStamp = timeStamp
        self.tableNum = tableNum
    }
}
