//
//  OrderPlacedViewController.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/21/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
import Stripe
import SwiftMessages

enum STPBackendChargeResult {
    case Success, Failure
}

typealias STPTokenSubmissionHandler = (STPBackendChargeResult?, NSError?) -> Void

private let placedCellId = "PlacedCell"
private let backToQrSegue = "backToQr"
class OrderPlacedViewController: UIViewController, UITableViewDelegate,UITableViewDataSource,PKPaymentAuthorizationViewControllerDelegate {
    
    
    @IBOutlet weak var ordermoreButton: UIButton!
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        dismiss(animated: true, completion: nil)
        print("dismiss")
        
        let orderStatus = UserDefaults.standard.string(forKey: orderStatusKey)
        if orderStatus == ""{
            
        setMessage()
        self.performSegue(withIdentifier: backToQrSegue, sender: nil)
        }
    }
    
    let publishableKey = "pk_test_vbdWXPNgeirNrA0suqq1X4hx"
    let backendChargeURLString = "https://striperafaelbonini.herokuapp.com"
    let request = Stripe.paymentRequest(withMerchantIdentifier: "merchant.com.rafaelbonini.stripe", country: "US", currency: "USD")
    
    @IBOutlet weak var totalSumLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    var orderTotalSum = 0.0
    
    var getPlacedOrder : [RestaurantItemsTestModel]?
    
    var currentOrder : [RestaurantItemsTestModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let reorderStatus = UserDefaults.standard.string(forKey: reorderKey)
        if reorderStatus != ""{
            ordermoreButton.isHidden = true
        }else{
            ordermoreButton.isHidden = false
        }
        
        tableView.delegate = self
        tableView.dataSource = self
        

         
        
        if let order = getPlacedOrder{
            
            currentOrder = order
        }
        
        
        for orderItem in currentOrder{
            
            orderTotalSum += orderItem.price * Double(orderItem.quantity)
            
        }
        
        totalSumLabel.text = "$\(orderTotalSum)"
        
        tableView.reloadData()
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return currentOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: placedCellId, for: indexPath) as! OrderPlacedTableViewCell
        
        cell.restaurantItem = currentOrder[indexPath.row]
        
        return cell
    }
    
    
    
    @IBAction func payOrder(_ sender: Any) {
        
        
        let actionSheet = UIAlertController(title: "Choose the Payment Method", message: "Avaliable Payment Methods", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Apple Pay", style: .default, handler: { (action:UIAlertAction) in

            
            self.request.supportedNetworks = [PKPaymentNetwork.masterCard,PKPaymentNetwork.visa,PKPaymentNetwork.amex]
            //        request.merchantCapabilities = PKMerchantCapability.capabilityCredit.rawValue | PKMerchantCapability.capabilityDebit
            self.request.paymentSummaryItems = [PKPaymentSummaryItem(label: "Order total", amount: NSDecimalNumber(value: self.orderTotalSum))]
            
            let controlerpayment = PKPaymentAuthorizationViewController(paymentRequest: self.request)
            
            controlerpayment?.delegate = self
            
            self.present(controlerpayment!, animated: true, completion: nil)
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cash", style: .default, handler: { (action:UIAlertAction) in
            
            
            if let restaurantUid = UserDefaults.standard.string(forKey: restaurantUidKey),let restaurantTable = UserDefaults.standard.string(forKey: restaurantTableNumber){

                let table = "Table\(restaurantTable)"
                guard let userUid = Auth.auth().currentUser?.uid else{return}
                let ref = Database.database().reference().child("Restaurants").child(restaurantUid).child("Tables").child(table)
                
                ref.updateChildValues(["requestToPayWithCash":true])
                
                //save status and wait for restaurant to confirm payment
                
                
            }

            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
        
       
    }
    

    @IBAction func OrderMore(_ sender: Any) {
        
        self.dismiss(animated: true, completion: nil)
        

    }
    

    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        
        let apiClient = STPAPIClient(publishableKey: publishableKey)
        apiClient.createToken(with: payment) { (token, error) in
            if error == nil{
                if let token = token {
                    self.createBackendChargeWithToken(token: token, completion: { (result, error) -> Void in
                        if result == STPBackendChargeResult.Success{
                            completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.success, errors: nil))
//                            UserDefaults.standard.set("", forKey: orderStatusKey)
                            self.handlePaymentCompleted()
                            
                            print("Autorized")
                        }
                        else {
                            completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.failure, errors: nil))
                        }
                    })
                }
            }else {
                completion(PKPaymentAuthorizationResult.init(status: PKPaymentAuthorizationStatus.failure, errors: nil))
            }
            
        }
        
        

        
    }
    
    func createBackendChargeWithToken(token: STPToken, completion: @escaping STPTokenSubmissionHandler) {
        if backendChargeURLString != "" {
            if let url = URL(string: backendChargeURLString  + "/charge") {
                
                let newTotalInPenies = orderTotalSum*100
                let total = NSDecimalNumber(value: newTotalInPenies)
                let session = URLSession(configuration: URLSessionConfiguration.default)
                let request = NSMutableURLRequest(url: url)
                request.httpMethod = "POST"
                let postBody = "stripeToken=\(token.tokenId)&amount=\(total)"
                let postData = postBody.data(using: String.Encoding.utf8, allowLossyConversion: false)
                session.uploadTask(with: request as URLRequest, from: postData, completionHandler: { data, response, error in
                    let successfulResponse = (response as? HTTPURLResponse)?.statusCode == 200
                    if successfulResponse && error == nil {
                        completion(.Success, nil)
                    } else {
                        if error != nil {
                            completion(.Failure, (error! as NSError))
                        } else {
                            completion(.Failure, NSError(domain: StripeDomain, code: 50, userInfo: [NSLocalizedDescriptionKey: "There was an error communicating with your payment backend."]))
                        }
                        
                    }
                }).resume()
                
                return
            }
        }
        completion(STPBackendChargeResult.Failure, NSError(domain: StripeDomain, code: 50, userInfo: [NSLocalizedDescriptionKey: "You created a token! Its value is \(token.tokenId). Now configure your backend to accept this token and complete a charge."]))
    }
    
    
    //Order was paid , remove user fromo table and save order in history
    func handlePaymentCompleted(){
        //when payment confirmed, store order to be accessed in history orders
        
        let ref = Database.database().reference()
        let restaurantUid = UserDefaults.standard.string(forKey: restaurantUidKey)!
        
        let restaurantTable = UserDefaults.standard.string(forKey: restaurantTableNumber)!
        
        
        let timeStamp = Int(Date().timeIntervalSince1970)
        
        let userUid = Auth.auth().currentUser!.uid
        
        let childRef = ref.child("Orders").childByAutoId()
        
        
        ref.child("Restaurants").child(restaurantUid).observeSingleEvent(of: .value) { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            
            let restaurantName = value?["username"] as? String
            
            if let name = restaurantName{
                
                
                for (i,order) in self.currentOrder.enumerated().reversed(){
                    
                    
                    childRef.child("order").child("dish\(i+1)").updateChildValues([
                        "dishName": order.name,
                        "dishPrice": order.price,
                        "dishQuantity": order.quantity,
                        "servWFood": order.servWithFood
                        ])
                }
                
                childRef.updateChildValues(["restaurantUid":restaurantUid,"tableNum":restaurantTable,"timeStamp":timeStamp,"restaurantName":name,"customerUid":userUid,"orderTotalPrice":self.orderTotalSum])
                
                //reference the saved order to the user order history
                ref.child("Users").child(userUid).child("orderHistory").updateChildValues([childRef.key:1])
                ref.child("Users").child(userUid).child("curentOrderInfo").removeValue()
                ref.child("Restaurants").child(restaurantUid).child("orderHistory").updateChildValues([childRef.key:1])
                
                //delete from table database the current user that paid for the order
                let deleteRef = Database.database().reference().child("Restaurants").child(restaurantUid).child("Tables").child("Table\(restaurantTable)").child(userUid)
                deleteRef.removeValue()
                
                //                self.view.window?.rootViewController?.dismiss(animated: true, completion: nil)
                
                UserDefaults.standard.set("", forKey: orderStatusKey)
                UserDefaults.standard.set("", forKey: reorderKey)
            
//                self.performSegue(withIdentifier: backToQrSegue, sender: nil)
                
            }
            
            
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == backToQrSegue{
            print("goback")

        }
    }
    
    func setMessage(){
        
        let view = MessageView.viewFromNib(layout: .centeredView)
        
        
        view.configureTheme(.success)
        
        
        view.configureDropShadow()
        
        view.configureContent(title: "Payment", body: "The transaction has been completed!")
        
        view.button?.isHidden = true
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
        
    }
}
