//
//  ImportCsv.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/13/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import Foundation
import UIKit
import CSV
import Firebase
import FirebaseAuth

class ImportCsv
{
    
    class func csvInport(path: String){
        
        var userUid = ""
        if let Uid = Auth.auth().currentUser?.uid{
            userUid = Uid
        }
        let ref = Database.database().reference().child("Restaurants").child(userUid).child("Menu")
        var counter = 0
        
        var itemDictionaty : [String:String] = [:]
        
        let stream = InputStream(fileAtPath: path)!
        let csv = try! CSVReader(stream: stream)
        while let row = csv.next() {
            
            if counter == 0{
                counter+=1
            }else{
                
                
                
//                row 0 food group(Category) / row 1 dish name / row 2 dish description / row 3 dish portion / row 4 dish price
                
                
                let categoryref = ref.child(String(counter))
                
                
                itemDictionaty["dishCategory"] = row[0]
                itemDictionaty["dishName"] = row[1]
                itemDictionaty["dishDesc"] = row[2]
                itemDictionaty["dishPortion"] = row[3]
                itemDictionaty["dishPrice"] = row[4]

                
                categoryref.updateChildValues(itemDictionaty)

                itemDictionaty.removeAll()
                
                
                print("\(row)")

                counter+=1
                
            }
            
            let categoryref = ref.child("numOfDishes")
            categoryref.setValue(counter-1)
               
        }

    }
    
}


