//
//  ResgisterViewController.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 12/7/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
import JGProgressHUD
import SwiftMessages

private let verifyPhoneSegue = "verifyPhoneSegue"
class RegisterViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate ,UITextFieldDelegate{

    
    
    @IBOutlet weak var userName: UITextField!
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userPassword: UITextField!
//    @IBOutlet weak var invalidInputLabel: UILabel!
    @IBOutlet weak var userProfileImage: UIImageView!
    let hud = JGProgressHUD(style: .dark)
    
    var imagePicker = UIImagePickerController()
    
    @IBOutlet weak var backgroundView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        
        self.userName.delegate = self
        self.userEmail.delegate = self
        self.userPassword.delegate = self

        addCameraIcToImgView()
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(editProfileImage))
        userProfileImage.isUserInteractionEnabled = true
        userProfileImage.addGestureRecognizer(singleTap)
        
        userProfileImage.layer.cornerRadius = userProfileImage.bounds.width/2
        userProfileImage.layer.masksToBounds = true
        
        
        
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
//        invalidInputLabel.alpha = 0
        
        SetTextViews()
        
    }

    //MARK: - RESGISTER
    @IBAction func createAccount(_ sender: Any) {
     
        userPassword.layer.borderWidth = 0
        userName.layer.borderWidth = 0
        userEmail.layer.borderWidth = 0
        
        if let email = userEmail.text, let password = userPassword.text, let name = userName.text{
            
            if name.count > 2 {
                
                if email.isValidEmail(){
                    
                    if password.count > 5{


                        self.performSegue(withIdentifier: verifyPhoneSegue, sender: nil)
                        
                    }else{
                        //password invalid
                        
//                        invalidInputLabel.text = "A valid password needs 6 or more characters."
//                        invalidInputLabel.alpha = 1
                        self.setMessage(body: "A valid password needs 6 or more characters.")
                        userPassword.layer.borderWidth = 2
                        userPassword.layer.borderColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 0.5).cgColor
                        
                    }
                    
                }else{
                    //email invalid
                    

                    self.setMessage(body: "Please, enter a valid email.")
                    userEmail.layer.borderWidth = 2
                    userEmail.layer.borderColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 0.5).cgColor
                }
                
            }else{
                //warn name invalid
                
                self.setMessage(body: "A valid name need 3 or more characters")
                userName.layer.borderWidth = 2
                userName.layer.borderColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 0.5).cgColor
                
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        userProfileImage.image = image
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == verifyPhoneSegue{
            
            let verifyVC = segue.destination as! VerificationViewController
            
            verifyVC.getUserName = userName.text!
            verifyVC.getUserEmail = userEmail.text!
            verifyVC.getUserPassword = userPassword.text!
            verifyVC.getProfileImage = userProfileImage.image
            
        }
    }
    
    func SetTextViews(){
        //set text field background color
        userName.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        userEmail.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        userPassword.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        
        
        //set textfield placeholder text color
        userName.attributedPlaceholder = NSAttributedString(string: "Name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        userEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        userPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        
        //set textfield text padding
        userName.setLeftPaddingPoints(20)
        userEmail.setLeftPaddingPoints(20)
        userPassword.setLeftPaddingPoints(20)
    }
    
    func setGradientBackground() -> CAGradientLayer {
        let colorTop =  UIColor(red: 130/255.0, green: 194/255.0, blue: 238/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 185/255.0, green: 244/255.0, blue: 182/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        
        return gradientLayer
    }

    
    func setMessage(body:String){
        
        let view = MessageView.viewFromNib(layout: .centeredView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.error)
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        view.configureContent(title: "Sign Up", body: body)
        
        view.button?.isHidden = true
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
        
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        if textField == userName{
            textField.resignFirstResponder()
            userEmail.becomeFirstResponder()
        }else if textField == userEmail{
            userEmail.resignFirstResponder()
            userPassword.becomeFirstResponder()
        }else if textField == userPassword{
            textField.resignFirstResponder()
            
        }
        
        
            return true
    }
    @objc func editProfileImage(){
        
        
        self.imagePicker.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                
                self.imagePicker.sourceType = .photoLibrary;
                self.imagePicker.allowsEditing = true
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
    }
    
    func addCameraIcToImgView(){
        let image = UIImage(named: "camera_ic_white")
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: (userProfileImage.bounds.width/2)-25, y: (userProfileImage.bounds.height/2)-25, width: 50, height: 40)
        imageView.alpha = 0.6
        userProfileImage.addSubview(imageView)
        
    }
}


