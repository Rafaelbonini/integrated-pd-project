//
//  MainTableStatusViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/8/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import FirebaseAuth
import Firebase
import UserNotifications

private let qrCodeSegueId = "QRCodeSegue"
private let collectionCellId = "tableCellId"
private let OrderHistorySegue = "OrderHistorySegue"
private let TableDetailsSegue = "DetailTableSegue"
private let menuScreenSegue = "goToMenuId"
private let orderStatusSegue = "orderStatusSegue"
private let editProfileSegue = "gotoEditProfile"
class MainTableStatusViewController: UIViewController,UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout {
    
    
    var waiterRequest :[String:Bool] = [:]
    var managerRequest : [String:Bool] = [:]
    var billRequest : [String:Bool] = [:]
    var numOfTables = 0
//    var updatedOrders : [tableOrder] = []
    var updatedNOrders : [TablesData] = []
    var selectedTableOrderIndex = -1
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var slideMenuView: UIView!
    var isMenuOpen = false;
    var navigationBarHeight: CGFloat = 0
    var keepRequestManager = false
    var existsManager = false
    
    let blackView : UIView = {
        let bView = UIView(frame: .zero)
        bView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        return bView
    }()
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return numOfTables
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: collectionCellId, for: indexPath) as! TableCollectionViewCell
        
        
        //        cell.colorView.backgroundColor = UIColor.gray
        cell.colorView.layer.cornerRadius = 50
        cell.colorView.clipsToBounds = true
        
        cell.tableNumLabel.text = String(indexPath.row+1)
        
        print(indexPath.row)
//        print(updatedOrders[indexPath.row].tableStatus)
//        print(updatedOrders[indexPath.row].tableNum)
        //        if updatedOrders[indexPath.row].tableNum == indexPath.row+1{
        
        if updatedNOrders[indexPath.row].tableStatus == "Order Placed"{
            
            cell.colorView.backgroundColor = UIColor.green
            cell.tableNumLabel.textColor = UIColor.black
            
        }else if updatedNOrders[indexPath.row].tableStatus == "Seated"{
            
            cell.colorView.backgroundColor = UIColor.blue
            cell.tableNumLabel.textColor = UIColor.white
            
        }else{
            cell.colorView.backgroundColor = UIColor.gray
            cell.tableNumLabel.textColor = UIColor.black
        }
        
        if let tableResquestedWaiter = waiterRequest[String(updatedNOrders[indexPath.row].tableNum)]{
            if tableResquestedWaiter{
                cell.colorView.backgroundColor = UIColor.red
                cell.tableNumLabel.textColor = UIColor.white
            }
            
        }
        if let tableResquestedManager = managerRequest[String(updatedNOrders[indexPath.row].tableNum)]{
            
            
//            print("bbindexpathrow: \(indexPath.row)")
//            print("bbtablenum: \(String(updatedNOrders[indexPath.row].tableNum))")
//            print("bbresult: \(managerRequest[String(updatedNOrders[indexPath.row].tableNum)])")
//            print("bbresult: \(managerRequest)")
            
            if tableResquestedManager{
                cell.colorView.backgroundColor = UIColor.red
                cell.tableNumLabel.textColor = UIColor.white
            }
        }
        if let requestBill = billRequest[String(updatedNOrders[indexPath.row].tableNum)]{
            if requestBill{
                cell.colorView.backgroundColor = UIColor.red
                cell.tableNumLabel.textColor = UIColor.white
            }
            
        }
        return cell
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        navigationController?.navigationBar.barTintColor = UIColor(displayP3Red: 182/255, green: 77/255, blue: 67/255, alpha: 1)
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        print("viewDidLoad()")
        view.bringSubviewToFront(slideMenuView)
        
        
        //        try! Auth.auth().signOut()
        
        collectionView.delegate = self
        collectionView.dataSource = self
        
        
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissMenu)))
        
        navigationBarHeight = UIApplication.shared.statusBarFrame.size.height +
            (self.navigationController?.navigationBar.frame.height ?? 0.0)
        //                try! Auth.auth().signOut()
        
        // Do any additional setup after loading the view.
        
        
        var restaurantUid = ""
        if let Uid = Auth.auth().currentUser?.uid{
            restaurantUid = Uid
        }

        
        
        let sitRef = Database.database().reference().child("Restaurants").child(restaurantUid).child("Tables")
        
        //when a user sits
        sitRef.observe(.childAdded) { (snapshot) in
            
            print("bla")
            
            
            if let dictionary = snapshot.value as? [String:AnyObject]{
                
                print("dictionary: \(dictionary)")
                
                var tableNm = ""
                for customersInTable in dictionary.values{
                    
                    if let numTable = customersInTable["tableNumber"] as? String{
                        tableNm = numTable
                    }
                }
                
                //check if request node for waiter exists
                if let tableWaiterRequest = dictionary["WaiterRequest"] as? Bool{
                    
                    //if exists check if is true
                    if tableWaiterRequest{
                        
                        //a waiter has been requested, save table number that requested
                        self.waiterRequest[tableNm] = true
                        
                        //inform that the table has requested the waiter with an alert
                        let alert = UIAlertController(
                            title: "Staff Request",
                            message: String (format:"Table %@ is requesting a Waiter!", tableNm),
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        //the node exists but has already been dismissed (restaurant market the request as solved)
                        self.waiterRequest[tableNm] = false
                    }
                    
                }
                if let tableManagerRequest = dictionary["managerRequest"] as? Bool{
                    //managerRequest
                    
                    //if exists check if is true
                    if tableManagerRequest{
                        //a manager has been requested, save table number that requested
                        self.managerRequest[tableNm] = true
                        
                        //inform that the table has requested the Manager with an alert
                        let alert = UIAlertController(
                            title: "Staff Request",
                            message: String (format:"Table %@ is requesting a Manager!", tableNm),
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        //the node exists but has already been dismissed (restaurant market the request as solved)
                        self.managerRequest[tableNm] = false
                    }
                    
                }
                if let requestCheck =  dictionary["requestToPayWithCash"] as? Bool{
                    
                    if requestCheck{
                        //save that the table has requested the bill
                        self.billRequest[tableNm] = true
                        
                        
                        //inform that the table has requested the Manager with an alert
                        let alert = UIAlertController(
                            title: "Bill Request",
                            message: String (format:"Table %@ is requesting the Bill to pay in Cash!", tableNm),
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }else{
                        //the node exists but has already been dismissed (restaurant market the request as solved)
                        self.billRequest[tableNm] = false
                    }
                }
                //MARK: - new start
                
                print("dic : \(dictionary)")
                print("dic values: \(dictionary.values)")
                
                
                for item in dictionary.values{
                    
                    print("Item: \(item)")
                    
                    var tableNm = ""
                    if let numTable = item["tableNumber"] as? String{
                        
                        tableNm = numTable
                    }
                    
//                    //test 2.0
//                    if let tableManagerRequest = item["managerRequest"] as? Bool, let tableNum = item["tableNumber"] as? String{
//                        //managerRequest
//
//                        //if exists check if is true
//                        if tableManagerRequest{
//                            //a manager has been requested, save table number that requested
//                            self.managerRequest[tableNum] = true
//
//                            //inform that the table has requested the Manager with an alert
//                            let alert = UIAlertController(
//                                title: "Staff Request",
//                                message: String (format:"Table %@ is requesting a Manager!", tableNum),
//                                preferredStyle: .alert
//                            )
//                            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
//                            self.present(alert, animated: true, completion: nil)
//
//                            self.keepRequestManager = true
//
//                        }
//
//                    }
//                    //test 2.0 end
//
//
                    
                    var itemsFromOrderz : [ItemFromOrder] = []
                    if let userOrder = item["order"] as? [String:Any]{
                        
                        
                        for items in userOrder{
                            
                            if let item = userOrder[items.key] as? [String:AnyObject]{
                                
                                if let dishName = item["dishName"] as? String,let dishPrice = item["dishPrice"] as? Double,let dishQuantity = item["dishQuantity"] as? Int,let servWFood = item["servWFood"] as? Bool{
                                    
                                    itemsFromOrderz.append(ItemFromOrder(dishName: dishName, dishPrice: dishPrice, dishQuantity: dishQuantity, servWFood: servWFood))
                                    
                                    print("dishName \(dishName), dishPrice: \(dishPrice), dishQuantity: \(dishQuantity), servWFood: \(servWFood)")
                                    
                                    
                                }
                            }
                        }
                    }
                    
                    if let tableNumber = item["tableNumber"] as? String, let tableStatus = item["tableStatus"] as? String, let userUid = item["userUid"] as? String, let userName = item["userName"] as? String {
                        
                        print("blablavla \(tableStatus)  \(tableNumber)  \(userUid)   ")
                        
                        var tableNum = 0
                        
                        if let tNum = Int(tableNumber){
                            tableNum = tNum
                        }
                        
                        var ordersForCustomerInTable : [OrdersInTableData] = []
                        ordersForCustomerInTable.append(OrdersInTableData(userUid: userUid, userName: userName, tableStatus: tableStatus, tableNum: tableNum, items: itemsFromOrderz, timeStamp: 0))
                        
                        
                        for (index,_) in self.updatedNOrders.enumerated().reversed(){
                            
                            if self.updatedNOrders[index].tableNum == tableNum  {
                                
                                
                                let newCustomerOrder = TablesData(tableStatus: tableStatus, tableNum: tableNum, customersTableData: ordersForCustomerInTable)
                                
                                
                                if self.updatedNOrders[index].customersTableData.count == 0{
                                    
                                    //there are no customers at the table to be checked (add current user to table with order)
                                    self.updatedNOrders.remove(at: index)
                                    self.updatedNOrders.append(newCustomerOrder)
                                    self.updatedNOrders = self.updatedNOrders.sorted(by: { $0.tableNum < $1.tableNum })
                                    
                                    break;
                                }else{
                                    //there are already customers at the table
                                    //check if the current user is already at the table
                                    
                                    var saved = false
                                    var indexY = -1
                                    for (indexy,_) in self.updatedNOrders[index].customersTableData.enumerated().reversed(){
                                        
                                        if self.updatedNOrders[index].customersTableData[indexy].userUid == userUid{
                                            
                                            saved = true
                                            indexY = indexy
                                            break;
                                        }
                                        
                                    }
                                    
                                    if saved{
                                        
                                        self.updatedNOrders[index].customersTableData.remove(at: indexY)
                                        let newUpdatedInfo = OrdersInTableData(userUid: userUid, userName: userName, tableStatus: tableStatus, tableNum: tableNum, items: itemsFromOrderz, timeStamp: 0)
                                        
                                        self.updatedNOrders[index].customersTableData.append(newUpdatedInfo)
                                        
                                        self.updatedNOrders = self.updatedNOrders.sorted(by: { $0.tableNum < $1.tableNum })
                                        break;
                                        
                                    }else{
                                        
                                        // get all custumers at the table info
                                        var ordersInTable = self.updatedNOrders[index].customersTableData
                                        
                                        //add new customer information
                                        ordersInTable.append(ordersForCustomerInTable[0])
                                        
                                        self.updatedNOrders.remove(at: index)
                                        self.updatedNOrders.append(TablesData(tableStatus: tableStatus, tableNum: tableNum, customersTableData: ordersInTable))
                                        
                                        self.updatedNOrders = self.updatedNOrders.sorted(by: { $0.tableNum < $1.tableNum })
                                        break;
                                    }
                                    
                                }
                                
                            }
                        }
                        self.collectionView.reloadData()
                     
                    }
                    
                }
                
                //MARK: - new end
            }
        }
        
        
        //when a order is placed
        //        let ref = Database.database().reference().child("Restaurants").child(restaurantUid).child("Tables")
        
        sitRef.observe(.childChanged) { (snapshot) in
            
            if let dictionary = snapshot.value as? [String:AnyObject]{
                
                print("dictionary: \(dictionary)")

                var tableNm = ""
                for customersInTable in dictionary.values{
                    
                    if let numTable = customersInTable["tableNumber"] as? String{
                        tableNm = numTable
                    }
                }
                
                //check if request node for waiter exists
                if let tableWaiterRequest = dictionary["WaiterRequest"] as? Bool{

                    //if exists check if is true
                    if tableWaiterRequest{
                        
                        //a waiter has been requested, save table number that requested
                        self.waiterRequest[tableNm] = true
                        
                        //inform that the table has requested the waiter with an alert
                        let alert = UIAlertController(
                            title: "Staff Request",
                            message: String (format:"Table %@ is requesting a Waiter!", tableNm),
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        //the node exists but has already been dismissed (restaurant market the request as solved)
                        self.waiterRequest[tableNm] = false
                    }
                    
                }
                if let tableManagerRequest = dictionary["managerRequest"] as? Bool{
                    //managerRequest
                    
                    //if exists check if is true
                    if tableManagerRequest{
                        //a manager has been requested, save table number that requested
                        self.managerRequest[tableNm] = true
                        
                        //inform that the table has requested the Manager with an alert
                        let alert = UIAlertController(
                            title: "Staff Request",
                            message: String (format:"Table %@ is requesting a Manager!", tableNm),
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        //the node exists but has already been dismissed (restaurant market the request as solved)
                        self.managerRequest[tableNm] = false
                    }
                    
                }
                if let requestCheck =  dictionary["requestToPayWithCash"] as? Bool{
                    
                    if requestCheck{
                        //save that the table has requested the bill
                        self.billRequest[tableNm] = true
                        
                        
                        //inform that the table has requested the Manager with an alert
                        let alert = UIAlertController(
                            title: "Bill Request",
                            message: String (format:"Table %@ is requesting the Bill to pay in Cash!", tableNm),
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }else{
                        //the node exists but has already been dismissed (restaurant market the request as solved)
                        self.billRequest[tableNm] = false
                    }
                }
                
                //MARK: - multi functionality start
                
                print("dic : \(dictionary)")
                print("dic values: \(dictionary.values)")
                
                var modify = true
                
                
                
                
                
                for item in dictionary.values{
                    
                    print("Item: \(item)")
                    
                    
                    var itemsFromOrderz : [ItemFromOrder] = []
                    if let userOrder = item["order"] as? [String:Any]{
                        
                        
                        for items in userOrder{
                            
                            if let item = userOrder[items.key] as? [String:AnyObject]{
                                
                                if let dishName = item["dishName"] as? String,let dishPrice = item["dishPrice"] as? Double,let dishQuantity = item["dishQuantity"] as? Int,let servWFood = item["servWFood"] as? Bool{
                                    
                                    itemsFromOrderz.append(ItemFromOrder(dishName: dishName, dishPrice: dishPrice, dishQuantity: dishQuantity, servWFood: servWFood))
                                    
                                    print("dishName \(dishName), dishPrice: \(dishPrice), dishQuantity: \(dishQuantity), servWFood: \(servWFood)")
                                    
                                    
                                }
                            }
                        }
                    }
                    
                    if let tableNumber = item["tableNumber"] as? String, let tableStatus = item["tableStatus"] as? String, let userUid = item["userUid"] as? String, let timeStamp = item["timeStamp"] as? Int, let userName = item["userName"] as? String{
                        
                        print("blablavla \(tableStatus)  \(tableNumber)  \(userUid)   \(timeStamp)")
                        
                        
                        
                        
                        var tableNum = 0
                        
                        if let tNum = Int(tableNumber){
                            tableNum = tNum
                        }
                        
                        var ordersForCustomerInTable : [OrdersInTableData] = []
                        ordersForCustomerInTable.append(OrdersInTableData(userUid: userUid, userName: userName, tableStatus: tableStatus, tableNum: tableNum, items: itemsFromOrderz, timeStamp: timeStamp))
                        
                        
                        for (index,_) in self.updatedNOrders.enumerated().reversed(){
                            
                            if self.updatedNOrders[index].tableNum == tableNum  {
                                
                                if modify{
                                    self.updatedNOrders[index].tableStatus = ""
                                    self.updatedNOrders[index].customersTableData = []
                                    modify = false

                                }
                                
                                let newCustomerOrder = TablesData(tableStatus: tableStatus, tableNum: tableNum, customersTableData: ordersForCustomerInTable)
                                
                                
                                if self.updatedNOrders[index].customersTableData.count == 0{
                                    
                                    //there are no customers at the table to be checked (add current user to table with order)
                                    self.updatedNOrders.remove(at: index)
                                    self.updatedNOrders.append(newCustomerOrder)
                                    self.updatedNOrders = self.updatedNOrders.sorted(by: { $0.tableNum < $1.tableNum })
                                    break;
                                }else{
                                    //there are already customers at the table
                                    //check if the current user is already at the table
                                    
                                    var saved = false
                                    var indexY = -1
                                    for (indexy,_) in self.updatedNOrders[index].customersTableData.enumerated().reversed(){
                                        
                                        if self.updatedNOrders[index].customersTableData[indexy].userUid == userUid{
                                            
                                            saved = true
                                            indexY = indexy
                                            break;
                                        }
                                        
                                    }
                                    
                                    if saved{
                                        
                                        self.updatedNOrders[index].customersTableData.remove(at: indexY)
                                        self.updatedNOrders[index].tableStatus = "Order Placed"
                                        let newUpdatedInfo = OrdersInTableData(userUid: userUid, userName: userName, tableStatus: "Order Placed", tableNum: tableNum, items: itemsFromOrderz, timeStamp: timeStamp)
                                        
                                        self.updatedNOrders[index].customersTableData.append(newUpdatedInfo)
                                        
                                        self.updatedNOrders = self.updatedNOrders.sorted(by: { $0.tableNum < $1.tableNum })
                                        break;
                                        
                                    }else{
                                        
                                        // get all custumers at the table info
                                        var ordersInTable = self.updatedNOrders[index].customersTableData
                                        
                                        //add new customer information
                                        ordersInTable.append(ordersForCustomerInTable[0])
                                        
                                        self.updatedNOrders.remove(at: index)
                                        self.updatedNOrders.append(TablesData(tableStatus: "Order Placed", tableNum: tableNum, customersTableData: ordersInTable))
                                        
                                        self.updatedNOrders = self.updatedNOrders.sorted(by: { $0.tableNum < $1.tableNum })
                                        break;
                                    }
                                    
                                }
                                
                            }
                        }
                        self.keepRequestManager = false
                        self.collectionView.reloadData()
                    }
                    
                }
                
                
                
                //MARK: - multi functionality end
                
                self.collectionView.reloadData()
                //                print("userUid: \(userUid)")
                //                print("tableStatus: \(tableStatus)")
                //                print("tableNumbers: \(tableNumber)")
            }
        }
        
        sitRef.observe(.childRemoved, with: { (snapshot) in
            
            if let dictionary = snapshot.value as? [String:AnyObject]{
                
                print("dictionary: \(dictionary)")

                
                
                //MARK: - new start
                
                print("dic : \(dictionary)")
                
                
                for item in dictionary.values{
                    
                    print("Item: \(item)")
                    
                    
                    
                    if let tableNumber = item["tableNumber"] as? String, let userUid = item["userUid"] as? String{
                
                        var tableNum = 0
                        
                        if let tNum = Int(tableNumber){
                            tableNum = tNum
                        }
                        
                        //loop all tables to find the match
                        for (index,_) in self.updatedNOrders.enumerated().reversed(){
                            
                            if self.updatedNOrders[index].tableNum == tableNum  {
                                
                                //loop users sitted at the table to find match
                                for (indexy,_) in self.updatedNOrders[index].customersTableData.enumerated().reversed(){
                                    
                                    if self.updatedNOrders[index].customersTableData[indexy].userUid == userUid{
                                        
                                        self.updatedNOrders[index].customersTableData.remove(at: indexy)

                                        
                                        if self.updatedNOrders[index].customersTableData.count == 0{
                                        //if there is not one at the table anymore remove table status
                                            
                                            self.updatedNOrders[index].tableStatus = ""
                                        }else if(self.updatedNOrders[index].customersTableData.count == 1){
                                        //if there is only one user at the table the table gets that user table status
                                            
                                            self.updatedNOrders[index].tableStatus = self.updatedNOrders[index].customersTableData[0].tableStatus
                                        }else{
                                            //if there are still multiple users at the table display by level
                                            //if a user has places the order the table gets that status if no one ordered the table gets the sited status
                                            
                                            for customerInTable in self.updatedNOrders[index].customersTableData{
                                                
                                                if customerInTable.tableStatus != "Order Placed"{
                                                    self.updatedNOrders[index].tableStatus = "Seated"
                                                }else{
                                                    self.updatedNOrders[index].tableStatus = "Order Placed"
                                                    return;
                                                }
                                                
                                            }

                                        }
                                        
                                        
                                        self.collectionView.reloadData()
                                        return;
                                    }
                                    
                                }
                                
                            }
                        }
                        
                        
                        
                    
                    }
                }
                //MARK: - new end
                
                
                
            }
            
        }) { (error) in
            print(error)
        }

    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //        try! Auth.auth().signOut()s
        
        print(numOfTables)
        
        let restaurantUid = Auth.auth().currentUser?.uid
        
        Database.database().reference().child("Restaurants").child(restaurantUid!).observeSingleEvent(of: .value, with: { (snapshot) in
            // Get user value
            let value = snapshot.value as? NSDictionary
            
            let numberOfTables = value!["NumerOfTables"] as? Int ?? -1
            
            print(numberOfTables)
            
            UserDefaults.standard.setValue(numberOfTables, forKey: numOfTableKey)
            
            self.numOfTables = numberOfTables
            
            
            
            
            
            if self.updatedNOrders.count == 0 {
                let tableOrders : [OrdersInTableData] = []
                for index in 1...numberOfTables{
                    
                    self.updatedNOrders.append(TablesData(tableStatus: "", tableNum: index, customersTableData: tableOrders))
                    
                }
                self.collectionView.reloadData()
            }
            
            
        }) { (error) in
            print(error.localizedDescription)
        }
        
        
        //testing
//        for item in updatedOrders{
//            print(item.tableNum)
//        }
        
        slideMenuView.isHidden = true
        if let window = UIApplication.shared.keyWindow{
            window.addSubview(blackView)
        }
    }
    
    
    @objc func dismissMenu(){
        
        self.dismissMenuAnimated()
    }
    
    func dismissMenuAnimated(){
        
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            if let window = UIApplication.shared.keyWindow{
                
                self.blackView.alpha = 0
                
                self.slideMenuView.frame = CGRect(x: -self.slideMenuView.frame.width, y: 0, width: self.slideMenuView.frame.width, height: self.slideMenuView.frame.height)
                self.blackView.frame = CGRect(x: 0, y: self.navigationBarHeight, width: window.frame.width, height: window.frame.height-self.navigationBarHeight)
                
                
                //                self.resetPwdView.frame = CGRect(x: 0 , y: window.frame.height, width: window.frame.width, height: window.frame.height/2)
                
            }
        }, completion: {(completion) in
            
            self.isMenuOpen = false
        })
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == TableDetailsSegue{
            
            let detailsVc = segue.destination as! TableDetailsViewController
            
//            detailsVc.getTableOrder = updatedOrders[selectedTableOrderIndex]
//            print(String(selectedTableOrderIndex+1))
            
            detailsVc.getTableInfo = updatedNOrders[selectedTableOrderIndex]
            print(String(selectedTableOrderIndex+1))
            
            //check if the tapped table request an waiter / manager
            if let waiterRequestForTable = waiterRequest[String(selectedTableOrderIndex+1)]{
                
                print(waiterRequestForTable)
                detailsVc.getWaiterRequested = waiterRequestForTable
            }
            if let managerRequestForTable = managerRequest[String(selectedTableOrderIndex+1)]{
                
                print(managerRequestForTable)
                detailsVc.getManagerRequested = managerRequestForTable
            }
            if let requestedBill = billRequest[String(selectedTableOrderIndex+1)]{
                print(requestedBill)
                detailsVc.getBillRequested = requestedBill
            }
            
            
            
        }else if segue.identifier == qrCodeSegueId {
            
        }else if segue.identifier == OrderHistorySegue {
            
            
        }else if segue.identifier == editProfileSegue{
            
        }
    }
    
    
    
    @IBAction func menuButtonTapped(_ sender: UIButton) {
        // use the tag property of the button pressed to know which Artist to send over
        print(sender.tag)
        
        UIView.animate(withDuration: 0.3, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            if let window = UIApplication.shared.keyWindow{
                
                self.blackView.alpha = 0
                
                self.slideMenuView.frame = CGRect(x: -self.slideMenuView.frame.width, y: 0, width: self.slideMenuView.frame.width, height: self.slideMenuView.frame.height)
                self.blackView.frame = CGRect(x: 0, y: self.navigationBarHeight, width: window.frame.width, height: window.frame.height-self.navigationBarHeight)
                
            }
        }, completion: {(completion) in
            
            if sender.tag == 1{
                //qrcode button
                self.performSegue(withIdentifier: qrCodeSegueId, sender: nil)
            }else if sender.tag == 2{
                //log out button
                try! Auth.auth().signOut()
                self.dismiss(animated: true, completion: nil)
            }else if sender.tag == 3{
                //order history button
                self.performSegue(withIdentifier: OrderHistorySegue, sender: nil)
            }else if sender.tag == 4{
                //menu button
                self.performSegue(withIdentifier: menuScreenSegue, sender: nil)
            }else if sender.tag == 5{
                //orders status button
                self.performSegue(withIdentifier: orderStatusSegue, sender: nil)
                
            }else if sender.tag == 6{
                //orders status button
                self.performSegue(withIdentifier: editProfileSegue, sender: nil)
                
            }
            
        })
        
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        
        
        
        selectedTableOrderIndex = indexPath.row
        
        performSegue(withIdentifier: TableDetailsSegue, sender: nil)
        
        print("selected \(indexPath.row)")
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 100, height: 100)
    }
    
    @IBAction func menuButton(_ sender: Any) {
        
        if let window = UIApplication.shared.keyWindow{
            
            if !isMenuOpen{
                
                
                slideMenuView.isHidden = false
                slideMenuView.frame = CGRect(x: -window.frame.width*0.6, y: 0, width: window.frame.width*0.6, height: window.frame.height)
                
                self.blackView.frame = CGRect(x: 0, y: self.navigationBarHeight, width: window.frame.width, height: window.frame.height-navigationBarHeight)
                
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                    
                    self.slideMenuView.frame = CGRect(x: 0, y: 0, width: window.frame.width*0.6 , height: window.frame.height                    )
                    self.slideMenuView.layer.insertSublayer(self.setGradientBackground(), at: 0)
                    self.blackView.alpha = 1
                    self.blackView.frame = CGRect(x: self.slideMenuView.frame.width, y: self.navigationBarHeight, width: window.frame.width-self.slideMenuView.frame.width, height: window.frame.height-self.navigationBarHeight)
                    
                }, completion: {(completion)in
                    
                    
                    self.isMenuOpen = true
                })
                
            }else{
                
                self.dismissMenuAnimated()
            }
        }
        
    }

    func setGradientBackground() -> CAGradientLayer {
        let colorTop =  UIColor(red: 182/255.0, green: 77/255.0, blue: 67/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 243/255.0, green: 198/255.0, blue: 143/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.slideMenuView.bounds
        
        return gradientLayer
    }
    
    
    func setCollectionWithInfo(restUid:String){
        
        let sitRef = Database.database().reference().child("Restaurants").child(restUid).child("Tables")
        
        sitRef.observeSingleEvent(of: .value) { (snapshot) in
            
            if let dictionary = snapshot.value as? [String:AnyObject]{
                
                print("dictionary: \(dictionary)")
                
                var tableNm = ""
                for customersInTable in dictionary.values{
                    
                    if let numTable = customersInTable["tableNumber"] as? String{
                        tableNm = numTable
                    }
                }
                
                //check if request node for waiter exists
                if let tableWaiterRequest = dictionary["WaiterRequest"] as? Bool{
                    
                    //if exists check if is true
                    if tableWaiterRequest{
                        
                        //a waiter has been requested, save table number that requested
                        self.waiterRequest[tableNm] = true
                        
                        //inform that the table has requested the waiter with an alert
                        let alert = UIAlertController(
                            title: "Staff Request",
                            message: String (format:"Table %@ is requesting a Waiter!", tableNm),
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        //the node exists but has already been dismissed (restaurant market the request as solved)
                        self.waiterRequest[tableNm] = false
                    }
                    
                }
                if let tableManagerRequest = dictionary["managerRequest"] as? Bool{
                    //managerRequest
                    
                    //if exists check if is true
                    if tableManagerRequest{
                        //a manager has been requested, save table number that requested
                        self.managerRequest[tableNm] = true
                        
                        //inform that the table has requested the Manager with an alert
                        let alert = UIAlertController(
                            title: "Staff Request",
                            message: String (format:"Table %@ is requesting a Manager!", tableNm),
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                    }else{
                        //the node exists but has already been dismissed (restaurant market the request as solved)
                        self.managerRequest[tableNm] = false
                    }
                    
                }
                if let requestCheck =  dictionary["requestToPayWithCash"] as? Bool{
                    
                    if requestCheck{
                        //save that the table has requested the bill
                        self.billRequest[tableNm] = true
                        
                        
                        //inform that the table has requested the Manager with an alert
                        let alert = UIAlertController(
                            title: "Bill Request",
                            message: String (format:"Table %@ is requesting the Bill to pay in Cash!", tableNm),
                            preferredStyle: .alert
                        )
                        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
                        self.present(alert, animated: true, completion: nil)
                        
                        
                    }else{
                        //the node exists but has already been dismissed (restaurant market the request as solved)
                        self.billRequest[tableNm] = false
                    }
                }
                
                //MARK: - multi functionality start
                
                print("dic : \(dictionary)")
                print("dic values: \(dictionary.values)")
                
                var modify = true
                
                
                
                if let tablenum = dictionary["tableNumber"] as? String{
                    print(tablenum)
                }
                
                for item in dictionary.values{
                    
                    print("Item: \(item)")
//                    print("Itemvalue: \(item["EjRUb3767HUHB8HwhY3QPNJj2eS2"] as? NSDictionary)")
                    
                    
                    
                    if let tableblock = item as? [String:AnyObject]{
                        print("block \(tableblock)")
                        print("uid testing: \(tableblock.keys)")
                        print("uid values testing: \(tableblock.values)")
                        
                        var uids : [String] = []
                        for key in tableblock.keys{
                            if key.count > 25{

                                print("uidbla: \(key)")
                                uids.append(key)
                            }
                        }
                        

                        for uid in uids{
                            
                            if let valuesz = item[uid] as? [String:Any]{
                                
                                print("valuez : \(valuesz)")
//
//                                print("table numbver : \(valuesz["tableNumber"] as? String)")
//
//                                print("table userName : \(valuesz["userName"] as? String)")
//                                print("table tableStatus : \(valuesz["tableStatus"] as? String)")
                                
                                
                                var itemsFromOrderz : [ItemFromOrder] = []
                                if let userOrder = valuesz["order"] as? [String:Any]{

                                    for items in userOrder{
                                        
                                        if let item = userOrder[items.key] as? [String:AnyObject]{
                                            
                                            if let dishName = item["dishName"] as? String,let dishPrice = item["dishPrice"] as? Double,let dishQuantity = item["dishQuantity"] as? Int,let servWFood = item["servWFood"] as? Bool{
                                                
                                                itemsFromOrderz.append(ItemFromOrder(dishName: dishName, dishPrice: dishPrice, dishQuantity: dishQuantity, servWFood: servWFood))
                                                
                                                print("dishName \(dishName), dishPrice: \(dishPrice), dishQuantity: \(dishQuantity), servWFood: \(servWFood)")
                                                
                                                
                                            }
                                        }
                                    }
                                }
                                //
                                if let tableNumber = valuesz["tableNumber"] as? String, let tableStatus = valuesz["tableStatus"] as? String, let userUid = valuesz["userUid"] as? String, let timeStamp = valuesz["timeStamp"] as? Int, let userName = valuesz["userName"] as? String{
                                    
                                    print("blablavla \(tableStatus)  \(tableNumber)  \(userUid)   \(timeStamp)")
                                    
                                    
                                    
                                    
                                    var tableNum = 0
                                    
                                    if let tNum = Int(tableNumber){
                                        tableNum = tNum
                                    }
                                    
                                    var ordersForCustomerInTable : [OrdersInTableData] = []
                                    ordersForCustomerInTable.append(OrdersInTableData(userUid: userUid, userName: userName, tableStatus: tableStatus, tableNum: tableNum, items: itemsFromOrderz, timeStamp: timeStamp))
                                    
                                    
                                    for (index,_) in self.updatedNOrders.enumerated().reversed(){
                                        
                                        if self.updatedNOrders[index].tableNum == tableNum  {
                                            
                                            if modify{
                                                self.updatedNOrders[index].tableStatus = ""
                                                self.updatedNOrders[index].customersTableData = []
                                                modify = false
                                                
                                            }
                                            
                                            let newCustomerOrder = TablesData(tableStatus: tableStatus, tableNum: tableNum, customersTableData: ordersForCustomerInTable)
                                            
                                            
                                            if self.updatedNOrders[index].customersTableData.count == 0{
                                                
                                                //there are no customers at the table to be checked (add current user to table with order)
                                                self.updatedNOrders.remove(at: index)
                                                self.updatedNOrders.append(newCustomerOrder)
                                                self.updatedNOrders = self.updatedNOrders.sorted(by: { $0.tableNum < $1.tableNum })
                                                break;
                                            }else{
                                                //there are already customers at the table
                                                //check if the current user is already at the table
                                                
                                                var saved = false
                                                var indexY = -1
                                                for (indexy,_) in self.updatedNOrders[index].customersTableData.enumerated().reversed(){
                                                    
                                                    if self.updatedNOrders[index].customersTableData[indexy].userUid == userUid{
                                                        
                                                        saved = true
                                                        indexY = indexy
                                                        break;
                                                    }
                                                    
                                                }
                                                
                                                if saved{
                                                    
                                                    self.updatedNOrders[index].customersTableData.remove(at: indexY)
                                                    self.updatedNOrders[index].tableStatus = "Order Placed"
                                                    let newUpdatedInfo = OrdersInTableData(userUid: userUid, userName: userName, tableStatus: "Order Placed", tableNum: tableNum, items: itemsFromOrderz, timeStamp: timeStamp)
                                                    
                                                    self.updatedNOrders[index].customersTableData.append(newUpdatedInfo)
                                                    
                                                    self.updatedNOrders = self.updatedNOrders.sorted(by: { $0.tableNum < $1.tableNum })
                                                    break;
                                                    
                                                }else{
                                                    
                                                    // get all custumers at the table info
                                                    var ordersInTable = self.updatedNOrders[index].customersTableData
                                                    
                                                    //add new customer information
                                                    ordersInTable.append(ordersForCustomerInTable[0])
                                                    
                                                    self.updatedNOrders.remove(at: index)
                                                    self.updatedNOrders.append(TablesData(tableStatus: "Order Placed", tableNum: tableNum, customersTableData: ordersInTable))
                                                    
                                                    self.updatedNOrders = self.updatedNOrders.sorted(by: { $0.tableNum < $1.tableNum })
                                                    break;
                                                }
                                                
                                            }
                                            
                                        }
                                    }
                                    self.keepRequestManager = false
                                    self.collectionView.reloadData()
                                }
                            }
                            
                        }
                        
                    }

                    
                    var itemsFromOrderz : [ItemFromOrder] = []
                    if let userOrder = item["order"] as? [String:Any]{
                        
                        
                        for items in userOrder{
                            
                            if let item = userOrder[items.key] as? [String:AnyObject]{
                                
                                if let dishName = item["dishName"] as? String,let dishPrice = item["dishPrice"] as? Double,let dishQuantity = item["dishQuantity"] as? Int,let servWFood = item["servWFood"] as? Bool{
                                    
                                    itemsFromOrderz.append(ItemFromOrder(dishName: dishName, dishPrice: dishPrice, dishQuantity: dishQuantity, servWFood: servWFood))
                                    
                                    print("dishName \(dishName), dishPrice: \(dishPrice), dishQuantity: \(dishQuantity), servWFood: \(servWFood)")
                                    
                                    
                                }
                            }
                        }
                    }
                    
                }
                }
                
            
                self.collectionView.reloadData()
            
            }
        }
    @IBAction func refresh(_ sender: Any) {
        
        var restaurantUid = ""
        if let Uid = Auth.auth().currentUser?.uid{
            restaurantUid = Uid
        }
        
        setCollectionWithInfo(restUid: restaurantUid)
    }
}

