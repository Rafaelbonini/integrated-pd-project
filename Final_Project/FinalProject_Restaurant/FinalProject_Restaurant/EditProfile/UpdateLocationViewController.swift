//
//  UpdateLocationViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 2/4/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import CoreLocation
import MapKit
import Firebase
import JGProgressHUD

class UpdateLocationViewController: UIViewController, CLLocationManagerDelegate{

    
    
    let hud = JGProgressHUD(style: .dark)
    
    @IBOutlet weak var locationStreet: UITextField!
    @IBOutlet weak var locationCity: UITextField!
    @IBOutlet weak var locationState: UITextField!
    @IBOutlet weak var locationZipCode: UITextField!
    
    let manager = CLLocationManager()
    
    var latitude = ""
    var longitude = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        manager.delegate = self
        
        
        
        

    }
    
    @IBAction func updateLocation(_ sender: Any) {
        
        let geocoder = CLGeocoder()
        
        //get input location for text fields
        if let street = locationStreet.text, let city = locationCity.text, let state = locationState.text, let zipCode = locationZipCode.text{
            
            //verify that input is not empty
            if street.count != 0 && city.count != 0 && state.count != 0 && zipCode.count != 0{
                
//                locationFeedback.alpha = 0
                
                let address = "\(street), \(city), \(state) \(zipCode)"
                
                //try to get the point location from the input address
                geocoder.geocodeAddressString(address, completionHandler: {(placemarks, error) -> Void in
                    if((error) != nil){
                        print("Error", error ?? "")
                    }
                    if let placemark = placemarks?.first {
                        let coordinates:CLLocationCoordinate2D = placemark.location!.coordinate
                        print("Lat: \(coordinates.latitude) -- Long: \(coordinates.longitude)")
                        
                        //save latitude and longitude representation from the address
                        self.latitude = String(coordinates.latitude)
                        self.longitude = String(coordinates.longitude)
                        
                        let mapSnapshotOptions = MKMapSnapshotter.Options()
                        
                        let location = CLLocationCoordinate2DMake(coordinates.latitude, coordinates.longitude)
                        
                        let region = MKCoordinateRegion(center: location, latitudinalMeters: 350, longitudinalMeters: 350)
                        mapSnapshotOptions.region = region
                        
                        //Get image of current location
                        // Set the scale of the image. We'll just use the scale of the current device, which is 2x scale on Retina screens.
                        mapSnapshotOptions.scale = UIScreen.main.scale
                        
                        // Set the size of the image output.
                        mapSnapshotOptions.size = CGSize(width: 300, height: 300)
                        
                        // Show buildings and Points of Interest on the snapshot
                        mapSnapshotOptions.showsBuildings = true
                        mapSnapshotOptions.showsPointsOfInterest = true
                        
                        let snapShotter = MKMapSnapshotter(options: mapSnapshotOptions)
                        
                        snapShotter.start(completionHandler: { (snapshot, error) in
                            
                            guard let snapshot = snapshot, error == nil else {
                                if let error = error{
                                print("\(error)")
                                }
                                
                                
                                let alert = UIAlertController(title: "Location", message: "Please enter a valid address!", preferredStyle: .alert)
                                alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                                self.present(alert, animated: true)
                                return
                            }
                            
                            
                            //set an pin at the center of the map image
                            let imageView = UIImageView(frame: CGRect(x: 10, y: 50, width: 250, height: 230))
                            
                            UIGraphicsBeginImageContextWithOptions(CGSize(width: 300, height: 300), true, 0)
                            snapshot.image.draw(at: .zero)
                            
                            let pinView = MKPinAnnotationView(annotation: nil, reuseIdentifier: nil)
                            let pinImage = pinView.image
                            
                            var point = snapshot.point(for: coordinates)
                            
                            if imageView.bounds.contains(point) {
                                let pinCenterOffset = pinView.centerOffset
                                point.x -= pinView.bounds.size.width / 2
                                point.y -= pinView.bounds.size.height / 2
                                point.x += pinCenterOffset.x
                                point.y += pinCenterOffset.y
                                pinImage?.draw(at: point)
                            }
                            
                            let image = UIGraphicsGetImageFromCurrentImageContext()
                            
                            UIGraphicsEndImageContext()
                            
                            // do whatever you want with this image, e.g.
                            
                            DispatchQueue.main.async {
                                //display map image from set address with a pin at the center in alert for confirmation
                                let showAlert = UIAlertController(title: "Confirm Location", message: nil, preferredStyle: .alert)
                                
                                imageView.image = image //  image here
                                showAlert.view.addSubview(imageView)
                                let height = NSLayoutConstraint(item: showAlert.view, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 335)
                                let width = NSLayoutConstraint(item: showAlert.view, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 250)
                                
                                showAlert.view.addConstraint(height)
                                showAlert.view.addConstraint(width)
                                showAlert.addAction(UIAlertAction(title: "Confirm", style: .default, handler: { action in
                                    
                                    guard let restaurantUid = Auth.auth().currentUser?.uid else{return}
                                    // update location value in database
                                    let ref = Database.database().reference().child("Restaurants").child(restaurantUid).child("location")

                                    ref.updateChildValues(["latitude":self.latitude,"longitude":self.longitude])

                                    
                                    let alert = UIAlertController(title: "Location Updated", message: "New Location has been set!", preferredStyle: .alert)
                                    self.present(alert, animated: true)
                                    
                                    
                                    let when = DispatchTime.now() + 2
                                    DispatchQueue.main.asyncAfter(deadline: when){
                                        // your code with delay
                                        alert.dismiss(animated: true, completion: nil)
                                        self.navigationController?.popViewController(animated: true)
                                    }
                                    
                                    
                                    
                                }))
                                showAlert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: { action in
                                    // actions
                                    self.latitude = ""
                                    self.longitude = ""
                                }))
                                self.present(showAlert, animated: true, completion: nil)
                            }
                            
                            
                        })
                        
                    }
                })
                
                
            }else{
//                locationFeedback.alpha = 1
            }
        }
   
    }
    
    @IBAction func useCurrentLocation(_ sender: Any) {
        hud.textLabel.text = "Getting current Location"
        hud.show(in: self.view)
        manager.requestWhenInUseAuthorization()
        manager.requestLocation()
        
    }
    
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Failed to find user's location: \(error.localizedDescription)")
        hud.dismiss(animated: true)
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let location = locations.first {
            
            
            
            print("Found user's location: \(location)")
            latitude = String(location.coordinate.latitude)
            longitude = String(location.coordinate.longitude)
            
            hud.textLabel.text = "Updating Location"
            guard let restaurantUid = Auth.auth().currentUser?.uid else{return}
            // update location value in database
            let ref = Database.database().reference().child("Restaurants").child(restaurantUid).child("location")
            
            ref.updateChildValues(["latitude":self.latitude,"longitude":self.longitude])
            
            
            let alert = UIAlertController(title: "Location Updated", message: "New Location has been set!", preferredStyle: .alert)
            self.present(alert, animated: true)
            
            hud.dismiss(animated: true)

            let when = DispatchTime.now() + 2
            DispatchQueue.main.asyncAfter(deadline: when){
                // your code with delay
                alert.dismiss(animated: true, completion: nil)
                self.navigationController?.popViewController(animated: true)
            }
        }

    }
    
    @IBAction func dismissLocation(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
}
