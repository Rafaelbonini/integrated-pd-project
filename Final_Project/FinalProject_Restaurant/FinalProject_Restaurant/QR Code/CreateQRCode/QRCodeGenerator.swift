//
//  QRCodeGenerator.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 2/4/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import Foundation
import UIKit


class QRCodeGenerator {
    
    class func generateQRCodeFromString(_ strQR:String) -> CIImage {
        let dataString = strQR.data(using: String.Encoding.isoLatin1)
        
        let qrFilter = CIFilter(name:"CIQRCodeGenerator")
        qrFilter?.setValue(dataString, forKey: "inputMessage")
        
        let transform = CGAffineTransform(scaleX: 50, y: 50)
        
        if let output = qrFilter!.outputImage?.transformed(by: transform) {
            return  output
        }
//        return (qrFilter?.outputImage)!
        return (qrFilter?.outputImage)!
    }
    
    
    
    class func convert(_ cmage:CIImage) -> UIImage
    {
        let context:CIContext = CIContext.init(options: nil)
        let cgImage:CGImage = context.createCGImage(cmage, from: cmage.extent)!
        let image:UIImage = UIImage.init(cgImage: cgImage)
        return image
    }
    
}
