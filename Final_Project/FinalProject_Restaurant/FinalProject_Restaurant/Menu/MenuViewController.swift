//
//  MenuViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/29/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
import JGProgressHUD
import SwiftMessages

private let cellId = "menuCell"
private let detailsSegueId = "ToDetailsSegue"
public let editedKey = "editedItem"
private let newItemSegueId = "newItemSegue"
class MenuViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var restaurantMenu : [MenuModel] = []
    var selectedRow = -1
    var tableSections : [String] = []
    var cellsInSectionsDic : [String:Int] = [:]
    var selectedSection = -1
    let restaurantUid = Auth.auth().currentUser!.uid
    let hud = JGProgressHUD(style: .dark)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        
        hud.textLabel.text = "Loading Menu"
        hud.show(in: self.view)
        
        tableView.dataSource = self
        tableView.delegate = self
        
        
        UserDefaults.standard.setValue("Unchanged", forKey: editedKey)
       getDataFromDB()
        
    }
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return tableSections.count
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let cellsInSection = cellsInSectionsDic[tableSections[section]]{
            return cellsInSection
        }
        
        return 0
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MenuCellTableViewCell
     
        
        
        if restaurantMenu.count != 0{
            
            var currentRow = indexPath.row
            
            var sectionToIndex = indexPath.section
            print("section: \(indexPath.section)")
            
            if indexPath.section != 0{
                
                sectionToIndex = sectionToIndex-1
                
                for index in 0...sectionToIndex{
                    
                    currentRow += cellsInSectionsDic[tableSections[index]]!
                }
            }
            
            print("current row: \(currentRow)")
            
            print("section to index: \(sectionToIndex)")
            
            if restaurantMenu[currentRow].category == tableSections[indexPath.section]{
                
                //              cell.itemImage.image = category1[indexPath.row].image
                cell.itemName.text = restaurantMenu[currentRow].dishName
                cell.itemDesc.text = restaurantMenu[currentRow].desc
                cell.itemPrice.text =  "$\(restaurantMenu[currentRow].dishPrice)"
                

                
                
                print(currentRow)
                print(restaurantMenu[currentRow].hasImg)
                if restaurantMenu[currentRow].hasImg{
                    
                    print("resturant image name: \(restaurantUid)\(restaurantMenu[currentRow].index).jpg")
                    
                    print("image name : \(restaurantMenu[currentRow].index).jpg")
                    
                    let profileImageRef = Storage.storage().reference().child("MenuItem/\(restaurantUid)\(restaurantMenu[currentRow].index).jpg")
                    
                    profileImageRef.getData(maxSize: 1 * 1024 * 1024) { (data, error) in
                        
                        if error != nil{
                            print("error ocurred getting image from database")
                        }else{
                            
                            let image = UIImage(data: data!)
                            
                            self.restaurantMenu[currentRow].image = image
                            
                            
                            DispatchQueue.main.async {
                                cell.itemImage.image = image
                            }
                        }
                        
                    }
                }else{
                    cell.itemImage.image = nil
                }
                
                print("restaurant imag status : \(restaurantMenu[currentRow].image)")

                
                
            }
   
        }
        
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        selectedRow = indexPath.row
        var sectionToIndex = indexPath.section
        selectedRow = indexPath.row
        if indexPath.section != 0{
            
            sectionToIndex = sectionToIndex-1
            
            for index in 0...sectionToIndex{
                
                selectedRow += cellsInSectionsDic[tableSections[index]]!
                
            }
            
        }
        
        self.performSegue(withIdentifier: detailsSegueId, sender: nil)
        
        
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return tableSections[section]
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let label = UILabel()
        label.text = tableSections[section]
        label.frame = CGRect(x: 4, y: 0, width: UIScreen.main.bounds.width-70, height: 35)
        label.font = UIFont(name: "Avenir Medium", size: 18)
        label.adjustsFontSizeToFitWidth = true
        
        let button = UIButton(type: .system)
        button.setTitle("Add Item", for: .normal)
        button.backgroundColor = UIColor.lightGray
        button.contentHorizontalAlignment = .right
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        button.addSubview(label)
        
        button.addTarget(self, action: #selector(handleAddITem), for: .touchUpInside)
        
        button.tag = section
        
        return button
        
    }
    
    @objc func handleAddITem(button:UIButton){
    
        print("button tag: \(button.tag)")
        
        selectedSection = button.tag
        
        performSegue(withIdentifier: newItemSegueId, sender: nil)
    
    
    }
    override func viewWillAppear(_ animated: Bool) {

     
        let updatedItem = UserDefaults.standard.string(forKey: editedKey)
        
        if updatedItem != "Unchanged"{
            
            UserDefaults.standard.setValue("Unchanged", forKey: editedKey)
            
            restaurantMenu = []
            getDataFromDB()
            
        }
        
        
    }
    

    func setTable(){
        
        tableSections = []
        cellsInSectionsDic = [:]
            
            for menuItem in restaurantMenu{
                //get num of sections / title of sections
                if !tableSections.contains(menuItem.category){ tableSections.append(menuItem.category) }
                
                //get number of cells for each sections
                if cellsInSectionsDic.keys.contains(menuItem.category) {
                    // contains key
                    let newValue = cellsInSectionsDic[menuItem.category]! + 1
                    
                    cellsInSectionsDic[menuItem.category] = newValue
                    
                } else {
                    // does not contain key
                    cellsInSectionsDic[menuItem.category] = 1
                }
            }
        
        
        tableSections = tableSections.sorted()
        print("table sections: \(tableSections)")
        print("table cellsInSectionsDic: \(cellsInSectionsDic.reversed())")
    }
    
    @IBAction func newCategoryOrFile(_ sender: Any) {
        

        var inputTextField: UITextField?;
        
        let actionSheet = UIAlertController(title: "Choose Option", message: "", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "New Category", style: .default, handler: { (action:UIAlertAction) in
            
            let alert = UIAlertController(title: "New Category", message: "Enter the new menu category.", preferredStyle: UIAlertController.Style.alert)

            alert.addTextField(configurationHandler: {(textField: UITextField!) in
                textField.placeholder = "Enter new category"
                
                inputTextField = textField
                


            })
            

            alert.addAction(UIAlertAction(title: "Add", style: UIAlertAction.Style.default, handler: { (action) in
                
                let category : String = (inputTextField?.text)!
                
                print("BOOM! I received '\(category)'");
                
                
                    
                    //if the new category does not already exit
                    if !self.tableSections.contains(category){
                        
                        //add to current list of categories
                        self.tableSections.append(category)
                        
                        self.cellsInSectionsDic[category] = 0
                        
                        print(self.tableSections)
                        print(self.cellsInSectionsDic)
                        
                        self.tableView.reloadData()
                    }
                
            }))
            
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
            
            self.present(alert, animated: true, completion: nil)
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Import Menu from File", style: .default, handler: { (action:UIAlertAction) in
            

        }))
        
        actionSheet.addAction(UIAlertAction(title: "Import Menu from E-mail", style: .default, handler: { (action:UIAlertAction) in
            
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == detailsSegueId{
            
            let detailsVC = segue.destination as! DetailsMenuViewController
            
            detailsVC.getRestaurantMenu = restaurantMenu[selectedRow]
            detailsVC.getNumOfMenuItems = restaurantMenu.count
            
        }else if segue.identifier == newItemSegueId{
            
            let detailsVC = segue.destination as! DetailsMenuViewController
            
            detailsVC.getCategory = tableSections[selectedSection]
            
            print(selectedSection)
            
            
        }
    }
 
    
    func getDataFromDB(){
        
        guard let restaurantUid = Auth.auth().currentUser?.uid else{
            return
        }
        
        
        
        
        
        Database.database().reference().child("Restaurants").child(restaurantUid).child("Menu").observeSingleEvent(of: .value) { (snapshot) in
            
            print(restaurantUid)
            
            let value = snapshot.value as? NSDictionary
            
            if value == nil {
                
                
                self.hud.dismiss(animated: true)
                
                self.setMessage(body: "Menu has not been set yet")

                
                return
            }
            
            let numOfDishes = value?["numOfDishes"] as? Int ?? -1
            var dishName = ""
            var dishCategory = ""
            var dishDesc = ""
            var dishPortion = ""
            var dishPrice = ""
            var dishHasImg = false
            
            
            
            for index in 1...numOfDishes{
                
                let foodIndex = value?[String(index)] as? NSDictionary
                
                
                dishName = foodIndex?["dishName"] as? String ?? ""
                dishCategory = foodIndex?["dishCategory"] as? String ?? ""
                dishDesc = foodIndex?["dishDesc"] as? String ?? ""
                dishPortion = foodIndex?["dishPortion"] as? String ?? ""
                dishPrice = foodIndex?["dishPrice"] as? String ?? ""
                dishHasImg = foodIndex?["dishHasImage"] as? Bool ?? false
                
                let doubleDishPrice = Double(dishPrice)
                
                
                self.restaurantMenu.append(MenuModel(dishName: dishName, dishPrice: doubleDishPrice!, desc: dishDesc, image: nil, category: dishCategory, portion: dishPortion, index: index, hasImg: dishHasImg))
                
                print("name \(dishName), name \(dishPrice)")
                
            }

            
            if self.restaurantMenu.count != 0 {
                
                self.restaurantMenu = self.restaurantMenu.sorted(by: { $0.category < $1.category})
                
                
                self.setTable()
                
                
            }
            self.tableView.reloadData()
            self.hud.dismiss(animated: true)
        }
    }
    
    func setMessage(body:String){
        
        let view = MessageView.viewFromNib(layout: .centeredView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        view.configureContent(title: "Menu", body: body)
        
        view.button?.isHidden = true
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
        
    }
    
}
