//
//  RestaurantItemsTestModel.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/13/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import Foundation
import UIKit

class RestaurantItemsTestModel{
    
    let image : UIImage
    let name : String
    let desc : String
    let portionSize : String
    let servWithFood : Bool
    let price : Double
    let quantity : Int
    
    init(image:UIImage,name : String, desc : String, portionSize : String, servWithFood : Bool, price : Double,quantity : Int) {
    
        self.image = image
         self.name = name
         self.desc = desc
         self.portionSize = portionSize
         self.servWithFood = servWithFood
         self.price = price
        self.quantity = quantity
    }
    
}
