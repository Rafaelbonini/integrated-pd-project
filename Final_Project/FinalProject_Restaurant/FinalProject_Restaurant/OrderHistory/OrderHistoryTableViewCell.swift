//
//  OrderHistoryTableViewCell.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/25/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

class OrderHistoryTableViewCell: UITableViewCell {

    
    @IBOutlet weak var orderNumLabel: UILabel!
    @IBOutlet weak var orderDateLabel: UILabel!
    @IBOutlet weak var orderPriceLabel: UILabel!
    
    var restaurantInfo : OrderHistory?{
        
        didSet{
            
            let totalPrice = "$ \(restaurantInfo!.totalOrderCost.clean)"
            let date = NSDate(timeIntervalSince1970: restaurantInfo!.timeStamp)
            let dayTimePeriodFormatter = DateFormatter()
    
            dayTimePeriodFormatter.dateFormat = "MMM/dd/YYYY"
            
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            
//            orderNumLabel.text = restaurantInfo?.restaurantName
            orderPriceLabel.text = totalPrice
            orderDateLabel.text = dateString
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
