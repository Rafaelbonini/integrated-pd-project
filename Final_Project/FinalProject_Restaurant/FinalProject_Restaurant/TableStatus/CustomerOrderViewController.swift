//
//  CustomerOrderViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 3/27/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase


private let cellId = "CellId"
class CustomerOrderViewController: UIViewController, UITableViewDataSource,UITableViewDelegate {
    
    
    @IBOutlet weak var customerLabel: UILabel!
    @IBOutlet weak var tableNumLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalLabel: UILabel!
    var total = 0.0
    
    var getCustomerInfo : OrdersInTableData?
    var userOrder : [ItemFromOrder] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        if let customerInfo = getCustomerInfo{
            
            customerLabel.text = "Customer: \(customerInfo.userName)"
            tableNumLabel.text = "Table: \(customerInfo.tableNum)"
            
            let date = NSDate(timeIntervalSince1970: Double(customerInfo.timeStamp))
            let dayTimePeriodFormatter = DateFormatter()
            dayTimePeriodFormatter.dateFormat = "MMM/dd/YYYY hh:mm"
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            
            dateLabel.text = "Date: \(dateString)"
            
            userOrder =  customerInfo.items
            
            
            for item in userOrder{
                
                total += item.dishPrice * Double(item.dishQuantity)
                
            }
            self.totalLabel.text = "$\(total)"
        }
        
        
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return userOrder.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! CustomerOrderTableViewCell
        
        let price = userOrder[indexPath.row].dishPrice * Double(userOrder[indexPath.row].dishQuantity)

        var nameAndQuantity = "\(userOrder[indexPath.row].dishName)"
        nameAndQuantity = nameAndQuantity.truncate(length: 24, trailing: "...")
        let quantity = " x \(userOrder[indexPath.row].dishQuantity)"
        nameAndQuantity += quantity
        
        
        cell.dishPrice.text = "$\(price)"
        cell.dishNameQuantity.text = nameAndQuantity
        
        return cell
    }
    
    
    
    @IBAction func serviceEnded(_ sender: Any) {
        
        let alert = UIAlertController(title: "Service Ended", message: "The customer has Paid the order?", preferredStyle: .alert)
        
        alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        alert.addAction(UIAlertAction(title: "Order Paid", style: .default, handler: { (alertAction) in
            
            //add to history and remove from table
            
            guard let restaurantUid = Auth.auth().currentUser?.uid else{
                return
            }
            
            
            
            if let customerInfo = self.getCustomerInfo{
                
                let ref = Database.database().reference()
                
                let timeStamp = Int(Date().timeIntervalSince1970)
                
                let userUid = customerInfo.userUid
                
                let childRef = ref.child("Orders").childByAutoId()
                
                ref.child("Restaurants").child(restaurantUid).observeSingleEvent(of: .value) { (snapshot) in
                    
                    let value = snapshot.value as? NSDictionary
                    
                    let restaurantName = value?["username"] as? String
                    
                    if let name = restaurantName{
                        
                        
                        for (i,order) in self.userOrder.enumerated().reversed(){
                            
                            
                            childRef.child("order").child("dish\(i+1)").updateChildValues([
                                "dishName": order.dishName,
                                "dishPrice": order.dishPrice,
                                "dishQuantity": order.dishQuantity,
                                "servWFood": order.servWFood
                                ])
                        }
                        
                        childRef.updateChildValues(["restaurantUid":restaurantUid,"tableNum":customerInfo.tableNum,"timeStamp":timeStamp,"restaurantName":name,"customerUid":userUid,"orderTotalPrice":self.total])
                        
                        //reference the saved order to the user order history
                        ref.child("Users").child(userUid).child("orderHistory").updateChildValues([childRef.key:1])
                        ref.child("Users").child(userUid).child("curentOrderInfo").removeValue()
                        ref.child("Restaurants").child(restaurantUid).child("orderHistory").updateChildValues([childRef.key:1])
                        
                        
                        let deleteRef = Database.database().reference().child("Restaurants").child(restaurantUid).child("Tables").child("Table\(customerInfo.tableNum)").child(userUid)
                        deleteRef.removeValue()
                        
                    }
                }

            }
            
            self.navigationController?.popToRootViewController(animated: true)
        }))
        
        self.present(alert, animated: true)
    }
}
