//
//  VerificationViewController.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 2/7/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import SinchVerification
import SwiftMessages
import Firebase
import JGProgressHUD

private let goToQRSegue = "GoToQR"
private let verifyCodeSegue = "verifyCodeSegue"
class VerificationViewController: UIViewController,UINavigationControllerDelegate {

    @IBOutlet weak var buttonConstaintDistance: NSLayoutConstraint!
    @IBOutlet weak var authButton: UIButton!
    @IBOutlet weak var codeTextField: UITextField!
    @IBOutlet weak var codeLabel: UILabel!
    @IBOutlet weak var phoneNumberText: UITextField!
    @IBOutlet weak var backgroundView: UIImageView!
    
//    let applicationKey = "f99098e9-ec1a-4b35-951f-328f1d859342"
    
    let applicationKey = "8ef87af3-0648-4385-be44-0a528a9caef3"
    var defaultRegion = SINDeviceRegion.currentCountryCode()
    var countryCallingCode = ""
    var countryDisplayNam = ""
    var messageSent = false
    var finalNum = ""
    var editingNum : String?
    
    let hud = JGProgressHUD(style: .dark)
    

    //get registration info
    var getUserName : String?
    var getUserEmail : String?
    var getUserPassword : String?
    var getProfileImage : UIImage?
    
    weak var verification : SINVerificationProtocol?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.view.sendSubviewToBack(self.backgroundView)
        setTextFields()
        
        if editingNum == nil{
            Auth.auth().addStateDidChangeListener { (auth, user) in
                if user != nil{
                    
                    self.performSegue(withIdentifier: goToQRSegue, sender: nil)
                    
                }
            }
        }
        
        
        self.title = "Verify Phone Number"
        
        self.authButton.titleLabel?.text = "Request Code"
        self.phoneNumberText.isEnabled = true
        
        let minx = phoneNumberText.frame.midX
        codeTextField.frame = CGRect(x: minx-75 , y: phoneNumberText.frame.minY, width: 150, height: phoneNumberText.frame.height)
        codeTextField.alpha = 0
        phoneNumberText.becomeFirstResponder()

        // Do any additional setup after loading the view.
        var regions : [SINRegionInfo] = []
        
        let regionList = SINPhoneNumberUtil().regionList(with: NSLocale.current)
        regions = regionList.entries.sorted { (a:SINRegionInfo, b:SINRegionInfo) -> Bool in
            return a.countryDisplayName < b.countryDisplayName
        }

        print(defaultRegion)
        for region in regions{
            
            if defaultRegion == region.isoCountryCode{
                 countryCallingCode = "+\(region.countryCallingCode)"
                 countryDisplayNam = region.countryDisplayName
                print("+\(region.countryCallingCode) - \(region.isoCountryCode)")
                codeLabel.text = "\(countryDisplayNam) (\(countryCallingCode))"
                return
            }
        }
    }
    
    func setTextFields(){

        //set text field background color
        phoneNumberText.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 1);
        codeTextField.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 1);
        codeLabel.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        
        //set textfield placeholder text color
        phoneNumberText.attributedPlaceholder = NSAttributedString(string: "your phone number", attributes: [NSAttributedString.Key.foregroundColor: UIColor(displayP3Red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        codeTextField.attributedPlaceholder = NSAttributedString(string: "_ _ _ _", attributes: [NSAttributedString.Key.foregroundColor: UIColor(displayP3Red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        
        
        //set textfield text padding

    }
    
    @IBAction func validate(_ sender: Any) {
        
        
        if !messageSent{
            if let phoneNum = phoneNumberText.text{
                
                let number = "\(countryCallingCode)\(phoneNum)"
                finalNum = number
                
                let phoneNumberValidator = number.isPhoneNumber
                
                if (phoneNumberValidator){
                    hud.textLabel.text = "Sending Message"
                    hud.show(in: self.view)
                    
                    verification = SINVerification.smsVerification(withApplicationKey: applicationKey, phoneNumber: number)
                    
                    verification?.initiate(completionHandler: { (result, error) in
                        
                        if result.success{
                            //success
                            print("success")
                            self.hud.dismiss(animated: true)
                            
                            self.setMessage(body:"A message with the code has been sent to your phone number", error: false)
                            
                            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseInOut, animations: {
                                
                                self.authButton.titleLabel?.text = "Verify Code"
                                self.phoneNumberText.isEnabled = false
                                self.phoneNumberText.resignFirstResponder()
                                self.codeTextField.becomeFirstResponder()
                                self.codeTextField.alpha = 1
                                self.codeTextField.frame = CGRect(x: self.phoneNumberText.frame.midX-75, y: self.phoneNumberText.frame.maxY+15, width: 150, height: self.codeTextField.frame.height)
                                self.buttonConstaintDistance.constant = 90
                                self.codeTextField.becomeFirstResponder()
                                self.view.layoutIfNeeded()
                                self.messageSent = true
                            }) { (Bool) in
                                
                                
                            }
                            
                        }else{
                            self.setMessage(body: "Message failed!", error: true)
                        }
                    })
                    
                    
                }
                
            }
        }else{
            //message was sent - needs to verify message code

            if let code = codeTextField.text{
                
                hud.textLabel.text = "Matching Codes"
                hud.show(in: self.view)
                
                
                
                
                verification = SINVerification.smsVerification(withApplicationKey: applicationKey, phoneNumber: finalNum)
                
                //verify if the entered code matches the sms code
                verification!.verifyCode(code, completionHandler: { (success:Bool, error:Error?) in
                    
                    if success{
                        
//                        self.codeTextField.resignFirstResponder()
                        self.hud.dismiss(animated: true)
                        print("code verified, number can be saved for user")
                        
                        self.setMessage(body:"Phone Number Authenticated", error: false)
                        
                        
                        //if true previous view was register view else if editprofile
                        if self.getUserEmail != nil{
                            self.createUser()
                        }else{
                            self.updatePhoneNumber()
                        }
                        
                        
                    }else{
                        self.hud.dismiss(animated: true)
                        //incorrect code
                        
                        self.setMessage(body: "Code Incorrect", error: true)
                        if let error = error{
                        print(error)
                        }
                    }

                })
            }
        }
        
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
     
        
        

    }
    
    func setMessage(body:String,error:Bool){
        
        let view = MessageView.viewFromNib(layout: .centeredView)
        
        // Theme message elements with the warning style.
        if error{
            view.configureTheme(.error)
        }else{
            view.configureTheme(.success)
        }
        
        
        
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        view.configureContent(title: "Message", body: body)
        
        view.button?.isHidden = true
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
        
    }
    
    
    func updatePhoneNumber (){
        
        guard let userUid = Auth.auth().currentUser?.uid else{
            return
        }
        
        let ref = Database.database().reference().child("Users").child(userUid)
        ref.updateChildValues(["phone":finalNum])
        
    }
    
    func createUser(){
        
        hud.textLabel.text = "Signing Up"
        hud.show(in: self.view)

        
        Auth.auth().createUser(withEmail: getUserEmail!, password: getUserPassword!) { (firuser, error) in
            if(error != nil){
                print("error in create user: \(String(describing: error))")
                
                self.hud.dismiss(animated: true)
                // create the alert
                
                self.setMessage(body: "This account cannot be registered.", error: true)
                
                
                
            }else if let firuser = firuser{
                
                //set the user name
                let changeResquest = Auth.auth().currentUser?.createProfileChangeRequest();
                changeResquest?.displayName = self.getUserName;
                changeResquest?.commitChanges(completion: { (error) in
                    if (error != nil){
                        print("user name change request failed")
                    }else{
                        print("user name chaged!")
                    }
                })
                
                var newUser = User(userUid: "", userName: "", userEmail: "", profilePic: UIImage(), phoneNumber: "")
                if let image = self.getProfileImage{
                    newUser = User(userUid:firuser.user.uid , userName: self.getUserName!, userEmail: self.getUserEmail!, profilePic: image, phoneNumber: self.finalNum)
                }else{
                    newUser = User(userUid:firuser.user.uid , userName: self.getUserName!, userEmail: self.getUserEmail!, profilePic: UIImage(named: "profileImage")!, phoneNumber: self.finalNum)
                }
                
                
                newUser.save(completion: { (error) in
                    if error == nil{
                        print("error saving user to database")
                    }
                })
                
            }
        }
    }

//    @IBAction func goBack(_ sender: Any) {
//        self.dismiss(animated: true
//            , completion: nil)
//    }
    
//    override func viewWillDisappear(_ animated: Bool) {
//        self.navigationController?.setNavigationBarHidden(false, animated: false)
//    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    @IBAction func goback(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
