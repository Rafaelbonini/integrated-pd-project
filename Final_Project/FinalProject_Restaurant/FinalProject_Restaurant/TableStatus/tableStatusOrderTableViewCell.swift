//
//  tableStatusOrderTableViewCell.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/26/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

class tableStatusOrderTableViewCell: UITableViewCell {

    
    @IBOutlet weak var nameQuantityLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
