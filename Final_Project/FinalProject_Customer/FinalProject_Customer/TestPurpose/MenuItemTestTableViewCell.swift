//
//  MenuItemTestTableViewCell.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/13/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

class MenuItemTestTableViewCell: UITableViewCell {

    
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemTittle: UILabel!
    @IBOutlet weak var itemDesc: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
