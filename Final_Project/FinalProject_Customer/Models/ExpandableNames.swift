//
//  ExpandableNames.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 12/16/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import Foundation

struct ExpandableNames {
    
    var isExpanded: Bool
    let names: [MenuModel]
    
}
