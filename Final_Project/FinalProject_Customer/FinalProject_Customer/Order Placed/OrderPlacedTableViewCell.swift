//
//  OrderPlacedTableViewCell.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/23/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

class OrderPlacedTableViewCell: UITableViewCell {

    
    var restaurantItem : RestaurantItemsTestModel?{
        
        didSet{
         
            var itemNam = ""
            
            if let name = restaurantItem?.name, let qty = restaurantItem?.quantity{

                itemNam = name.truncate(length: 24, trailing: "...")
                itemNam += " x \(qty)"
            }

            let priceSum = (restaurantItem?.price)! * Double((restaurantItem?.quantity)!)

            
            itemPricesum.text = "$\(priceSum)"
            itemName.text = itemNam
        }
    }
    
    
    @IBOutlet weak var itemPricesum: UILabel!
    @IBOutlet weak var itemName: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
