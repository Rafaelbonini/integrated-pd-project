//
//  MenuItemsViewController.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/24/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase

private let cellId = "menuCell"
private let detailSegueId = "toDetailItem"
private let cartSegueId = "CartIdentifier"

class MenuItemsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate,DataEnteredDelegate {
    
    
    @IBOutlet weak var backbutton: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var menuView: UIView!
    var getRestaurantMenu : [MenuModel]?
    var RestaurantMenu : [MenuModel] = []
    var tableSections : [String] = []
    var cellsInSectionsDic : [String:Int] = [:]
    var isMenuOpen = false;
    var itemOrdered : RestaurantItemsTestModel?
    var itemsOrdered : [RestaurantItemsTestModel] = []
    var restaurantUid = ""
    var selectedRow = -1
    var menuSectionItemsArray : [ExpandableNames] = []
    @IBOutlet weak var navigationItemFTitle: UINavigationItem!
    
    
    func orderList(items: [RestaurantItemsTestModel]) {
        
        itemsOrdered = items
        
        for item in itemsOrdered{
            print("\(item.name)  quantity: \(item.quantity)")
        }
    }
    
    let blackView : UIView = {
        let bView = UIView(frame: .zero)
        bView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        return bView
    }()
    let gradientView : UIView = {
        let gView = UIView(frame: CGRect(x: 0, y: 0, width: 2000, height: 2000))
        gView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        return gView
    }()
    
    func setTable(){
        
//        if let restaurantMenu = getRestaurantMenu{
        
            //total num of cells
//            restaurantMenu.count
        if getRestaurantMenu != nil{
            for menuItem in RestaurantMenu{
                //get num of sections / title of sections
                if !tableSections.contains(menuItem.dishCategory){ tableSections.append(menuItem.dishCategory) }
                
                //get number of cells for each sections
                if cellsInSectionsDic.keys.contains(menuItem.dishCategory) {
                    // contains key
                    let newValue = cellsInSectionsDic[menuItem.dishCategory]! + 1
                    
                    cellsInSectionsDic[menuItem.dishCategory] = newValue
                    
                } else {
                    // does not contain key
                    cellsInSectionsDic[menuItem.dishCategory] = 1
                }
            }
            
        }
//        }
        
        tableSections = tableSections.sorted()
        print("table sections: \(tableSections)")
        print("table cellsInSectionsDic: \(cellsInSectionsDic.reversed())")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        menuView.isHidden = true
        
        let orderStatus = UserDefaults.standard.string(forKey: orderStatusKey)
        
        if orderStatus == ""{
            
            backbutton.isHidden = false
        }else{
            backbutton.isHidden = true
        }
        
        
        if let window = UIApplication.shared.keyWindow{
        menuView.frame = CGRect(x: -(window.frame.width/2), y: 0, width: window.frame.width/2, height: window.frame.height)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
       self.navigationItemFTitle.title = "Menu"
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        
        self.tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 44, right: 0);
        

        navigationController?.navigationBar.barTintColor = UIColor(displayP3Red: 182/255, green: 77/255, blue: 67/255, alpha: 1)
        
        if let menu = getRestaurantMenu{
            RestaurantMenu = menu
            
        }
        
        
        
        tableView.delegate = self
        tableView.dataSource = self
        
        
        restaurantUid = UserDefaults.standard.string(forKey: restaurantUidKey)!
        
        
        for bla in getRestaurantMenu!{
            print(bla.dishName)
            print(bla.dishPrice)
        }
        
        RestaurantMenu = RestaurantMenu.sorted(by: { $0.dishCategory < $1.dishCategory })
        

        
        for bla in getRestaurantMenu!{
            print(bla.dishName)
            print(bla.dishPrice)
        }
        
        setTable()
        
        var tempArrayItems : [MenuModel] = []
        var count = 0
        for item in RestaurantMenu{

            if item.dishCategory == tableSections[count]{
                tempArrayItems.append(item)
            }else{
                self.menuSectionItemsArray.append(ExpandableNames.init(isExpanded: true, names: tempArrayItems))
                tempArrayItems = []
                tempArrayItems.append(item)
                count+=1
                
            }
            
        }
        print("check")
        
        if let window = UIApplication.shared.keyWindow{
            window.addSubview(blackView)

        }
        
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissMenu)))
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(dismissMenu))
        swipeLeft.direction = UISwipeGestureRecognizer.Direction.left
        
        tableView.addGestureRecognizer(swipeLeft)
        
    }

    
    // MARK: - Table view data source
    
     func numberOfSections(in tableView: UITableView) -> Int {
        //return the number of sections
        return menuSectionItemsArray.count
    }
    
     func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // return the number of rows
        
        
        if !menuSectionItemsArray[section].isExpanded{
            return 0
        }
        
//        if let cellsInSection = cellsInSectionsDic[tableSections[section]]{
//            return cellsInSection
//
//        }
        
        return menuSectionItemsArray[section].names.count
    }
    
     func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! MenuItemTestTableViewCell
        
        
        
//        if let item = getRestaurantMenu{
        
            print(tableSections)
            for item in getRestaurantMenu!{
                print(item.dishName)
            }
            print(indexPath.section)
            print(RestaurantMenu[indexPath.row].dishCategory)
            print(RestaurantMenu[indexPath.row].dishName)
            
            
            var currentRow = indexPath.row
            
            var sectionToIndex = indexPath.section
            print("section: \(indexPath.section)")
            
            if indexPath.section != 0{
                
                sectionToIndex = sectionToIndex-1
                
                for index in 0...sectionToIndex{
                    
                    currentRow += cellsInSectionsDic[tableSections[index]]!
                    
                }
                
            }
            
            
            print("current row: \(currentRow)")
            
            
            print("section to index: \(sectionToIndex)")
            
            if RestaurantMenu[currentRow].dishCategory == tableSections[indexPath.section]{
                
               
                cell.itemTittle.text = RestaurantMenu[currentRow].dishName
                cell.itemDesc.text = RestaurantMenu[currentRow].dishDesc
                cell.itemPrice.text = "$ \(RestaurantMenu[currentRow].dishPrice)"
                
                if RestaurantMenu[currentRow].hasImg{
                    
                
                    
                    if RestaurantMenu[currentRow].hasImg{
                        
                        print("resturant image name: \(restaurantUid)\(RestaurantMenu[currentRow].index).jpg")
                        
                        print("image name : \(RestaurantMenu[currentRow].index).jpg")
                        
                        let profileImageRef = Storage.storage().reference().child("MenuItem/\(restaurantUid)\(RestaurantMenu[currentRow].index).jpg")
                        
                        profileImageRef.getData(maxSize: 1 * 1024 * 1024) { (data, error) in
                            
                            if error != nil{
                                print("error ocurred getting image from database")
                            }else{
                                
                                let image = UIImage(data: data!)
                                
//                                self.getRestaurantMenu[currentRow] .image = image
                                
                                self.RestaurantMenu[currentRow].image = image
                                
                                DispatchQueue.main.async {
                                    
                                    cell.itemImage.image = image
                                }
                            }
                            
                        }
                    }else{
                        cell.itemImage.image = nil
                    }
                    
                }
                
                
            }
        
        cell.imageView?.layer.masksToBounds = true
//        }
        return cell
    }
    
     func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        selectedRow = indexPath.row
        var sectionToIndex = indexPath.section
        selectedRow = indexPath.row
        if indexPath.section != 0{
            
            sectionToIndex = sectionToIndex-1
            
            for index in 0...sectionToIndex{
                
                selectedRow += cellsInSectionsDic[tableSections[index]]!
                
            }
            
        }
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        performSegue(withIdentifier: detailSegueId, sender: nil)
        
    }
    
    
     func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 100
    }
    
     func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return tableSections[section]
    }
    
     func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 35
    }
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let label = UILabel()
        label.text = tableSections[section]
        label.frame = CGRect(x: 4, y: 0, width: 300, height: 35)
        label.font = UIFont(name: "Avenir Black", size: 20)
        label.adjustsFontSizeToFitWidth = true
        
        let button = UIButton(type: .system)
        button.setTitle("Close", for: .normal)
        button.backgroundColor = UIColor(displayP3Red: 235/255, green: 235/255, blue: 235/255, alpha: 1)
        button.contentHorizontalAlignment = .right
        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
        button.setTitleColor(UIColor.black, for: .normal)
        button.addSubview(label)
        
        button.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
        
        button.tag = section
        
        return button 
    }
    
    @objc func handleExpandClose (button: UIButton){
        
        print("Trying to expand and close section...")
        
        let section = button.tag
        
        // we'll try to close the section first by deleting the rows
        var indexPaths = [IndexPath]()
        for row in menuSectionItemsArray[section].names.indices {
            let indexPath = IndexPath(row: row, section: section)
            indexPaths.append(indexPath)
        }
        
        let isExpanded = menuSectionItemsArray[section].isExpanded
        menuSectionItemsArray[section].isExpanded = !isExpanded
        
        button.setTitle(isExpanded ? "Open" : "Close", for: .normal)
        
        if isExpanded {
            tableView.deleteRows(at: indexPaths, with: .fade)
        } else {
            tableView.insertRows(at: indexPaths, with: .fade)
        }
        
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if isMenuOpen{isMenuOpen=false}
        
        if segue.identifier == detailSegueId{

            let detailView = segue.destination as! DetailsTestViewController
            detailView.delegate = self
            detailView.getitemsOrdered = itemsOrdered
            
//            if let menuItems = getRestaurantMenu{
//
//                detailView.getSelectedItem = menuItems[selectedRow]
//
//            }
            if RestaurantMenu != nil{
                detailView.getSelectedItem = RestaurantMenu[selectedRow]
            }

            
        }else if segue.identifier == cartSegueId{
            let cartVC = segue.destination as! CartViewController
            
            cartVC.getOrders = itemsOrdered
            
        }
    }
    
    @IBAction func menuButton(_ sender: Any) {
        
        
        if let window = UIApplication.shared.keyWindow{
            
            print(isMenuOpen)
            if !isMenuOpen{
                
                
                menuView.isHidden = false
                menuView.frame = CGRect(x: -window.frame.width*0.6, y: 0, width: window.frame.width*0.6, height: window.frame.height)
//                gradientView.frame = CGRect(x: -window.frame.width*0.6, y: 0, width: window.frame.width*0.6, height: window.frame.height)
                blackView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
                
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                
                    
                    self.tableView.frame = CGRect(x: window.frame.width*0.6, y: 0, width: self.tableView.frame.width, height: self.tableView.frame.height)
//                    self.gradientView.frame = CGRect(x: 0, y: 0, width: window.frame.width*0.6 , height: window.frame.height)
                    self.menuView.frame = CGRect(x: 0, y: 0, width: window.frame.width*0.6 , height: window.frame.height)
                    self.menuView.layer.insertSublayer(self.setGradientBackground(), at: 0)
                    
//                    self.gradientView.clipsToBounds = true
                    self.blackView.alpha = 1
                    self.blackView.frame = CGRect(x: self.menuView.frame.width, y: 0, width: window.frame.width-self.menuView.frame.width, height: window.frame.height)
                    
                }, completion: {(completion)in
                    
                    
                    self.isMenuOpen = true
                })
                
            }else{
                
                self.dismissMenuAnimated()
            }
        }
        
        
        
        
    }
    func dismissMenuAnimated(){
        
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            if let window = UIApplication.shared.keyWindow{
                
                self.blackView.alpha = 0
                
                self.menuView.frame = CGRect(x: -self.menuView.frame.width, y: 0, width: self.menuView.frame.width, height: self.menuView.frame.height)
                
                
                self.tableView.frame = CGRect(x: 0, y: 0, width: self.tableView.frame.width, height: self.tableView.frame.height)
                
                self.blackView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
                
                
                //                self.resetPwdView.frame = CGRect(x: 0 , y: window.frame.height, width: window.frame.width, height: window.frame.height/2)
                
            }
        }, completion: {(completion) in
            
            self.isMenuOpen = false
        })
        
    }
    @objc func dismissMenu(){
        dismissMenuAnimated()
    }
    
    @IBAction func requestWaiter(_ sender: Any) {


        
        displayAlert(message: "The request for a waiter has been sent to the restaurant!", waiterOrManager: "waiter")
    }
    
    @IBAction func requestManager(_ sender: Any) {
        
        displayAlert(message: "The request for a manager has been sent to the restaurant!", waiterOrManager: "manager")
    }
    
    func displayAlert(message: String,waiterOrManager: String){
        
        
        var resturantUid = ""
        var currentTable = ""
        if let restUid = UserDefaults.standard.string(forKey: restaurantUidKey), let tableNumber = UserDefaults.standard.string(forKey: restaurantTableNumber){
            resturantUid = restUid
            currentTable = tableNumber
        }
        
        guard let userUid = Auth.auth().currentUser?.uid else{return}
        
        let restaurantRef = Database.database().reference().child("Restaurants").child(resturantUid).child("Tables").child("Table\(currentTable)")
        
        
        if waiterOrManager == "waiter"
        {
            restaurantRef.updateChildValues(["WaiterRequest":true])
        }else if waiterOrManager == "manager"{
            restaurantRef.updateChildValues(["managerRequest":true])
//            restaurantRef.child("updated").removeValue()
        }
        
        
        dismissMenuAnimated()
        
        
        let alert = UIAlertController(
            title: "Staff Request",
            message: String (format:message),
            preferredStyle: .alert
        )
        alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
 
    func setGradientBackground() -> CAGradientLayer {
        let colorTop =  UIColor(red: 182/255.0, green: 77/255.0, blue: 67/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 243/255.0, green: 198/255.0, blue: 143/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.menuView.bounds
        
        return gradientLayer
    }
    
    @IBAction func goBack(_ sender: Any) {
     
        dismissMenu()
        self.dismiss(animated: true, completion: nil)
    }
}
