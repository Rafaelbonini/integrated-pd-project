//
//  DetailsTestViewController.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/13/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import SwiftMessages


protocol DataEnteredDelegate: class {
    func orderList(items: [RestaurantItemsTestModel])
}

class DetailsTestViewController: UIViewController,UIPickerViewDelegate,UIPickerViewDataSource {
    
    weak var delegate: DataEnteredDelegate? = nil

    @IBOutlet weak var optionsView: UIView!
    @IBOutlet weak var navigationItemForTittle: UINavigationItem!
    @IBOutlet weak var quantityButton: UIButton!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UILabel!
    @IBOutlet weak var ItemDesc: UILabel!
    @IBOutlet weak var itemPrice: UILabel!
    var quantity: Int = 1
    
    var typePickerView: UIPickerView = UIPickerView()
    
    let quantityArray = [1,2,3,4,5,6,7,8]
    
    
    var getitemsOrdered : [RestaurantItemsTestModel]?
    var getSelectedItem : MenuModel?
    
    let blackView : UIView = {
        let bView = UIView(frame: .zero)
        
        bView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        return bView
    }()
    var quantityView : UIView = {
        let uview = UIView(frame: .zero)
        uview.backgroundColor = UIColor(displayP3Red: 185/255, green: 244/255, blue: 182/255, alpha: 1.0)
        return uview
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
//        print(getSelectedItem?.dishName)
        
        optionsView.layer.borderWidth = 1
        optionsView.layer.borderColor = UIColor.black.cgColor
        
        if let foodItem = getSelectedItem{
            
            if let image = foodItem.image{
                print("saporra nao ta nil")
            }

            self.navigationItemForTittle.title = foodItem.dishCategory
//            self.navigationItemForTittle.
            self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
            itemImage.image = foodItem.image
            itemName.text = foodItem.dishName
            ItemDesc.text = foodItem.dishDesc
            itemPrice.text = "$ \(foodItem.dishPrice)"
        }

        
        self.typePickerView.dataSource = self
        self.typePickerView.delegate = self
        
    }
    
    
    @IBAction func Quantity(_ sender: Any) {
        
        self.typePickerView.selectRow(quantity-1, inComponent: 0, animated: true)
        
        
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissPicker)))
        
        if let window = UIApplication.shared.keyWindow{
            
            let tittleLabel : UILabel = {
                let tittle = UILabel(frame: .zero)
                tittle.textColor = UIColor.white
                tittle.font = UIFont(name: "Avenir-Light" , size: 25)
                tittle.text = "Select the Quantity"
                tittle.textAlignment = .center
                
                return tittle
            }()
            
        
            window.addSubview(blackView)
            window.addSubview(quantityView)
            
            quantityView.layer.addSublayer(setGradientBackground())
            quantityView.addSubview(tittleLabel)
            quantityView.addSubview(typePickerView)
            
            quantityView.translatesAutoresizingMaskIntoConstraints = false
            
            
            quantityView.frame = CGRect(x: 0, y:window.frame.height, width: window.frame.width, height: window.frame.height/2)
            
            
            self.typePickerView.frame = CGRect(x: (quantityView.frame.height/2)-100, y:(quantityView.frame.width/2)-100 , width: 200, height: 200)
            

            
            blackView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            tittleLabel.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: 90)

            
            UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                self.blackView.alpha = 1;
                self.quantityView.frame = CGRect(x: 0, y:window.frame.height/2, width: window.frame.width, height: window.frame.height/2)
            }, completion: nil)
            
            
        }
        
        
    }
    
    
    @objc func handleQtyChange(){
        
        
        
    }
    
    
    @objc func dismissPicker(){
        
        
//        self.quantityButton.titleLabel?.text = String(self.quantity)
        
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            if let window = UIApplication.shared.keyWindow{
                
                self.blackView.alpha = 0
                self.quantityView.frame = CGRect(x: 0 , y: window.frame.height, width: window.frame.width, height: window.frame.height/2)
                
            }
        }, completion: {(completion)in
            
            
            self.quantityButton.titleLabel?.text = String(self.quantity)
        })
        
        
        
    }
    
    
    @IBAction func OrderItem(_ sender: Any) {
        
        
        
//        let item = RestaurantItemsTestModel(image: self.getImage!, name: self.getName!, desc: self.getDesc!, portionSize: "", servWithFood: false, price: self.getPrice!, quantity: quantity)
        var dishPrice = 0.0
        
        if let itemPrice = getSelectedItem?.dishPrice{
            dishPrice = Double(itemPrice)!
            
        }
        

        let item = RestaurantItemsTestModel(image: UIImage(), name: (getSelectedItem?.dishName)!, desc: (getSelectedItem?.dishDesc)!, portionSize: (getSelectedItem?.dishPortion)!, servWithFood: false, price: dishPrice, quantity: quantity)
        
        if let items = getitemsOrdered{
            var duplicate : Bool = false
            for (i,num) in items.enumerated().reversed() {
                
                //check for duplicate
                //if it is a duplicate just add the new order quantity to the current order, if not add item to order list
                if items[i].name == item.name{
                    let quantityToAdd = items[i].quantity
                    
                    var newQuantityItem = RestaurantItemsTestModel(image: items[i].image, name: items[i].name, desc: items[i].desc, portionSize: items[i].portionSize, servWithFood: items[i].servWithFood, price: items[i].price, quantity: quantityToAdd+quantity)
                    getitemsOrdered?.remove(at: i)
                    getitemsOrdered?.append(newQuantityItem)
                    
                    duplicate = true
                }
                
            }
            if !duplicate{
                getitemsOrdered?.append(item)
            }

        }

        
        delegate?.orderList(items: getitemsOrdered!)
        
        self.setMessage(body: "Item added to the order")

        self.navigationController?.popToRootViewController(animated: true)
//        self.dismiss(animated: true, completion: nil)
    }
    
    func setGradientBackground() -> CAGradientLayer {
        let colorTop =  UIColor(red: 182/255.0, green: 77/255.0, blue: 67/255.0, alpha: 1.0).cgColor
        let colorBottom = UIColor(red: 243/255.0, green: 198/255.0, blue: 143/255.0, alpha: 1.0).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = self.view.bounds
        
        return gradientLayer
    }
    
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return quantityArray.count
    }
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return String(quantityArray[row])
    }
    
 
    
    
    //Mark: Fix picker view allways starting from 1 and changing to 1 when value not changed
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        quantity = quantityArray[row]
//        quantityButton.titleLabel?.text = String(quantityArray[row])
        
    }

    func setMessage(body:String){
        
        let view = MessageView.viewFromNib(layout: .centeredView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.success)
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        view.configureContent(title: "Order", body: body)
        
        view.button?.isHidden = true
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
        
    }
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        let attributedString = NSAttributedString(string: String(quantityArray[row]), attributes: [NSAttributedString.Key.foregroundColor : UIColor.white])
        
        //,NSAttributedString.Key.font : UIFont(name: "Avenir Book", size: 30.0)!
        return attributedString
    }
}
