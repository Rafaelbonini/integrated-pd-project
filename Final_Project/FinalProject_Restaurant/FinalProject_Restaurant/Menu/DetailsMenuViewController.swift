//
//  DetailsMenuViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/29/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
import JGProgressHUD

class DetailsMenuViewController: UIViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate,UITextViewDelegate  {

    let hud = JGProgressHUD(style: .dark)
    
    
    @IBOutlet weak var navigationItemTittle: UINavigationItem!
    @IBOutlet weak var deleteButton: UIButton!
    @IBOutlet weak var itemImage: UIImageView!
    @IBOutlet weak var itemName: UITextField!
//    @IBOutlet weak var itemDesc: UITextView!
    @IBOutlet weak var itemPrice: UITextField!
    @IBOutlet weak var itemPortiosize: UITextField!
    @IBOutlet weak var itemDescTField: UITextField!
    
    var itemIndex = -1
    var getRestaurantMenu : MenuModel?
    var getCategory : String?
    var getNumOfMenuItems : Int?
    
    var imagePicker = UIImagePickerController()
    
    
    let blackView : UIView = {
        let bView = UIView(frame: .zero)
        
        bView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        return bView
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(imageTapped(tapGestureRecognizer:)))
        itemImage.isUserInteractionEnabled = true
        itemImage.addGestureRecognizer(tapGestureRecognizer)
        
        itemImage.addSubview(blackView)
        blackView.frame = CGRect(x: 0, y: itemImage.frame.height*0.8, width: itemImage.frame.width, height: itemImage.frame.height*0.2)
        
        let label = UILabel(frame: CGRect(x: 0, y: blackView.frame.minX, width: blackView.frame.width , height: blackView.frame.height))
        label.textAlignment = .center
        label.text = "Change image"
        label.textColor = UIColor.white
        label.font = UIFont(name: "Avenir Black", size: 17)
        blackView.addSubview(label)
        
        
        

        
        if let restaurantMenu = getRestaurantMenu{

            
            self.navigationItemTittle.title = restaurantMenu.category
            itemImage.image = restaurantMenu.image
            itemName.text = restaurantMenu.dishName
            itemDescTField.text = restaurantMenu.desc
            itemPrice.text = String(restaurantMenu.dishPrice)
            itemIndex = restaurantMenu.index
            
        }
        
        
        if let category = getCategory{
            
            self.deleteButton.isHidden = true
            
            self.title = category
            
        }
        
    }
    
    
    @objc func imageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let tappedImage = tapGestureRecognizer.view as! UIImageView
        
        self.imagePicker.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                
                self.imagePicker.sourceType = .photoLibrary;
                self.imagePicker.allowsEditing = true
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        itemImage.image = image
        
        self.dismiss(animated: true, completion: nil)
    }
    

    
    @IBAction func saveChanges(_ sender: Any) {
        
        hud.textLabel.text = "Saving"
        hud.show(in: self.view)
        
        guard let restaurantUid = Auth.auth().currentUser?.uid else{
            return
        }
        
//        let itemIndex = getRestaurantMenu!.index
        
        
        //if true means that it is a new item and not an update item
        if getRestaurantMenu == nil{
            if let name = self.itemName.text, let desc = self.itemDescTField.text,let price = self.itemPrice.text,let portion = self.itemPortiosize.text,let category = getCategory{
                
                
                var newItemIndex = -1
            
                let ref = Database.database().reference().child("Restaurants").child(restaurantUid).child("Menu")
                
                ref.observeSingleEvent(of: .value) { (snapshot) in
                    let value = snapshot.value as? [String:Any]
        
                    
                    if value == nil {
                        
                        ref.setValue(["numOfDishes":1])
                        newItemIndex = 1
                    }else{
                        newItemIndex = value?["numOfDishes"] as? Int ?? -1
                        
                        newItemIndex += 1
                    }
                    
                    
//                    print("num of tables : \(numOfTables)")
                    
                    
                    
                    //save image to storage to menu item folder, with name (restaurant uid + item index)
                    if let image = self.itemImage.image?.resize().jpegData(compressionQuality: 0.1){
                        
                        let storageRefere = Storage.storage().reference().child("MenuItem/\(restaurantUid)\(newItemIndex).jpg")
                        
                        storageRefere.putData(image, metadata: nil){ (metadata, error) in
                            
                            if error != nil{
                                print(error!)
                                
                            }else{
                                
                                
                                    var itemDictionaty : [String:Any] = [:]
                                    
                                    itemDictionaty["dishCategory"] = category
                                    itemDictionaty["dishName"] = name
                                    itemDictionaty["dishDesc"] = desc
                                    itemDictionaty["dishPortion"] = portion
                                    itemDictionaty["dishPrice"] = price
                                    itemDictionaty["dishHasImage"] = true
                                    
                                    
                                    let databaseRef = Database.database().reference().child("Restaurants").child(restaurantUid).child("Menu")
                                
                                databaseRef.updateChildValues(["numOfDishes":newItemIndex])
                                
                                    databaseRef.child(String(newItemIndex)).updateChildValues(itemDictionaty) { (error, ref) in
                                     
                                        
                                        
                                    }
                                
                                
                                UserDefaults.standard.setValue("Changed", forKey: editedKey)
                                self.navigationController?.popViewController(animated: true)
                                print("Image uploaded to Storage!")
                                self.hud.dismiss(animated: true)
                            }
                            
                        }
                    }
                    
                    
                    if let name = self.itemName.text, let desc = self.itemDescTField.text,let price = self.itemPrice.text,let portion = self.itemPortiosize.text, let category = self.getCategory{
                        
                        
                        var itemDictionaty : [String:Any] = [:]
                        
                        itemDictionaty["dishCategory"] = category
                        itemDictionaty["dishName"] = name
                        itemDictionaty["dishDesc"] = desc
                        itemDictionaty["dishPortion"] = portion
                        itemDictionaty["dishPrice"] = price
                        
                        
                        
                        
                        let databaseRef = Database.database().reference().child("Restaurants").child(restaurantUid).child("Menu")
                        
                        databaseRef.updateChildValues(["numOfDishes":newItemIndex])
                        
                        databaseRef.child(String(newItemIndex)).updateChildValues(itemDictionaty) { (error, ref) in
                            
                            if error != nil{
                                
                            }else{
                                
                                UserDefaults.standard.setValue("Changed", forKey: editedKey)
                                self.navigationController?.popViewController(animated: true)
                                
                            }
                            
                        }
                        
                    }
                    
                }
   
            }
            
        }else{
            
            
            //save image to storage to menu item folder, with name (restaurant uid + item index)
            if let image = itemImage.image?.resize().jpegData(compressionQuality: 0.1){
                
                let storageRefere = Storage.storage().reference().child("MenuItem/\(restaurantUid)\(itemIndex).jpg")
                
                storageRefere.putData(image, metadata: nil){ (metadata, error) in
                    
                    if error != nil{
                        print(error!)
                        
                    }else{
                        
                        
                        if let name = self.itemName.text, let desc = self.itemDescTField.text,let price = self.itemPrice.text,let portion = self.itemPortiosize.text, let category = self.getRestaurantMenu?.category{
                            
                            
                            var itemDictionaty : [String:Any] = [:]
                            
                            itemDictionaty["dishCategory"] = category
                            itemDictionaty["dishName"] = name
                            itemDictionaty["dishDesc"] = desc
                            itemDictionaty["dishPortion"] = portion
                            itemDictionaty["dishPrice"] = price
                            itemDictionaty["dishHasImage"] = true
                            
                            
                            let databaseRef = Database.database().reference()
                            
                            databaseRef.child("Restaurants").child(restaurantUid).child("Menu").child(String(self.itemIndex)).updateChildValues(itemDictionaty) { (error, ref) in
                                
                            }
                        }
                        
                        UserDefaults.standard.setValue("Changed", forKey: editedKey)
                        self.navigationController?.popViewController(animated: true)
                        print("Image uploaded to Storage!")
                        
                    }
                    
                }
            }
            
            
            if let name = self.itemName.text, let desc = self.itemDescTField.text,let price = self.itemPrice.text,let portion = self.itemPortiosize.text, let category = self.getRestaurantMenu?.category{
                
                
                var itemDictionaty : [String:Any] = [:]
                
                itemDictionaty["dishCategory"] = category
                itemDictionaty["dishName"] = name
                itemDictionaty["dishDesc"] = desc
                itemDictionaty["dishPortion"] = portion
                itemDictionaty["dishPrice"] = price
                
                
                
                let databaseRef = Database.database().reference()
                
                databaseRef.child("Restaurants").child(restaurantUid).child("Menu").child(String(itemIndex)).updateChildValues(itemDictionaty) { (error, ref) in
                    
                    if error != nil{
                        
                    }else{
                        
                        UserDefaults.standard.setValue("Changed", forKey: editedKey)
                        self.navigationController?.popViewController(animated: true)
                        
                    }
                    
                }
                
            }
            
        }
        
    }
    @IBAction func deleteItem(_ sender: Any) {

        hud.textLabel.text = "Removing from Menu"
        hud.show(in: self.view)
        
        guard let restaurantUid = Auth.auth().currentUser?.uid else{
            return
        }
        
        
        if let itemCount = getNumOfMenuItems{
            
            let newItemCount = itemCount - 1
            
            let ref = Database.database().reference().child("Restaurants").child(restaurantUid).child("Menu")
            
            ref.child(String(itemIndex)).removeValue { (error, reference) in
                
                if let error = error{
                    print(error)
                }else{
                    
                    
                    ref.updateChildValues(["numOfDishes":newItemCount])
                    
                    let storageRef = Storage.storage().reference().child("MenuItem/\(restaurantUid)\(self.itemIndex).jpg")
                    
                    storageRef.delete(completion: { (error) in
                        
                        if let error = error{
                            print(error)
                        }else{
                            print("deleted successfully")
                            
                        }
                    })
                    
                    self.hud.dismiss(animated: true)
                    UserDefaults.standard.setValue("Changed", forKey: editedKey)
                    self.navigationController?.popViewController(animated: true)
                }
            }
        }
    }
}

private extension UIImage{
    
    func  resize() -> UIImage {
        let height: CGFloat = 1000.0
        let ratio = self.size.width / self.size.height
        let width = height * ratio
        
        let newSize = CGSize(width: width, height: height)
        let newRectangle = CGRect(x: 0, y: 0, width: width, height: height)
        
        UIGraphicsBeginImageContext(newSize)
        
        self.draw(in: newRectangle)
        
        let resizedImage = UIGraphicsGetImageFromCurrentImageContext()
        
        UIGraphicsEndImageContext()
        
        return resizedImage!
    }
    
}
