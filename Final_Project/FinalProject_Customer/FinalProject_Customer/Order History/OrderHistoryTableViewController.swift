//
//  OrderHistoryTableViewController.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/24/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
import JGProgressHUD
import SwiftMessages

private let cellId = "orderCell"
private let detailSegueId = "DetailSegueId"
class OrderHistoryTableViewController: UITableViewController {
    
    let hud = JGProgressHUD(frame: CGRect(x:0 , y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height))
    
    
    var orderHistoryItems : [OrderHistory] = []
    var selectedItemIndex = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        hud.textLabel.text = "Loading Orders"
        hud.show(in: self.view)

        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.barTintColor  = UIColor(displayP3Red: 182/255, green: 77/255, blue: 67/255, alpha: 1.0)
        
        
        
        guard let currentUserUid = Auth.auth().currentUser?.uid else{
            return
        }
        
        //check if there is any record to retrieve
        let ref = Database.database().reference().child("Users").child(currentUserUid)
        
        ref.observe(.value) { (dataSnapshot) in
            
            if !dataSnapshot.hasChild("orderHistory"){
                self.setMessage(body: "No records to be shown")
                self.hud.dismiss(animated: true)
            }else{
                
                
                //get all orders uid saved to the user
                ref.child("orderHistory").observe(.childAdded, with: { (snapshot) in
                    
                    
                    
                    let orderId = snapshot.key
                    let orderReference = Database.database().reference().child("Orders").child(orderId)
                    
                    //use orders uid to get each order information
                    orderReference.observeSingleEvent(of: .value, with: { (snapshot) in
                        //                print(snapshot)
                        if let value = snapshot.value as? [String:Any]{
                            
                            if let customerUid = value["customerUid"] as? String, let restaurantName = value["restaurantName"] as? String, let restaurantUid = value["restaurantUid"] as? String, let tableNum = value["tableNum"] as? String, let timeStamp = value["timeStamp"] as? Double,let orderCost = value["orderTotalPrice"] as? Double, let orderDic = value["order"] as? [String:Any] {
                                
                                var orderItems : [ItemFromOrder] = []
                                for items in orderDic{
                                    
                                    if let item = orderDic[items.key] as? [String:Any]{
                                        
                                        if let dishName = item["dishName"] as? String, let dishPrice = item["dishPrice"] as? Double, let dishQuantity = item["dishQuantity"] as? Int, let servWFood = item["servWFood"] as? Bool{
                                            
                                            
                                            orderItems.append(ItemFromOrder(dishName: dishName, dishPrice: dishPrice, dishQuantity: dishQuantity, servWFood: servWFood))
                                            
                                            print("\(items.key)=  \(dishName):\(dishPrice):\(dishQuantity):\(servWFood)")
                                        }
                                    }
                                }
                                
                                self.orderHistoryItems.append(OrderHistory(restaurantName: restaurantName, restaurantUid: restaurantUid, tableNum: tableNum, timeStamp: timeStamp, order: orderItems, totalOrderCost: orderCost))
                                
                                self.orderHistoryItems = self.orderHistoryItems.sorted(by: { $0.timeStamp > $1.timeStamp })
                                print("\(customerUid):\(restaurantName):\(restaurantUid):\(tableNum):\(timeStamp)")
                                
                                self.tableView.reloadData()
                                self.hud.dismiss(animated: true)
                            }
                        }
                    }, withCancel: nil)
                    
                }, withCancel:nil)
                
                
            }
        }
        
        
       
        
        
    }
    
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return orderHistoryItems.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! OrderHistoryTableViewCell
        
        
        cell.restaurantInfo = orderHistoryItems[indexPath.row]
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        selectedItemIndex = indexPath.row
        performSegue(withIdentifier: detailSegueId, sender: nil)
        
        
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == detailSegueId{
            
            let detailVC = segue.destination as! DetailOrderHistoryViewController
            
            detailVC.getSelectedOrder = orderHistoryItems[selectedItemIndex]
            
        }
    }
    
    func setMessage(body:String){
        
        let view = MessageView.viewFromNib(layout: .centeredView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.warning)
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        view.configureContent(title: "Order History", body: body)
        
        view.button?.isHidden = true
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
        
    }
}
