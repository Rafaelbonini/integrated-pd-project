//
//  VerifyUserViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 2/2/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
import JGProgressHUD

private let changeItemSegue = "goToChangeVerifiedItem"
class VerifyUserViewController: UIViewController {

    let hud = JGProgressHUD(style: .dark)
    
    @IBOutlet weak var passwordTextField: UITextField!
    
    var getLabelTittle :String?
    var getValue : String?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        passwordTextField.setLineAndColorTV(color: UIColor.black)
        
        

        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        
        //small padding adjustment
        passwordTextField.setLeftPaddingPoints(1)
        
        //black line no bottom of text field
        let bottomLine = CALayer()
        bottomLine.frame = CGRect.init(x: 0, y: passwordTextField.frame.size.height - 1, width: passwordTextField.frame.size.width, height: 2)
        bottomLine.backgroundColor = UIColor.black.cgColor
        passwordTextField.borderStyle = UITextField.BorderStyle.none
        passwordTextField.layer.addSublayer(bottomLine)
    }
    
    @IBAction func verifyPassword(_ sender: Any) {
        
        
        
        
        let user = Auth.auth().currentUser
        let email = user?.email
        
        if let password = passwordTextField.text{
            
            if password.count > 5{
                hud.textLabel.text = "Verifying"
                hud.show(in: self.view)
                
                let credential = EmailAuthProvider.credential(withEmail: email!, password: password)
                
                // Prompt the user to re-provide their sign-in credentials
                
                
                user?.reauthenticateAndRetrieveData(with: credential, completion: { (DataResult, error) in
                    
                    if error != nil {
                        // An error happened.
                        self.hud.dismiss(animated: true)
                        print("user has failed to be reauthenticated")
                        self.passwordTextField.setLineAndColorTV(color: UIColor.red)
                    } else {
                        // User re-authenticated.
                        self.hud.dismiss(animated: true)
                        print("user has been reauthenticated")
                        self.performSegue(withIdentifier: changeItemSegue, sender: nil)
                        
                    }
                })
            }else{
                //input has less than 6 letters
                self.passwordTextField.setLineAndColorTV(color: UIColor.red)
                
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == changeItemSegue{
            
            let editProfile = segue.destination as! UpdateInfoProfileViewController
            
            editProfile.getLabelTittle = getLabelTittle
            editProfile.getValue = getValue
            
            if getLabelTittle == "Password"{
                editProfile.getSecureEntry = true
                editProfile.getValue = ""
            }
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }

    @IBAction func goBack(_ sender: Any) {
        self.navigationController?.popViewController(animated: true)
    }
}
