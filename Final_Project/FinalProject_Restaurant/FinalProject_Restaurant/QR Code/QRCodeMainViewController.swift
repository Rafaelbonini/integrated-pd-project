//
//  QRCodeMainViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/15/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import QRCodeReader
import JGProgressHUD

private let createCodesSegue = "createCodesSegue"
class QRCodeMainViewController: UIViewController,QRCodeReaderViewControllerDelegate {
    
    
    let hud = JGProgressHUD(style: .dark)
    
    lazy var reader: QRCodeReader = QRCodeReader()
    lazy var readerVC: QRCodeReaderViewController = {
        let builder = QRCodeReaderViewControllerBuilder {
            $0.reader                  = QRCodeReader(metadataObjectTypes: [.qr], captureDevicePosition: .back)
            $0.showTorchButton         = true
            $0.preferredStatusBarStyle = .lightContent
            
            
            $0.reader.stopScanningWhenCodeIsFound = false
        }
        
        return QRCodeReaderViewController(builder: builder)
    }()
    
    var numOfTable = 0
    override func viewDidLoad() {
        super.viewDidLoad()
        hud.dismiss(animated: false)
        
        
        numOfTable = UserDefaults.standard.integer(forKey: numOfTableKey)
        
        // Do any additional setup after loading the view.
    }
    
    
    private func checkScanPermissions() -> Bool {
        do {
            return try QRCodeReader.supportsMetadataObjectTypes()
        } catch let error as NSError {
            let alert: UIAlertController
            
            switch error.code {
            case -11852:
                alert = UIAlertController(title: "Error", message: "This app is not authorized to use Back Camera.", preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "Setting", style: .default, handler: { (_) in
                    DispatchQueue.main.async {
                        if let settingsURL = URL(string: UIApplication.openSettingsURLString) {
                            
                            UIApplication.shared.open(settingsURL, options: [:], completionHandler: nil)
                        }
                    }
                }))
                
                alert.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
            default:
                alert = UIAlertController(title: "Error", message: "Reader not supported by the current device", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            }
            
            present(alert, animated: true, completion: nil)
            
            return false
        }
    }
    
    
    func reader(_ reader: QRCodeReaderViewController, didScanResult result: QRCodeReaderResult) {
        
        dismiss(animated: true) { [weak self] in
            let alert = UIAlertController(
                title: "QRCodeReader",
                //                message: String (format:"%@ (of type %@)", result.value, result.metadataType),
                message: String (format:"QR Code content: %@", result.value),
                preferredStyle: .alert
            )
            alert.addAction(UIAlertAction(title: "OK", style: .cancel, handler: nil))
            
            self?.present(alert, animated: true, completion: nil)
        }
    }
    
    func readerDidCancel(_ reader: QRCodeReaderViewController) {
        reader.stopScanning()
        
        dismiss(animated: true, completion: nil)
    }
    

    @IBAction func scanCode(_ sender: Any) {
        
        guard checkScanPermissions() else { return }
        
        readerVC.modalPresentationStyle = .formSheet
        readerVC.delegate               = self
        
        readerVC.completionBlock = { (result: QRCodeReaderResult?) in
            if let result = result {
                print("Completion with result: \(result.value) of type \(result.metadataType)")
            }
        }
        
        present(readerVC, animated: true, completion: nil)

    }
    
    @IBAction func generateCodes(_ sender: Any) {
        
        self.performSegue(withIdentifier: createCodesSegue, sender: nil)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
//        hud.textLabel.text = "Generating QR Codes"
//        hud.show(in: self.view)
        if segue.identifier == createCodesSegue{
            let qrCodeVC = segue.destination as! TablesQRCodeTableViewController
            
            qrCodeVC.getTableNum = numOfTable
//
//            hud.dismiss(animated: true)
        }
    }
}
