//
//  CartTableViewCell.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/14/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

class CartTableViewCell: UITableViewCell {

    @IBOutlet weak var itemNameQuantity: UILabel!
    @IBOutlet weak var itemPriceSum: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
