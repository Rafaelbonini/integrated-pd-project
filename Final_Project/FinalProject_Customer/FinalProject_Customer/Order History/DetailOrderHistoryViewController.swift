//
//  DetailOrderHistoryViewController.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/25/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

private let cellId = "historyCellId"
private let placeSegueId = "placeOrderId"
public let reorderKey = "reorderitem"
class DetailOrderHistoryViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    
    @IBOutlet weak var reorderButton: UIButton!
    
    var getSelectedOrder : OrderHistory?
    var order : [ItemFromOrder] = []
    
    @IBOutlet weak var totalCostLabel: UILabel!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var orderDateLabel: UILabel!
    @IBOutlet weak var tableNumberLabel: UILabel!
    @IBOutlet weak var restaurantNameLabel: UILabel!
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        let orderstatus = UserDefaults.standard.string(forKey: orderStatusKey)
        if orderstatus != ""
        {
            UserDefaults.standard.setValue("", forKey: reorderKey)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        tableView.dataSource = self
        tableView.delegate = self
        
        if let selectedOrder = getSelectedOrder{
            
            
            
            let scannedRestaurant = UserDefaults.standard.string(forKey: restaurantUidKey)
            
            if scannedRestaurant != selectedOrder.restaurantUid{
                
                reorderButton.isHidden = true
            }else {
                reorderButton.isHidden = false
            }
            
            
            
            let date = NSDate(timeIntervalSince1970: selectedOrder.timeStamp)
            
            
            let dayTimePeriodFormatter = DateFormatter()
            
            dayTimePeriodFormatter.dateFormat = "MMM/dd/YYYY hh:mm"
            
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            
            
            restaurantNameLabel.text = "Restaurant: \(selectedOrder.restaurantName)"
            tableNumberLabel.text = "Table: \(selectedOrder.tableNum)"
            orderDateLabel.text = "Date: \(dateString)"
            totalCostLabel.text = "\(selectedOrder.totalOrderCost)"
            
            order = selectedOrder.order
            
            
            tableView.reloadData()
        }
        
        print(getSelectedOrder!)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return order.count
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //
        
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! OrderTableViewCell
        
//        let nameAndQuantity = "\(order[indexPath.row].dishName) x \(order[indexPath.row].dishQuantity)"
        let sumPrice = "$\(order[indexPath.row].dishPrice * Double(order[indexPath.row].dishQuantity))"
        
        var nameAndQuantity = "\(order[indexPath.row].dishName)"
        nameAndQuantity = nameAndQuantity.truncate(length: 24, trailing: "...")
        let quantity = " x \(order[indexPath.row].dishQuantity)"
        nameAndQuantity += quantity
        
        cell.itemName.text = nameAndQuantity
        cell.itemPrice.text = sumPrice
        
        return cell
    }

    
    @IBAction func reorderItem(_ sender: Any) {
        UserDefaults.standard.setValue("reorder", forKey: reorderKey)
        self.performSegue(withIdentifier: placeSegueId, sender: nil)
    }
    

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == placeSegueId{
            
            let placeOrder = segue.destination as! CartViewController
            placeOrder.getReorder = order
        }
    }
}
