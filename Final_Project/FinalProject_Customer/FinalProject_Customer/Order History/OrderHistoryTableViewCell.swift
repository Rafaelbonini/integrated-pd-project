//
//  OrderHistoryTableViewCell.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 1/24/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

class OrderHistoryTableViewCell: UITableViewCell {

    @IBOutlet weak var restaurantName: UILabel!
    @IBOutlet weak var orderDate: UILabel!
    @IBOutlet weak var orderPrice: UILabel!
    
    
    var restaurantInfo : OrderHistory?{
        
        didSet{
            
            

            let totalPrice = "$ \(restaurantInfo!.totalOrderCost.clean)"
            let date = NSDate(timeIntervalSince1970: restaurantInfo!.timeStamp)
            
            let dayTimePeriodFormatter = DateFormatter()
//            dayTimePeriodFormatter.dateFormat = "MMM dd YYYY hh:mm a"
            dayTimePeriodFormatter.dateFormat = "MMM/dd/YYYY"
            
            let dateString = dayTimePeriodFormatter.string(from: date as Date)
            
            restaurantName.text = restaurantInfo?.restaurantName
            orderPrice.text = totalPrice
            orderDate.text = dateString
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
