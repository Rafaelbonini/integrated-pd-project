//
//  TableCollectionViewCell.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/21/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

class TableCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var colorView: UIView!
    
    @IBOutlet weak var tableNumLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        colorView.backgroundColor = UIColor.gray
        colorView.layer.cornerRadius = colorView.frame.width/2
        colorView.clipsToBounds = true
    }
    

}
