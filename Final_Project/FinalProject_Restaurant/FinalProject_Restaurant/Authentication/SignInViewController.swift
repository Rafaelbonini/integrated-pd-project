//
//  SignInViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/13/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

import UIKit
import Firebase
import JGProgressHUD
import SwiftMessages

public let numOfTableKey = "restaurantNumberOfTables"
private let toMainScreenSegue = "GotoHome";
private let toMainScreenAnimatedSegue = "GotoHomeAnimated";
class SignInViewController: UIViewController,UITextFieldDelegate,UINavigationControllerDelegate {
    
    @IBOutlet weak var emailTextFieldYconstraint: NSLayoutConstraint!
    
    @IBOutlet weak var userEmail: UITextField!
    @IBOutlet weak var userPassword: UITextField!
    var added = false
    var logged : Bool = true
    let hud = JGProgressHUD(style: .dark)
    
    var resetPwdView : UIView = {
        let uview = UIView(frame: .zero)
        uview.backgroundColor = UIColor(displayP3Red: 1, green: 1, blue: 1, alpha: 0)
        return uview
    }()
    
    let blackView : UIView = {
        let bView = UIView(frame: .zero)
        
        bView.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.3)
        
        return bView
    }()
    
    let emailTextField : UITextField = {
        let txtView = UITextField(frame: .zero)
        txtView.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        txtView.textColor = UIColor(displayP3Red: 113/255, green: 122/255, blue: 122/255, alpha: 1)
        txtView.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor.init(red: 113/255, green: 122/255, blue: 122/255, alpha: 1)])
        return txtView
    }()
    
    let invalidEmail : UILabel = {
        let label = UILabel(frame: .zero)
        label.textColor = UIColor.red
        label.font = UIFont(name: "Avenir-Light" , size: 13)
        label.text = "Please enter a valid email."
        label.textAlignment = .center
        label.alpha = 0
        return label
    }()
    
    
    override func viewDidAppear(_ animated: Bool) {
        
        Auth.auth().addStateDidChangeListener { (auth, user) in
            if user != nil{
                
                if self.logged {
                    self.performSegue(withIdentifier: toMainScreenSegue, sender: nil)
                }else{
                    self.performSegue(withIdentifier: toMainScreenAnimatedSegue, sender: nil)
                }
                
            }
        }
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    @objc func keyboardWillAppear(_ notification: Notification) {
        
        
        let keyboardDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        let curve = notification.userInfo?[UIResponder.keyboardAnimationCurveUserInfoKey] as! NSNumber
        let keyboardFrame = (notification.userInfo?[UIResponder.keyboardFrameEndUserInfoKey] as AnyObject).cgRectValue
        
        UIView.animate(withDuration: keyboardDuration!, delay: 0.0, options: UIView.AnimationOptions(rawValue: UInt(curve)), animations: {
            self.resetPwdView.frame = CGRect(x: 0, y: self.view.frame.midY-(keyboardFrame?.height)!+10, width: self.resetPwdView.frame.width, height: self.resetPwdView.frame.height)
            
            if self.userPassword.isFirstResponder{
                
                //makes txt views move with keyboard appear
                self.emailTextFieldYconstraint.constant = (-(keyboardFrame?.height)!/4)+5
                self.view.layoutIfNeeded()
                
            }
        }, completion: nil)
        
        
    }
    
    @objc func keyboardWillDisappear(_ notification: Notification) {
        
        let keyboardDuration = (notification.userInfo?[UIResponder.keyboardAnimationDurationUserInfoKey] as AnyObject).doubleValue
        
        
        UIView.animate(withDuration: keyboardDuration!, animations: {
            self.resetPwdView.frame = CGRect(x: 0, y: self.view.frame.height/2+10, width: self.resetPwdView.frame.width, height: self.view.frame.height)
            
            
            self.emailTextFieldYconstraint.constant = -22
            self.view.layoutIfNeeded()
        })
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //        backgroundView.layer.addSublayer(setGradientBackground())
        //
        //        view.sendSubviewToBack(self.backgroundView)
        
        // Do any additional setup after loading the view.
        self.userEmail.delegate = self
        self.userPassword.delegate = self
        
        self.view.addGestureRecognizer(UITapGestureRecognizer(target: self.view, action: #selector(UIView.endEditing(_:))))
        resetPwdView.addGestureRecognizer(UITapGestureRecognizer(target: resetPwdView, action: #selector(UIView.endEditing(_:))))
        
        //        invalidInputLabel.alpha = 0;
        
        
        
        SetTextViews()
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        
        
        if textField == userEmail{
            
            textField.resignFirstResponder()
            userPassword.becomeFirstResponder()
        }else if textField == userPassword{
            textField.resignFirstResponder()
        }
        
        return true
        
    }
    
    //MARK: - LOG IN
    @IBAction func SignIn(_ sender: Any) {
        
        if let email = userEmail.text, let password = userPassword.text {
            
            //check if the user entered a valid email
            if email.isValidEmail(){
                hud.textLabel.text = "Signing In"
                hud.show(in: self.view)
                
                
                //remove invalid warning
                //                invalidInputLabel.alpha = 0
                userEmail.layer.borderWidth = 0
                
                //password is required to have at least 5 characters to be valid
                if password.count > 5{
                    
                    Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                        if error != nil{
                            
                            print("Failed to sign in!")
                            //                            self.invalidInputLabel.text = "Incorrect email or password"
                            //                            self.invalidInputLabel.alpha = 1
                            self.setMessage(body: "Incorrect email or password")
                            self.hud.dismiss(animated: true)
                            
                        }else{
                            
                            self.hud.dismiss(animated: true)
                            self.logged = false
                            print("Loged in sucessful")
                            
                        }
                    }
                }else{
                    hud.dismiss(animated: true)
                    // feedback on invalid password
                    
                    //                    invalidInputLabel.text = "A valid password must containt at least 6 characters."
                    //                    invalidInputLabel.alpha = 1
                    self.setMessage(body: "A valid password must containt at least 6 characters.")
                    userPassword.layer.borderWidth = 2
                    userPassword.layer.borderColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 0.5).cgColor
                }
                
                
            }else{
                //feedback on invalid email
                self.setMessage(body: "Invalid Email")
                //                invalidInputLabel.text = "Please enter a valid email."
                //                invalidInputLabel.alpha = 1
                userEmail.becomeFirstResponder()
                userEmail.layer.borderWidth = 2
                userEmail.layer.borderColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 0.5).cgColor
                
            }
        }
    }
    
    @IBAction func forgotPwd(_ sender: Any) {
        
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(dismissResetPwd)))
        
        if let window = UIApplication.shared.keyWindow{
            
            let tittleLabel : UILabel = {
                let tittle = UILabel(frame: .zero)
                tittle.textColor = UIColor(displayP3Red: 182/255, green: 77/255, blue: 67/255, alpha: 1)
                tittle.font = UIFont(name: "Avenir-Book" , size: 25)
                tittle.text = "Reset Password"
                tittle.textAlignment = .center
                
                return tittle
            }()
            
            let resetButton : UIButton = {
                let rbutton = UIButton(type: .system)
                rbutton.backgroundColor = UIColor(displayP3Red: 182/255, green: 77/255, blue: 67/255, alpha: 1)
                rbutton.setTitle("Reset", for: .normal)
                rbutton.setTitleColor(UIColor.white, for: .normal)
                rbutton.addTarget(self, action: #selector(handlePwdReset), for: .touchUpInside)
                rbutton.titleLabel?.textAlignment = .center
                
                return rbutton
            }()
            
            
            
            
            
            
            
            
            
            window.addSubview(blackView)
            window.addSubview(resetPwdView)
            
            
            resetPwdView.addSubview(tittleLabel)
            resetPwdView.addSubview(emailTextField)
            resetPwdView.addSubview(resetButton)
            resetPwdView.addSubview(invalidEmail)
            
            resetPwdView.translatesAutoresizingMaskIntoConstraints = false
            
            
            
            resetPwdView.frame = CGRect(x: 0, y:window.frame.height, width: window.frame.width, height: window.frame.height/2)
            
            let blurredBackgroundView = UIVisualEffectView()
            blurredBackgroundView.frame = CGRect(x: 0, y: 0, width: resetPwdView.layer.frame.width, height: resetPwdView.layer.frame.height)
            blurredBackgroundView.effect = UIBlurEffect(style: .regular)
            
            if !added{
                resetPwdView.addSubview(blurredBackgroundView)
                resetPwdView.sendSubviewToBack(blurredBackgroundView)
                added = true
            }
            
            
            //            blurredBackgroundView.frame = CGRect(x: 0, y:window.frame.height, width: window.frame.width, height: window.frame.height/2)
            
            emailTextField.frame = CGRect(x: 10, y: 10, width: resetPwdView.frame.width-30, height: 60)
            emailTextField.center = CGPoint(x: resetPwdView.frame.width/2, y: resetPwdView.frame.height/2)
            emailTextField.setLeftPaddingPoints(20)
            //            window.frame.width-100
            blackView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            tittleLabel.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: 90)
            resetButton.frame = CGRect(x: 0, y: resetPwdView.frame.height-65, width: window.frame.width, height: 65)
            invalidEmail.frame = CGRect(x: 0, y: resetPwdView.frame.height/2 + 15, width: window.frame.width, height: 50)
            
            UIView.animate(withDuration: 0.6, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseIn, animations: {
                self.blackView.alpha = 1;
                self.resetPwdView.frame = CGRect(x: 0, y:window.frame.height/2, width: window.frame.width, height: window.frame.height/2)
            }, completion: nil)
            
        }
        
    }
    
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    func SetTextViews(){
        //set text field background color
        userEmail.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        userPassword.backgroundColor = UIColor(displayP3Red: 1.0, green: 1.0, blue: 1.0, alpha: 0.7);
        
        //set textfield placeholder text color
        userEmail.attributedPlaceholder = NSAttributedString(string: "Email", attributes: [NSAttributedString.Key.foregroundColor: UIColor(displayP3Red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        userPassword.attributedPlaceholder = NSAttributedString(string: "Password", attributes: [NSAttributedString.Key.foregroundColor: UIColor(displayP3Red: 113/255, green: 112/255, blue: 112/255, alpha: 1)])
        
        //set textfield text padding
        userEmail.setLeftPaddingPoints(20)
        userPassword.setLeftPaddingPoints(20)
    }
    
    //MARK: - PASSWORD RESET
    @objc func handlePwdReset(){
        
        if let resetEmail = emailTextField.text{
            
            if resetEmail.isValidEmail(){
                
                emailTextField.layer.borderWidth = 0
                invalidEmail.alpha = 0
                
                Auth.auth().sendPasswordReset(withEmail: emailTextField.text!) { (error) in
                    if error != nil{
                        print("not sucessfull")
                        print(error!)
                        
                        self.invalidEmail.text = "Incorrect email"
                        self.invalidEmail.alpha = 1
                        self.emailTextField.layer.borderWidth = 2
                        self.emailTextField.layer.borderColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 0.5).cgColor
                        
                    }else{
                        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                            if let window = UIApplication.shared.keyWindow{
                                
                                self.blackView.alpha = 0
                                self.resetPwdView.frame = CGRect(x: 0 , y: window.frame.height, width: window.frame.width, height: window.frame.height/2)
                                
                            }
                        }, completion: { (completion) in
                            
                            self.blackView.removeFromSuperview()
                            self.resetPwdView.removeFromSuperview()
                            
                            
                            // create the alert
                            let alert = UIAlertController(title: "Reset Password", message: "An email has been sent to \(resetEmail) with a link to reset the password", preferredStyle: UIAlertController.Style.alert)
                            
                            // add an action (button)
                            alert.addAction(UIAlertAction(title: "OK", style: UIAlertAction.Style.default, handler: nil))
                            
                            // show the alert
                            self.present(alert, animated: true, completion: nil)
                            
                        })
                        
                    }
                }
                
                
            }else{
                
                self.setMessage(body: "Invalid Email")
                //                invalidEmail.text = "Please enter a valid email."
                //                invalidEmail.alpha = 1
                emailTextField.layer.borderWidth = 2
                emailTextField.layer.borderColor = UIColor(red: 255/255, green: 38/255, blue: 0/255, alpha: 0.5).cgColor
                
            }
        }
    }
    
    @objc func dismissResetPwd(){
        
        UIView.animate(withDuration: 0.8, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            if let window = UIApplication.shared.keyWindow{
                
                self.blackView.alpha = 0
                self.resetPwdView.frame = CGRect(x: 0 , y: window.frame.height, width: window.frame.width, height: window.frame.height/2)
                
            }
        }, completion: { (completion) in
            
            self.blackView.removeFromSuperview()
            self.resetPwdView.removeFromSuperview()
            
        })
        
        
        
        
    }
    func setMessage(body:String){
        
        let view = MessageView.viewFromNib(layout: .centeredView)
        
        // Theme message elements with the warning style.
        view.configureTheme(.error)
        
        // Add a drop shadow.
        view.configureDropShadow()
        
        view.configureContent(title: "Sign In", body: body)
        
        view.button?.isHidden = true
        
        // Increase the external margin around the card. In general, the effect of this setting
        // depends on how the given layout is constrained to the layout margins.
        view.layoutMarginAdditions = UIEdgeInsets(top: 20, left: 20, bottom: 20, right: 20)
        
        // Reduce the corner radius (applicable to layouts featuring rounded corners).
        (view.backgroundView as? CornerRoundingView)?.cornerRadius = 10
        
        // Show the message.
        SwiftMessages.show(view: view)
        
        
    }
    
    //    func setGradientBackground() -> CAGradientLayer {
    //        let colorTop =  UIColor(red: 182/255.0, green: 77/255.0, blue: 67/255.0, alpha: 0.4).cgColor
    //        let colorBottom = UIColor(red: 243/255.0, green: 198/255.0, blue: 143/255.0, alpha: 0.4).cgColor
    //
    //        let gradientLayer = CAGradientLayer()
    //        gradientLayer.colors = [colorTop, colorBottom]
    //        gradientLayer.locations = [0.0, 1.0]
    //        gradientLayer.frame = self.view.bounds
    //
    //        return gradientLayer
    //    }

    
    @IBAction func unwindToRoot(segue: UIStoryboardSegue){
        let _ = segue.source as! RegisterViewController
    }
    
}

