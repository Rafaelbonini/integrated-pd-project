////
////  RestaurantMenuTableViewController.swift
////  FinalProject_Customer
////
////  Created by Rafael Bonini on 12/16/18.
////  Copyright © 2018 Rafael Bonini. All rights reserved.
////
//
//import UIKit
//
//private let cellItemId = "menuCell"
//class RestaurantMenuTableViewController: UITableViewController {
//
//
//    var itemsArray : [RestaurantItemsTestModel] = []
//
//
//
//
//
//    //    let menuTittles = ["Appetizers","Main Dishes","Desserts"]
//
//    var category1 : [RestaurantItemsTestModel] = []
//
//    var category2 : [RestaurantItemsTestModel] = []
//
//    let menuTittles = ["Appetizers","Main Dishes"]
//
//    var menuArray = [
//
//        ExpandableNames.init(isExpanded: true, names: ["Artichoke & Spinach Dip","Chicken Fajita Nachos","Lobster Pizza","Tamale Cakes","Shrimp Scampi"]),
//        ExpandableNames.init(isExpanded: true, names: ["Lamb Salad with Fregola","Smoked Pork Jowl with Pickles","Vegan Charcuterie","Pork Rillette Hand Pies","Malted Custard French Toast"]),
//        ExpandableNames.init(isExpanded: true, names: ["Chocolate Fondue","Brownie Sundae","Cannoli","Strawberry Shortcake","Creme Brulee","Apple Pie"])
//
//    ]
//
//
//
//
//    override func viewDidLoad() {
//        super.viewDidLoad()
//
//
//        // Uncomment the following line to preserve selection between presentations
//        // self.clearsSelectionOnViewWillAppear = false
//
//        // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
//        // self.navigationItem.rightBarButtonItem = self.editButtonItem
//    }
//
//    // MARK: - Table view data source
//
//    override func numberOfSections(in tableView: UITableView) -> Int {
//        // #warning Incomplete implementation, return the number of sections
//        return menuArray.count
//    }
//
//    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
//        // #warning Incomplete implementation, return the number of rows
//
//        if !menuArray[section].isExpanded{
//            return 0
//        }
//
//
//        return menuArray[section].names.count
//    }
//
//    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
//
//
//        let label = UILabel()
//        label.text = menuTittles[section]
//        label.frame = CGRect(x: 4, y: 0, width: 100, height: 35)
//
//        let button = UIButton(type: .system)
//        button.setTitle("Close", for: .normal)
//        button.backgroundColor = UIColor.orange
//        button.contentHorizontalAlignment = .right
//        button.contentEdgeInsets = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 8)
//        button.addSubview(label)
//
//        button.addTarget(self, action: #selector(handleExpandClose), for: .touchUpInside)
//
//        button.tag = section
//
//        return button
//    }
//
//    @objc func handleExpandClose(button: UIButton){
//
//        print("Trying to expand and close section...")
//
//        let section = button.tag
//        
//        // we'll try to close the section first by deleting the rows
//        var indexPaths = [IndexPath]()
//        for row in menuArray[section].names.indices {
//            let indexPath = IndexPath(row: row, section: section)
//            indexPaths.append(indexPath)
//        }
//
//        let isExpanded = menuArray[section].isExpanded
//        menuArray[section].isExpanded = !isExpanded
//
//        button.setTitle(isExpanded ? "Open" : "Close", for: .normal)
//
//        if isExpanded {
//            tableView.deleteRows(at: indexPaths, with: .fade)
//        } else {
//            tableView.insertRows(at: indexPaths, with: .fade)
//        }
//    }
//
//    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
//        return 35
//    }
//
//    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
//        let cell = tableView.dequeueReusableCell(withIdentifier: cellItemId, for: indexPath) as! MenuItemTableViewCell
//
//        cell.itemName.text = menuArray[indexPath.section].names[indexPath.row]
//
//        return cell
//    }
//
//    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
//
//        self.dismiss(animated: true, completion: nil)
//        tableView.deselectRow(at: indexPath, animated: true)
//    }
//
//}
