//
//  EditProfileTableViewController.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 2/2/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase

private let editProfileSegueId = "ToEditProfile"
private let verifyPasswordSegue = "GoVerifyPassword"
private let updateLocation = "updateCurrentLocation"
private let cellId = "cellId"
class EditProfileTableViewController: UITableViewController,UIImagePickerControllerDelegate, UINavigationControllerDelegate  {


    @IBOutlet weak var userProfileView: UIImageView!
    
    //username: location NumerOfTables: email: password
    
    var itemTittles = ["Restaurant Name","Email","Location","Password","Number of Tables"]
    var itemValues = ["","","","",""]
    var selectedIndex = -1
    var imagePicker = UIImagePickerController()
    var userUid :String?
    
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        let singleTap = UITapGestureRecognizer(target: self, action: #selector(editProfileImage))
        userProfileView.isUserInteractionEnabled = true
        userProfileView.addGestureRecognizer(singleTap)
        
        
        userProfileView.layer.cornerRadius = userProfileView.bounds.width/2
        userProfileView.layer.masksToBounds = true
        
        tableView.tableFooterView = UIView()
        
        
        addCameraIcToImgView()
        
        userUid = Auth.auth().currentUser?.uid
        
        Database.database().reference().child("Restaurants").child(userUid!).observeSingleEvent(of: .value) { (snapshot) in
            
            let value = snapshot.value as? [String:Any]
            
            self.itemValues[0] = value?["username"] as? String ?? ""
            self.itemValues[1] = value?["email"] as? String ?? ""
//            let userPhone = value?["phone"] as? NSDictionary
//            self.itemValues[2] = userPhone?["phoneNumber"] as? String ?? ""
            self.itemValues[2] = ""
            
//            let locationValues = value!["location"] as? [String:Any]

            
            self.itemValues[3] = "******"
            if let tableNum = value?["NumerOfTables"] as? Int{
                self.itemValues[4] = String(tableNum)
            }
            
            self.tableView.reloadData()
            
        }
        
        
        let profileImageRef = Storage.storage().reference().child("RestaurantPictures/\(userUid!).jpg")
        
        profileImageRef.getData(maxSize: 1 * 1024 * 1024) { (data, error) in
            
            if error != nil{
                print("error ocurred getting image from database")
            }else{
                
                let image = UIImage(data: data!)
                
                DispatchQueue.main.async {
                    self.userProfileView.image = image
                    self.tableView.reloadData()
                }
                
            }
        }
        
    }

    func saveNewImageToDB(profilePic:UIImage){

        if let image = profilePic.jpegData(compressionQuality: 0.1){

            let storageRefere = Storage.storage().reference().child("RestaurantPictures/\(userUid!).jpg")

            storageRefere.putData(image, metadata: nil){ (metadata, error) in

                if error != nil{
                    print(error!)

                }else{

                    print("naicela")
                }


            }
        }
    }
    // MARK: - Table view data source
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return itemTittles.count
    }
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: cellId, for: indexPath) as! EditProfileTableViewCell
        
        // Configure the cell...
        
        cell.cellEditPurpose.text = itemTittles[indexPath.row]
        cell.cellValue.text = itemValues[indexPath.row]
        
        
        return cell
    }
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70;
    }
    
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        
        selectedIndex = indexPath.row
        
        tableView.deselectRow(at: indexPath, animated: true)
        
        if itemTittles[indexPath.row] == "Email" || itemTittles[indexPath.row] == "Password"{
            
            performSegue(withIdentifier: verifyPasswordSegue, sender: nil)
        }else if(itemTittles[indexPath.row] == "Location"){
         
            performSegue(withIdentifier: updateLocation, sender: nil)
        }else{
            performSegue(withIdentifier: editProfileSegueId, sender: nil)
        }
        
        
        
    }
    @IBAction func closeController(_ sender: Any) {
        
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
        
    }
    
    func addCameraIcToImgView(){
        
        let image = UIImage(named: "camera_ic_white")
        let imageView = UIImageView(image: image)
        imageView.frame = CGRect(x: (userProfileView.bounds.width/2)-25, y: (userProfileView.bounds.height/2)-25, width: 50, height: 40)
        imageView.alpha = 0.6
        userProfileView.addSubview(imageView)
        
    }
    
    @objc func editProfileImage(){
        
        
        self.imagePicker.delegate = self
        
        let actionSheet = UIAlertController(title: "Photo Source", message: "Choose a source", preferredStyle: .actionSheet)
        
        actionSheet.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action:UIAlertAction) in
            
            self.imagePicker.sourceType = .camera
            self.present(self.imagePicker, animated: true, completion: nil)
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Photo Library", style: .default, handler: { (action:UIAlertAction) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary) {
                
                self.imagePicker.sourceType = .photoLibrary;
                self.imagePicker.allowsEditing = true
                self.present(self.imagePicker, animated: true, completion: nil)
            }
            
        }))
        
        actionSheet.addAction(UIAlertAction(title: "Cancel", style: .cancel, handler: nil))
        
        self.present(actionSheet, animated: true, completion: nil)
        
        
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        let image = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        
        userProfileView.image = image
        
        saveNewImageToDB(profilePic: image)
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        let userUid = Auth.auth().currentUser?.uid
        
        
        Database.database().reference().child("Restaurants").child(userUid!).observeSingleEvent(of: .value) { (snapshot) in
            
            let value = snapshot.value as? NSDictionary
            
            //username: location NumerOfTables: email: password
            
            self.itemValues[0] = value?["username"] as? String ?? ""
            self.itemValues[1] = value?["email"] as? String ?? ""
            self.itemValues[2] = ""
            self.itemValues[3] = "******"
            if let tableNum = value?["NumerOfTables"] as? Int{
                self.itemValues[4] = String(tableNum)
            }
            
            self.tableView.reloadData()
            
        }
        
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == editProfileSegueId{
            
            let editProfile = segue.destination as! UpdateInfoProfileViewController
            
            editProfile.getLabelTittle = itemTittles[selectedIndex]
            editProfile.getValue = itemValues[selectedIndex]
            
        }else if segue.identifier == verifyPasswordSegue{
            
            
            let verifyPassword = segue.destination as! VerifyUserViewController
            
            verifyPassword.getLabelTittle = itemTittles[selectedIndex]
            verifyPassword.getValue = itemValues[selectedIndex]
            
        }
    }


}
