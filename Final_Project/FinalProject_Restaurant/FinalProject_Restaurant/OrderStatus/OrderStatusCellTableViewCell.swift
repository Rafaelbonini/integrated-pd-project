//
//  OrderStatusCellTableViewCell.swift
//  FinalProject_Restaurant
//
//  Created by Rafael Bonini on 1/31/19.
//  Copyright © 2019 Rafael Bonini. All rights reserved.
//

import UIKit

class OrderStatusCellTableViewCell: UITableViewCell {

    @IBOutlet weak var tableNumLabel: UILabel!
    @IBOutlet weak var timeStampLabel: UILabel!
    @IBOutlet weak var tableStatusLabel: UILabel!
    
    var orderInfo : OrdersInTableData?{
        
        didSet{
            
            
            if let tableNum = orderInfo?.tableNum, let timestamp = orderInfo?.timeStamp, let status = orderInfo?.tableStatus{
            
                tableNumLabel.text = "Table \(tableNum)"
                
                if status == ""{
                    tableStatusLabel.text = "Paid"
                }else{
                    tableStatusLabel.text = status
                }
                
                let date = NSDate(timeIntervalSince1970: Double(timestamp))
                let dayTimePeriodFormatter = DateFormatter()
                
                dayTimePeriodFormatter.dateFormat = "HH:mm"
                
                let dateString = dayTimePeriodFormatter.string(from: date as Date)
                
                timeStampLabel.text = dateString
                
            }
            
        }
        
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
