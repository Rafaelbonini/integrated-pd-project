//
//  UpdateInfoProfileViewController.swift
//  FinalProject_Customer
//
//  Created by Rafael Bonini on 12/14/18.
//  Copyright © 2018 Rafael Bonini. All rights reserved.
//

import UIKit
import Firebase
import JGProgressHUD


class UpdateInfoProfileViewController: UIViewController {
    
    @IBOutlet weak var errorFeedbackLabel: UILabel!
    @IBOutlet weak var editLabel: UILabel!
    @IBOutlet weak var editTextField: UITextField!
    @IBOutlet weak var updateButton: UIButton!
    var getLabelTittle :String?
    var getValue : String?
    var getSecureEntry : Bool = false
    let hud = JGProgressHUD(style: .dark)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //set if text field will be used with secure text entry or not
        editTextField.isSecureTextEntry = getSecureEntry
        
        errorFeedbackLabel.alpha = 0
        
        
        if getLabelTittle == "Password"{
            editLabel.text = "New Password"
        }else{
            editLabel.text = getLabelTittle
        }
        
        editTextField.text = getValue
        
        
        updateButton.setTitle("Update \(getLabelTittle!)", for: .normal)
        
        setTextField()
    }
    
    
    func setTextField(){
        
        //small padding adjustment
        editTextField.setLeftPaddingPoints(1)

        //black line no bottom of text field
        let bottomLine = CALayer()
        bottomLine.frame = CGRect.init(x: 0, y: editTextField.frame.size.height - 1, width: editTextField.frame.size.width, height: 2)
        bottomLine.backgroundColor = UIColor.black.cgColor
        editTextField.borderStyle = UITextField.BorderStyle.none
        editTextField.layer.addSublayer(bottomLine)
    }

    @IBAction func dismissView(_ sender: Any) {
//        self.dismiss(animated: true, completion: nil)
        self.navigationController?.popViewController(animated: true)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(true, animated: false)
    }
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.setNavigationBarHidden(false, animated: false)
    }
    
    @IBAction func updateItem(_ sender: Any) {
        
        let userUid = Auth.auth().currentUser?.uid
        
        
        switch getLabelTittle {
        case "Name":
            
            if let name = editTextField.text{
                
                if name.count > 2{
                    hud.textLabel.text = "Updating Name"
                    hud.show(in: self.view)
                    
                    //set the user name in the user auth
                    let changeResquest = Auth.auth().currentUser?.createProfileChangeRequest();
                    changeResquest?.displayName = name;
                    changeResquest?.commitChanges(completion: { (error) in
                        if (error != nil){
                            print("user name change request failed")
                            self.hud.dismiss(animated: true)
                        }else{
                            
                            //update name in database
                            let ref = Database.database().reference().child("Users").child(userUid!)
                            ref.updateChildValues(["username":name])
                            
//                            self.dismiss(animated: true, completion: nil)
                            self.navigationController?.popViewController(animated: true)
                            print("user name chaged!")
                        }
                    })
                }else{
                 // name with less than 3 characters
                    self.editTextField.setLineAndColorTV(color: UIColor.red)
                    errorFeedbackLabel.alpha = 1
                    errorFeedbackLabel.text = "A valid name need 3 or more characters"
                }
            }
            
        case "Email":
            
            if let email = editTextField.text{
                
                if email.isValidEmail(){
                    hud.textLabel.text = "Updating Email"
                    hud.show(in: self.view)

                    Auth.auth().currentUser?.updateEmail(to: email) { (error) in

                        if (error != nil){
                            print("email not changed, error:")
                            self.hud.dismiss(animated: true)
                        }else{
                            
                            let ref = Database.database().reference().child("Users").child(userUid!)
                            ref.updateChildValues(["email":email])
                            
                            //go back to profile edit page
                            self.hud.dismiss(animated: true)
                            
                            self.navigationController?.popToViewController(ofClass: ProfileEditTableViewController.self, animated: true)
//                            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                            print("email changed")
                        }
                    }
                }else{
                    //email not valid
                    self.editTextField.setLineAndColorTV(color: UIColor.red)
                    errorFeedbackLabel.alpha = 1
                    errorFeedbackLabel.text = "Please, enter a valid email"
                    
                }
                
            }
            
        case "Cellphone":
            
            print("bla")
        case "Password":
            
            if let password = editTextField.text{
                
                if password.count > 5{
                    hud.textLabel.text = "Updating Password"
                    hud.show(in: self.view)
                    
                    Auth.auth().currentUser?.updatePassword(to: password) { (error) in
                        
                        if error != nil{
                            print("Failed to update password")
                            self.hud.dismiss(animated: true)
                            self.editTextField.setLineAndColorTV(color: UIColor.red)
                        }else{
                            print("Password Updated")
                            
                            //go back to profile edit page
                            self.hud.dismiss(animated: true)
                            self.navigationController?.popToViewController(ofClass: ProfileEditTableViewController.self, animated: true)
//                            self.presentingViewController?.presentingViewController?.dismiss(animated: true, completion: nil)
                        }
                    }
                }else{
                    print("password does not have more than 5 characters")
                    self.editTextField.setLineAndColorTV(color: UIColor.red)
                    errorFeedbackLabel.alpha = 1
                    errorFeedbackLabel.text = "A valid password needs 6 or more characters."
                }
            }
        default:
            break;
        }
        
        
    }
}
